TOMS INTEGRATION FRAMEWORK - TOMS Weather App
===

## Ports
 **_Port used for application/swagger/health_**
 * server.port = 8222

----

##SOFTWARE:
>       Gradle:  4.10.3
>       SpringBoot: 1.5.22.RELEASE
>       Apache Camel: 2.20.4


Using Gradle, Apache Camel, and Spring Boot.


To run the application through Spring Boot:

	$ gradle bootRun

----

**H2 Console For DEV-Env**

> NOTE: this service calls the toms repository  service and you can query the database
using the toms respository db console below.

*Example Query*
```SQL
select * from r5trackingdata where tkd_sourcecode = 'TIF' and tkd_TRANS = 'KTW';
``````

The following Database Console allows developers to run queries against the in-memory database.

    http://localhost:8095/h2-console
    User: sa
    Pass: [blank] - no password
    JDBC URL: jdbc:h2:mem:TEST;MVCC=true;DB_CLOSE_DELAY=-1;MODE=Oracle

----

## RUNGUIDE
   [Go to this page](RunGuide.md)
   
----

## Testing

----

## Design Documentation


## Health / Actuator
The following health URL's are available

**Application Health**

    http://localhost:8222/health

**Application Log File**

    http://localhost:8222/loggers
    
**Beans Loaded**

    http://localhost:8222/beans

**Environment Variables Loaded**

    http://localhost:8222/env
    
**Camel Routes**

    http://localhost:8222/camel/routes

    http://localhost:8222/camel/routes/routeForecastWeatherTrigger/info

    http://localhost:8222/camel/routes/routeCurrentWeatherTrigger/detail
    
    http://localhost:8222/camel/routes/routePostTomRepository/detail
    
    http://localhost:8222/camel/routes/routeForecastWeather/detail
    
    http://localhost:8222/camel/routes/routeDirectCurrentWeather/detail
    
    http://localhost:8222/camel/routes/route2/detail
    
    http://localhost:8222/camel/routes/route1/detail
    
** Mapping Documentation **

|JSON FIELD         |R5 FIELD    |DESCRIPTION|
|-------------------|------------|----------|
|lon                |promptData1 | Longitude|
|lat                |promptData2 | Latitude |
|country            |promptData3 |
|timezone           |promptData4 |
|sunrise            |promptData5 |
|sunset             |promptData6 |
|weather id         |promptData7 |
|weather main       |promptData8 |
|weather description|promptData9 |
|temp               |promptData10|
|feels_like         |promptData11|
|temp_min           |promptData12|
|temp_max           |promptData13|
|pressure           |promptData14|
|humidity           |promptData15|
|visibility         |promptData16|
|wind speed         |promptData17|
|wind deg           |promptData18|
|clouds             |promptData19|
|dt                 |promptData20| Date Time UTC |
|id                 |promptData21| Location Id   |
|name               |promptData22| Location Name |
|sea_level          |promptData23|
|grnd_level         |promptData24|
|temp_kf            |promptData25|
|snow 3h            |promptData26|
|Load Type          |promptData50| FORECAST_WEATHER or CURRENT_WEATHER |