# tif-weather-app  (MICROSERVICE)

## SERVICE DESCRIPTION

The TIF Weather Service (App) ...

![Image](./documentation/images/KalTire_TOMS_DD_Weather_App.png)

...

  
---
  
  
### Service Process Flow, Structure & Operations

#TODO://
   
### API Operations


>	#### Swagger End Point:
```
http://< host >:8222/api/tif/v1.0/api-doc/swagger.json
```
   
---
   
   
## SERVICE ADMINISTRATION

### Stop Start Scripts

> Stop Service
```
sudo systemctl stop tif-weather.service
```

> Start Service
```
sudo systemctl start tif-weather.service
```

> Status Service
```
sudo systemctl status tif-weather.service
```

### Environments
|HOST                   |ENVIRONMENT|
|-----------------------|-----------|
| (NOT SETUP)           | dev       |
| lqatomsif01v.kal.test | qa        |
| tomsifpre.kaltire.com | pre       |
| tomsif.kaltire.com    | prod      |



### Logging

Each Service has own log file
```
/u01/<<<location>>>
```

   
---
   
   
## SERVICE CONFIGURATION

Each Service has own Configuration Folder
```
/u01/tif/tif-weather.service/
```
Located within each of the configuration folders you will see the following set of files

* /u01/tif/tif-weather.service/application.properties
* /u01/tif/tif-weather.service/application-dev.properties
* /u01/tif/tif-weather.service//tif-weather-service.conf

### application.properties Configuration
Specifies the environment variable for the service
 * options = [dev, qa, pre, prod]

 Example:
 ```
 spring.profiles.active=qa
 ```

### application-dev.properties Configration


### Available Override Options
|Option                                         |Value|
|-----------------------------------------------|------------------------------------------|
| server.port                                   | Port the health endpoints are exposed on |
| db.service.url                                | http://0.0.0.0:8222  |
| db.service.create.r5track.url                 | http://0.0.0.0:8222/api/tif/v1.0/r5track  |
| db.service.count.r5track.url                  | http://0.0.0.0:8222/api/tif/v1.0/r5track/counts  |
| db.service.service.ping.url                   | http://0.0.0.0:8222/ping  |
| logging.level.com.mdw360.service=             | DEBUG  |
| security.basic.enabled                        | true  |
| security.user.name                            | TIF  |
| security.user.password                        | ************  |
| tif.home                                      | #{systemProperties['user.home']}  |

### tif-weather-service.conf Configuration
Each service will have own JVM Settings file to override the JVM Settings and pass system based properties

Example (currently configured values):

```
JAVA_OPTS='-XX:+UseG1GC -XX:MaxRAM=256m -XX:+TieredCompilation -XX:TieredStopAtLevel=1 -XX:MinHeapFreeRatio=20 -XX:MaxHeapFreeRatio=40 -XX:GCTimeRatio=4 -XX:AdaptiveSizePolicyWeight=90'
```

   
---
   
   
## HEALTH ENDPOINTS

**HEALTH (UP/DOWN)**
```
   curl --request GET --header 'application/json' http://lqatomsif01v.kal.test:8222/health
```

> SAMPLE RESULT:

```java
{
    "status": "UP",
    "healthService": {
        "status": "UP",
        "email.service.connection.up": true,
        "email.service.connection.message": "Successfully connected with EMAIL (2018-08-13 14:25 PDT)",
        "sftp.service.connection.up": true,
        "sftp.service.connection.message": "Successfully connected with SFTP (2018-08-13 14:25 PDT)",
        "db.service.connection.up": true,
        "db.service.connection.message": "Successfully connected with DB (2018-08-13 14:25 PDT)"
    },
    "camel": {
        "status": "UP",
        "name": "Tif Toms Meterdata App",
        "version": "2.20.3",
        "contextStatus": "Started"
    },
    "camel-health-checks": {
        "status": "UP",
        "route:simple.timer.route": "UP",
        "route:sftp-event-queue-route": "UP",
        "route:email-event-queue-route": "UP",
        "route:site-meter-event-queue-route": "UP",
        "route:site-meter-processor-queue-route": "UP",
        "route:toms-repository-queue.route": "UP",
        "route:datastore-repository-queue.route": "UP"
    },
    "diskSpace": {
        "status": "UP",
        "total": 19329449984,
        "free": 13420089344,
        "threshold": 10485760
    }
}
```

**HEALTH CAMEL ROUTES**
```
   curl --request GET --header 'application/json' http://lqatomsif01v.kal.test:8222/camel/health/check
```

> SAMPLE RESULTS:

```java
[
    {
        "status": "UP",
        "check": {
            "id": "route:simple.timer.route",
            "group": "camel"
        }
    },
    {
        "status": "UP",
        "check": {
            "id": "route:sftp-event-queue-route",
            "group": "camel"
        }
    },
    {
        "status": "UP",
        "check": {
            "id": "route:email-event-queue-route",
            "group": "camel"
        }
    },
    {
        "status": "UP",
        "check": {
            "id": "route:site-meter-event-queue-route",
            "group": "camel"
        }
    },
    {
        "status": "UP",
        "check": {
            "id": "route:site-meter-processor-queue-route",
            "group": "camel"
        }
    },
    {
        "status": "UP",
        "check": {
            "id": "route:toms-repository-queue.route",
            "group": "camel"
        }
    },
    {
        "status": "UP",
        "check": {
            "id": "route:datastore-repository-queue.route",
            "group": "camel"
        }
    }
]

```

**HEALTH CAMEL -- ( toms-repository-queue.route )**
```
   curl --request GET --header 'application/json' http://lqatomsif01v.kal.test:8222/camel/health/check/route:toms-repository-queue.route
```

> SAMPLE RESULTS:

```java
{
    "status": "UP",
    "details": {
        "route.id": "toms-repository-queue.route",
        "invocation.count": 3,
        "route.context.name": "Tif Toms Meterdata App",
        "invocation.time": "2018-08-13T14:27:09.633-07:00[America/Vancouver]",
        "route.status": "Started",
        "failure.count": 0
    },
    "check": {
        "id": "route:toms-repository-queue.route",
        "group": "camel",
        "metaData": {
            "invocation.count": 3,
            "invocation.attempt.time": "2018-08-13T14:27:09.633-07:00[America/Vancouver]",
            "check.id": "route:toms-repository-queue.route",
            "invocation.time": "2018-08-13T14:27:09.633-07:00[America/Vancouver]",
            "check.group": "camel",
            "failure.count": 0
        },
        "configuration": {
            "enabled": true
        }
    }
}

```

**HEALTH METRICS**
```
   curl --request GET --header 'application/json' http://lqatomsif01v.kal.test:8222/metrics
```

> SAMPLE RESULTS:

```java
{
    "mem": 185255,
    "mem.free": 15966,
    "processors": 1,
    "instance.uptime": 1743063990,
    "uptime": 1743074132,
    "systemload.average": 0.01,
    "heap.committed": 108544,
    "heap.init": 8192,
    "heap.used": 92193,
    "heap": 129024,
    "nonheap.committed": 78552,
    "nonheap.init": 2496,
    "nonheap.used": 76713,
    "nonheap": 0,
    "threads.peak": 31,
    "threads.daemon": 25,
    "threads.totalStarted": 37858,
    "threads": 27,
    "classes": 10420,
    "classes.loaded": 10638,
    "classes.unloaded": 218,
    "gc.g1_young_generation.count": 2760,
    "gc.g1_young_generation.time": 17892,
    "gc.g1_old_generation.count": 0,
    "gc.g1_old_generation.time": 0,
    "httpsessions.max": -1,
    "httpsessions.active": 0,
    "gauge.response.env": 581,
    "gauge.response.health": 1117,
    "gauge.response.camel.health.check": 8,
    "gauge.response.camel.health.check.id": 135,
    "gauge.response.star-star": 115,
    "counter.total.files.processed.from.sftp.to.db.services": 57,
    "counter.total.files.processed.from.emails.service.to.sftp.service": 67,
    "counter.status.200.camel.health.check.id": 1,
    "counter.status.200.health": 1,
    "counter.status.404.star-star": 1,
    "counter.status.200.camel.health.check": 1,
    "counter.status.200.env": 1
}
```

**HEALTH ROUTES INFO**
```
   curl --request GET --header 'application/json' http://lqatomsif01v.kal.test:8222/routesInfo
```

> SAMPLE RESULTS:

```java
[
    {
        "uptime": "20 days 4 hours",
        "uptimeMillis": 1743081551,
        "name": "simple.timer.route",
        "status": "Started",
        "details": {
            "deltaProcessingTime": 0,
            "exchangesInflight": 0,
            "exchangesTotal": 29052,
            "externalRedeliveries": 0,
            "failuresHandled": 0,
            "firstExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-1",
            "firstExchangeCompletedTimestamp": 1532452589286,
            "lastExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-2499231",
            "lastExchangeCompletedTimestamp": 1534195652637,
            "lastProcessingTime": 3,
            "maxProcessingTime": 301,
            "meanProcessingTime": 3,
            "minProcessingTime": 2,
            "redeliveries": 0,
            "totalProcessingTime": 91981,
            "hasRouteController": false
        }
    },
    {
        "uptime": "20 days 4 hours",
        "uptimeMillis": 1743081657,
        "name": "sftp-event-queue-route",
        "status": "Started",
        "details": {
            "deltaProcessingTime": -412,
            "exchangesInflight": 0,
            "exchangesTotal": 203364,
            "externalRedeliveries": 0,
            "failuresHandled": 0,
            "firstExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-42",
            "firstExchangeCompletedTimestamp": 1532452590724,
            "lastExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-2499316",
            "lastExchangeCompletedTimestamp": 1534195662976,
            "lastProcessingTime": 1312,
            "maxProcessingTime": 1151450,
            "meanProcessingTime": 1516,
            "minProcessingTime": 11,
            "redeliveries": 0,
            "totalProcessingTime": 308407094,
            "hasRouteController": false
        }
    },
    {
        "uptime": "20 days 4 hours",
        "uptimeMillis": 1743081660,
        "name": "email-event-queue-route",
        "status": "Started",
        "details": {
            "deltaProcessingTime": -135,
            "exchangesInflight": 0,
            "exchangesTotal": 203364,
            "externalRedeliveries": 0,
            "failuresHandled": 0,
            "firstExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-44",
            "firstExchangeCompletedTimestamp": 1532452589708,
            "lastExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-2499306",
            "lastExchangeCompletedTimestamp": 1534195654594,
            "lastProcessingTime": 269,
            "maxProcessingTime": 100136,
            "meanProcessingTime": 234,
            "minProcessingTime": 7,
            "redeliveries": 0,
            "totalProcessingTime": 47762065,
            "hasRouteController": false
        }
    },
    {
        "uptime": "20 days 4 hours",
        "uptimeMillis": 1743081660,
        "name": "site-meter-event-queue-route",
        "status": "Started",
        "details": {
            "deltaProcessingTime": 0,
            "exchangesInflight": 0,
            "exchangesTotal": 203364,
            "externalRedeliveries": 0,
            "failuresHandled": 0,
            "firstExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-46",
            "firstExchangeCompletedTimestamp": 1532452589264,
            "lastExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-2499288",
            "lastExchangeCompletedTimestamp": 1534195652643,
            "lastProcessingTime": 1,
            "maxProcessingTime": 190,
            "meanProcessingTime": 0,
            "minProcessingTime": 0,
            "redeliveries": 0,
            "totalProcessingTime": 150077,
            "hasRouteController": false
        }
    },
    {
        "uptime": "20 days 4 hours",
        "uptimeMillis": 1743081660,
        "name": "site-meter-processor-queue-route",
        "status": "Started",
        "details": {
            "deltaProcessingTime": 8,
            "exchangesInflight": 0,
            "exchangesTotal": 59,
            "externalRedeliveries": 0,
            "failuresHandled": 0,
            "firstExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-19504",
            "firstExchangeCompletedTimestamp": 1532466149410,
            "lastExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-1142978",
            "lastExchangeCompletedTimestamp": 1533249391034,
            "lastProcessingTime": 8,
            "maxProcessingTime": 19,
            "meanProcessingTime": 0,
            "minProcessingTime": 0,
            "redeliveries": 0,
            "totalProcessingTime": 56,
            "hasRouteController": false
        }
    },
    {
        "uptime": "20 days 4 hours",
        "uptimeMillis": 1743081665,
        "name": "toms-repository-queue.route",
        "status": "Started",
        "details": {
            "deltaProcessingTime": -920,
            "exchangesInflight": 0,
            "exchangesTotal": 59,
            "externalRedeliveries": 0,
            "failuresHandled": 0,
            "firstExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-19510",
            "firstExchangeCompletedTimestamp": 1532466150030,
            "lastExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-1142984",
            "lastExchangeCompletedTimestamp": 1533249392033,
            "lastProcessingTime": 1000,
            "maxProcessingTime": 3157,
            "meanProcessingTime": 1189,
            "minProcessingTime": 2,
            "redeliveries": 0,
            "totalProcessingTime": 70205,
            "hasRouteController": false
        }
    },
    {
        "uptime": "20 days 4 hours",
        "uptimeMillis": 1743081675,
        "name": "datastore-repository-queue.route",
        "status": "Started",
        "details": {
            "deltaProcessingTime": 390,
            "exchangesInflight": 0,
            "exchangesTotal": 59,
            "externalRedeliveries": 0,
            "failuresHandled": 0,
            "firstExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-19512",
            "firstExchangeCompletedTimestamp": 1532466149764,
            "lastExchangeCompletedExchangeId": "ID-lpretomsif01v-1532452582266-0-1142986",
            "lastExchangeCompletedTimestamp": 1533249391575,
            "lastProcessingTime": 541,
            "maxProcessingTime": 541,
            "meanProcessingTime": 127,
            "minProcessingTime": 2,
            "redeliveries": 0,
            "totalProcessingTime": 7510,
            "hasRouteController": false
        }
    }
]
```

**CONFIGURATION PROPERTIES**
```
   curl --request GET --header 'application/json' http://lqatomsif01v.kal.test:8222/configprops
```

> SAMPLE RESULTS:

```java
{
    "camel.component.vm-org.apache.camel.component.vm.springboot.VmComponentConfiguration": {
        "prefix": "camel.component.vm",
        "properties": {
            "queueSize": null,
            "concurrentConsumers": 1,
            "enabled": true,
            "resolvePropertyPlaceholders": true,
            "defaultQueueFactory": null
        }
    },
    "camel.language.tokenize-org.apache.camel.language.tokenizer.springboot.TokenizeLanguageConfiguration": {
        "prefix": "camel.language.tokenize",
        "properties": {
            "trim": true,
            "enabled": true
        }
    },
    "camel.component.binding-org.apache.camel.component.binding.springboot.BindingNameComponentConfiguration": {
        "prefix": "camel.component.binding",
        "properties": {
            "enabled": true,
            "resolvePropertyPlaceholders": true
        }
    },
    ...

```

**ENVIRONMENT PROPERTIES**
```
   curl --request GET --header 'application/json' http://lqatomsif01v.kal.test:8222/env
```

> SAMPLE RESULT:

```java
{
    "profiles": [
        "pre"
    ],
    "server.ports": {
        "local.server.port": 8222
    },
    "servletContextInitParams": {},
    "systemProperties": {
        "java.runtime.name": "OpenJDK Runtime Environment",
        "java.protocol.handler.pkgs": "org.springframework.boot.loader",
        "sun.boot.library.path": "/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.161-0.b14.el7_4.x86_64/jre/lib/amd64",
        "java.vm.version": "25.161-b14",
        "java.vm.vendor": "Oracle Corporation",
        "java.vendor.url": "http://java.oracle.com/",
    ...
```
**GIT COMMIT INFO**
```
   curl --request GET --header 'application/json' http://lqatomsif01v.kal.test:8222/info
```
> SAMPLE RESULTS:

```java
{
    "git": {
        "commit": {
            "time": "2018-07-17T08:42-0700",
            "message": {
                "full": "Update properties files for new configuration locations",
                "short": "Update properties files for new configuration locations"
            },
            "id": "66349a350b7862dc9eedb0d0196dd2c26777140a",
            "id.abbrev": "66349a3",
            "id.describe": "66349a3",
            "user": {
                "email": "neil@middleware360.com",
                "name": "Neil"
            }
        },
        "dirty": "false",
        "branch": "HEAD"
    }
}

```

**LOGGING INFO**
```
   curl --request GET --header 'application/json' http://lqatomsif01v.kal.test:8222/loggers
```

> SAMPLE RESULT:

```java
{
    "levels": [
        "OFF",
        "ERROR",
        "WARN",
        "INFO",
        "DEBUG",
        "TRACE"
    ],
    "loggers": {
        "ROOT": {
            "configuredLevel": "INFO",
            "effectiveLevel": "INFO"
        },
        "com": {
            "configuredLevel": null,
            "effectiveLevel": "INFO"
        },
        "com.mdw360": {
            "configuredLevel": null,
            "effectiveLevel": "INFO"
        },
        "com.mdw360.service": {
            "configuredLevel": "DEBUG",
            "effectiveLevel": "DEBUG"
        },
        "com.mdw360.service.Application": {
            "configuredLevel": null,
            "effectiveLevel": "DEBUG"
        },
        "com.mdw360.service.actuator": {
            "configuredLevel": null,
            "effectiveLevel": "DEBUG"
        },
        "com.mdw360.service.actuator.MetricsService": {
            "configuredLevel": null,
            "effectiveLevel": "DEBUG"
        },
        "com.mdw360.service.actuator.RoutesInfoEndpoint": {
            "configuredLevel": null,
            "effectiveLevel": "DEBUG"
        },
        "com.mdw360.service.api": {
            "configuredLevel": null,
            "effectiveLevel": "DEBUG"
        },
        "com.mdw360.service.api.ServiceExceptionHandlerApi": {
            "configuredLevel": null,
            "effectiveLevel": "DEBUG"
        },
        "com.mdw360.service.component": {
            "configuredLevel": null,
            "effectiveLevel": "DEBUG"
        },
        "com.mdw360.service.component.SourceRequestListener": {
            "configuredLevel": null,
            "effectiveLevel": "DEBUG"
        },
        "com.mdw360.service.config": {
            "configuredLevel": null,
            "effectiveLevel": "DEBUG"
        },
        "com.mdw360.service.config.init": {
            "configuredLevel": null,
            "effectiveLevel": "DEBUG"
        },
  ....
       
      
    }
}
```

### Invoke Endpoints Manually

*FORECAST WEATHER*

```
 curl --request GET -u TIF:gr3aTtir3s --header 'Content-Type: application/json' http://lqatomsif01v.kal.test:8222/api/weather/forecast
```
*CURRENT WEATHER*

```
 curl --request GET -u TIF:gr3aTtir3s --header 'Content-Type: application/json' http://lqatomsif01v.kal.test:8222/api/weather/current
```

### DB QUERIES

```
select * from r5trackingdata where tkd_sourcecode = 'TIF' and tkd_TRANS = 'KTW';
```
---
   
   
## OPERATIONS SUPPORT SCENARIOS
