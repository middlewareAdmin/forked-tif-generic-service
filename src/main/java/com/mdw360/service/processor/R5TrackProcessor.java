package com.mdw360.service.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mdw360.service.actuator.MetricsService;
import com.mdw360.service.component.DateHolder;
import com.mdw360.service.config.context.DBServiceContext;
import com.mdw360.service.model.dbservices.R5TrackCO;
import com.mdw360.service.model.helper.R5TrackResultContextHelper;
import com.mdw360.service.model.meterdata.configuration.ErrorContext;
import com.mdw360.service.model.meterdata.configuration.R5TrackResultContext;
import com.mdw360.service.util.baseutil.BaseUtil;
import com.mdw360.service.util.baseutil.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component("r5TrackProcessor")
@Slf4j
public class R5TrackProcessor implements Processor {

    @Autowired
    DBServiceContext dbServiceContext;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ObjectMapper jsonObjectMapper;

    @Autowired
    private MetricsService metricsService;

    @Autowired
    private DateHolder dateHolder;


    @Override
    public void process(Exchange exchange) throws Exception {
        log.info(exchange.getIn().getBody(String.class));
        R5TrackResultContextHelper r5TrackResultContextHelper = (R5TrackResultContextHelper)exchange.getIn().getBody();
        String requestId = UUID.randomUUID().toString();

        R5TrackResultContext r5TrackResultContext = r5TrackResultContextHelper.getR5TrackResultContext();
        List<R5TrackCO> r5TrackCOS = r5TrackResultContext.getSuccessResult();
        List<ErrorContext> errorsResult = r5TrackResultContext.getErrorsResult();

        int successCountOfSendR5TrackToDbService = 0;

        if (r5TrackCOS != null && r5TrackCOS.size() != 0) {
            HttpHeaders httpHeaders = BaseUtil.createHeaderWithBasicAuthAndJsonContentType(dbServiceContext.getUserName(), dbServiceContext.getPassword());

            for (R5TrackCO r5TrackCO : r5TrackCOS) {

                try {
                    String jsonInString = jsonObjectMapper.writeValueAsString(r5TrackCO);
                    HttpEntity<String> httpEntity = new HttpEntity<String>(jsonInString, httpHeaders);
                    log.debug("RequestId : {} , Invoking data repository service, URI: {}", requestId, dbServiceContext.getCreateR5trackURI());
                    ResponseEntity<Void> responseEntity = restTemplate.postForEntity(dbServiceContext.getCreateR5trackURI(), httpEntity, Void.class);
                    dateHolder.setLastAccessTimeOfDBServiceConnection(new Date());
                    log.info("RequestId: {} Successful send  to repository service, response code {}", requestId, responseEntity.getStatusCode());
                    successCountOfSendR5TrackToDbService ++;

                } catch (Exception e) {
                    log.error("RequestId: {}, Exception occur during send r5Track  {} to db service, error message {}", requestId, r5TrackCO, e.getMessage(), e);

                    StringBuilder errorMessageBuilder = new StringBuilder("Exception occur during send r5Track to db service");

                    ExceptionUtil.buildExceptionErrorMessage(e, errorMessageBuilder);
                }
            }
        }
        else {
            log.error("RequestId : {} ,Alert!!!! No R5Track available. : {}",requestId);
        }


        // Attached total successfully send r5Track to db service count
        r5TrackResultContextHelper.setSuccessCountOfSendR5TrackToDbService(successCountOfSendR5TrackToDbService);
        exchange.getIn().setBody(r5TrackResultContextHelper.getR5TrackResultContext().getSuccessResult());
        log.info("...finished running posts to DB r5TrackResultContextHelper: {}", r5TrackResultContextHelper);
    }

    @PostConstruct
    private void postConstruct(){
        log.info("DB Settings: URL: {}, USER: {}, PASS: {} Characters", dbServiceContext.getCreateR5trackURI(), dbServiceContext.getUserName(), dbServiceContext.getPassword().length());
    }
}
