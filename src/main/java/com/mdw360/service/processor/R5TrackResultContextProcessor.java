package com.mdw360.service.processor;

import com.mdw360.service.config.context.RoutePropertiesContext;
import com.mdw360.service.model.helper.R5TrackResultContextHelper;
import com.mdw360.service.model.meterdata.configuration.ErrorContext;
import com.mdw360.service.model.meterdata.configuration.R5TrackResultContext;
import com.mdw360.service.transform.PayloadToR5TrackResultContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.mdw360.service.util.constant.AppConstant.ERROR;
import static com.mdw360.service.util.constant.AppConstant.IS_DATA_AVAILABLE;

@Component("r5TrackResultContextProcessor")
@Slf4j
public class R5TrackResultContextProcessor implements Processor {

    @Autowired()
    @Qualifier("routePropertiesContext")
    RoutePropertiesContext routePropertiesContext;

    @Autowired()
    @Qualifier("currentWeatherToR5TrackResultContext")
    PayloadToR5TrackResultContext currentWeatherToR5TrackResultContext;

    @Autowired()
    @Qualifier("forecastWeatherToR5TrackResultContext")
    PayloadToR5TrackResultContext forecastWeatherToR5TrackResultContext;

    @Override
    public void process(Exchange exchange) throws Exception {

        String requestId = UUID.randomUUID().toString();
        String payload = exchange.getIn().getBody(String.class);
        String requestType = exchange.getIn().getHeader("REQUEST_TYPE", String.class);
        log.debug("RUNNING PROCESS HERE - R5TrackResultContextProcessor -- marshal response to R5TrackResultContextHelper");
        log.debug("Payload: {}", payload);
        log.info( "Starting process for requestId: {}", requestId);
        log.info("REQUEST_TYPE HEADER : {}", requestType);
        R5TrackResultContext r5TrackResultContext;

        try {
            //Transform the Payload to R5TrackResultContext
            //Has zero or many R5ResultCO's within the R5TrackResultContext
            if(requestType.equals("CURRENT_WEATHER")) {
                r5TrackResultContext = currentWeatherToR5TrackResultContext.transform(payload, routePropertiesContext.getTkdTransCode());
            }
            else if (requestType.equals("FORECAST_WEATHER")){
                r5TrackResultContext = forecastWeatherToR5TrackResultContext.transform(payload, routePropertiesContext.getTkdTransCode());
            }
            else{
                log.error("R5TrackResultContext not available for REQUEST_TYPE: {}", requestType);
                throw new Exception("R5TrackResultContext not available for REQUEST_TYPE: " + requestType);
            }

            R5TrackResultContextHelper r5TrackResultContextHelper = new R5TrackResultContextHelper(r5TrackResultContext, requestId);
            exchange.getIn().setBody(r5TrackResultContextHelper);
            exchange.getIn().setHeader(ERROR, false);

            boolean isErrorsExist = r5TrackResultContextHelper.getR5TrackResultContext().getErrorsResult().size() != 0;
            boolean isR5TrackExist = r5TrackResultContextHelper.getR5TrackResultContext().getSuccessResult().size() != 0;
            boolean isSkipResultExist = r5TrackResultContextHelper.getR5TrackResultContext().getSkipsResult().size() != 0;

            boolean isDataExistForProcess = isErrorsExist || isR5TrackExist || isSkipResultExist;

            exchange.getIn().setHeader(IS_DATA_AVAILABLE, isDataExistForProcess);

            log.info("RequestId :{}, Successfully transformed Payload to r5TrackResultContext: {}", requestId, r5TrackResultContext.toString());
        } catch (Exception e) {
            exchange.getIn().setHeader(ERROR, true);
            exchange.getIn().setHeader(IS_DATA_AVAILABLE, false);

            String errorMessage = "";
            List<ErrorContext> errorContexts = new ArrayList<>();
        }
    }
}
