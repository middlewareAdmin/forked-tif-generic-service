package com.mdw360.service.processor;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Slf4j
@Component("emptyProcessor")
public class EmptyProcessor implements Processor  {

    @Override
    public void process(Exchange exchange) throws Exception {
      log.info(exchange.getIn().toString());
    }
}
