package com.mdw360.service.component;

import com.google.common.base.Preconditions;
import org.springframework.stereotype.Component;

import java.util.Date;

/*
 * Here Get* methods returns a defensive copy of the fields.
 * The caller of get methods can do anything they want with the
 * returned Date object, without affecting the internals of this
 * class in any way. Why? Because they do not have a reference to
 * lastExecutionDate and lastProcessedDate. Rather, they are playing with a second Date that initially has the
 * same data as lastExecutionDate and lastProcessedDate.
 */
@Component
public class DateHolder {

    private static final String DATE_CANT_BE_NULL = "date Cannot be null";

    private Date lastAccessTimeOfEmailServiceConnection = null;

    private Date lastAccessTimeOfSftpServiceConnection = null;

    private Date lastAccessTimeOfDBServiceConnection = null;

    public Date getLastAccessTimeOfEmailServiceConnection() {
        return lastAccessTimeOfEmailServiceConnection != null ? new Date(lastAccessTimeOfEmailServiceConnection.getTime()) : null;
    }

    public void setLastAccessTimeOfEmailServiceConnection(Date lastAccessTimeOfEmailServiceConnection) {
        Preconditions.checkNotNull(lastAccessTimeOfEmailServiceConnection , DATE_CANT_BE_NULL);
        this.lastAccessTimeOfEmailServiceConnection = new Date(lastAccessTimeOfEmailServiceConnection.getTime());
    }

    public Date getLastAccessTimeOfSftpServiceConnection() {
        return lastAccessTimeOfSftpServiceConnection != null ? new Date(lastAccessTimeOfSftpServiceConnection.getTime()) : null;
    }

    public void setLastAccessTimeOfSftpServiceConnection(Date lastAccessTimeOfSftpServiceConnection) {
        Preconditions.checkNotNull(lastAccessTimeOfSftpServiceConnection , DATE_CANT_BE_NULL);
        this.lastAccessTimeOfSftpServiceConnection = new Date(lastAccessTimeOfSftpServiceConnection.getTime());
    }

    public Date getLastAccessTimeOfDBServiceConnection() {
        return lastAccessTimeOfDBServiceConnection != null ? new Date(lastAccessTimeOfDBServiceConnection.getTime()) : null;
    }

    //TODO: figure out if this class can just be deleted
    public void setLastAccessTimeOfDBServiceConnection(Date lastAccessTimeOfDBServiceConnection) {
        Preconditions.checkNotNull(lastAccessTimeOfDBServiceConnection , DATE_CANT_BE_NULL);
        this.lastAccessTimeOfDBServiceConnection = new Date(lastAccessTimeOfDBServiceConnection.getTime());
    }

}