package com.mdw360.service.component.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mdw360.service.model.helper.ExceptionHandlerVO;
import com.mdw360.service.util.constant.AppConstant;
import com.mdw360.service.util.enums.ExceptionPolicy;
import com.mdw360.service.util.exceptions.RetryException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.List;

import static com.mdw360.service.util.enums.ExceptionPolicy.*;

@Component("exceptionHandler")
@Slf4j
public class ExceptionHandlerImpl implements ExceptionHandler {

    @Value("${tif.home}")
    private String tifHome;

    private ObjectMapper jsonObjectMapper;

    @Autowired
    public void setJsonObjectMapper(ObjectMapper jsonObjectMapper) {
        this.jsonObjectMapper = jsonObjectMapper;
    }

    public void handleTifException(ExceptionHandlerVO exceptionHandlerVO) {
        ExceptionPolicy exceptionPolicy = null;
        if (exceptionHandlerVO.getException() instanceof HttpClientErrorException) {
            exceptionPolicy = handleHttpClientErrorException(exceptionHandlerVO);
        } else if (exceptionHandlerVO.getException() instanceof HttpServerErrorException) {
            handleHttpServerErrorException(exceptionHandlerVO);
            exceptionPolicy = HTTP_SERVER_ERROR;
        } else if (exceptionHandlerVO.getException() instanceof ResourceAccessException) {
            handleResourceAccessException(exceptionHandlerVO);
            exceptionPolicy = RESOURCE_ACCESS_ERROR;
        } else {
            exceptionHandlerVO.getException().printStackTrace();
            log.error("Alert!!!...:requestId: {}, exception with unknown reason, please trace with requestId : message header : {}",exceptionHandlerVO.getRequestId(), exceptionHandlerVO.getRequestId() );
            exceptionPolicy = UNKNOWN;
        }
        if (exceptionHandlerVO.isNeedToRetry() && exceptionHandlerVO.getRetryExceptionPolicies() != null && exceptionHandlerVO.getRetryExceptionPolicies().contains(exceptionPolicy)) {
            log.info("Retrying process due to error");
            throw new RetryException("Retry process", exceptionHandlerVO.getException());
        } else {
            if (exceptionHandlerVO.getSkipExceptionPolicies() == null || !exceptionHandlerVO.getSkipExceptionPolicies().contains(exceptionPolicy))
            addError(exceptionHandlerVO);
        }
    }








    private String getLastRequestId(List<String> requestIds) {
        return requestIds != null && requestIds.size() > 0 ? requestIds.get(requestIds.size()- 1) : "No request Id attached";
    }


    /**
     * @param exceptionHandlerVO
     * */
    private ExceptionPolicy handleHttpClientErrorException(ExceptionHandlerVO exceptionHandlerVO) {
        HttpClientErrorException httpClientErrorException = (HttpClientErrorException)exceptionHandlerVO.getException();
        // No data found for process, skip error
        if (httpClientErrorException.getStatusCode() != null && httpClientErrorException.getStatusCode() == HttpStatus.NOT_FOUND) {
            log.warn("requestId: {}, There is no data for processing", exceptionHandlerVO.getRequestId());
            return HTTP_CLIENT_ERROR_WITH_NOT_FOUND_STATUS;
        } else {
            log.error("requestId: {}, Error Message  : {}, status code : {}, cause : {}, response body: {} ", exceptionHandlerVO.getRequestId(), httpClientErrorException.getMessage(), httpClientErrorException.getStatusCode(), httpClientErrorException.getCause(), httpClientErrorException.getResponseBodyAsString());
            return HTTP_CLIENT_ERROR;
        }

    }

    /**
     * Exception can be cause of when an HTTP 5xx is received.
     *
     * @param exceptionHandlerVO
     * */
    private void handleHttpServerErrorException(ExceptionHandlerVO exceptionHandlerVO) {
        HttpServerErrorException h = (HttpServerErrorException)exceptionHandlerVO.getException();
        h.printStackTrace();
        log.error("HttpServerErrorException : requestId: {}, Statue : {}  Message  : {}, cause : {}", exceptionHandlerVO.getRequestId(), h.getStatusCode(),  h.getResponseBodyAsString(), h.getCause());
    }

    /**
     * Responsible for I/O exception, mostly can case of service down.
     *
     * @param exceptionHandlerVO
     * */
    private void handleResourceAccessException(ExceptionHandlerVO exceptionHandlerVO) {
        ResourceAccessException e = (ResourceAccessException)exceptionHandlerVO.getException();
        log.warn("requestId: {}, ResourceAccessException Error Message  : {}, cause : {}, stack trace: {} ",exceptionHandlerVO.getRequestId(), e.getMessage(), e.getCause(), e.getCause(), e.getStackTrace());
    }

    /**
     * Update Exchange
     *
     * @param exceptionHandlerVO
     * */
    private void addError(ExceptionHandlerVO exceptionHandlerVO) {
        exceptionHandlerVO.getExchange().getIn().setBody(exceptionHandlerVO.getException());
        exceptionHandlerVO.getExchange().getIn().setHeader(AppConstant.ERROR, true);
    }
}