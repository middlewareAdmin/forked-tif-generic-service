package com.mdw360.service.component.handler;

import com.mdw360.service.model.helper.ExceptionHandlerVO;

/*

* */
public interface ExceptionHandler {

    /**
     * Rest template can throws different exceptions in different scenarios, we need to skip same cases like HttpClientErrorException with NOT_FOUND status, there are also another scenario like ResourceAccessException which can be occur in case of service down
     * Main purpose of this component is handling exception and centralized rest template exception for different processor(as mentioned above) in one place
     * @param exceptionHandlerVO
     * */
    void handleTifException(ExceptionHandlerVO exceptionHandlerVO);


}
