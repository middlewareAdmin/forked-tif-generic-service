package com.mdw360.service.util.builders;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.File;

@Getter @Setter @Builder
public class FileValidatorHelperBuilder {

    private String requestId, siteId;

    private File file;

    private String error;

}
