package com.mdw360.service.util.builders;

import com.mdw360.service.util.baseutil.DateUtil;
import com.mdw360.service.util.enums.DataType;
import com.mdw360.service.util.exceptions.NotValidValueForColumnException;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.util.Map;

@Builder
@Getter
@Setter
@ToString
public class DataMapBuilder {

    private Map<String, String> map;

    private String value;

    private DataType dataType;

    private String destinationLocation;

    private String inputFormat;

    private String outputFormat;

    private boolean isStaticValue;

    private boolean trimSpaces;

    private boolean rightTrimSpaces;

    //As we saved all value in string filed in db services, so in this method we just validate the value type, and put validated value in map
    public void addValueDataMapValue() throws NotValidValueForColumnException {
        trimNonStringDataType();
        try {
            switch (dataType) {
                case INTEGER:
                    validateInteger();
                    break;
                case DECIMAL:
                    validateDouble();
                    break;
                case DATE:
                    if (!isStaticValue) {
                        value = DateUtil.getDateWithOutputFormat(value, inputFormat, outputFormat);
                    }
                    break;
                default:
                    trimIfRequiredForStringDataType();
            }
        } catch (ParseException | NumberFormatException e) {
            e.printStackTrace();
            throw new NotValidValueForColumnException(String .format("Exception occur during parse data %s", this.toString()));
        }
        map.put(destinationLocation, value);
    }

    // This method will validate the value also if format rquired if any changes then it will perform changes in value.
    public String validateAndGetValue() throws NotValidValueForColumnException {
        trimNonStringDataType();
        try {
            switch (dataType) {
                case INTEGER:
                    validateInteger();
                    break;
                case DECIMAL:
                    validateDouble();
                    break;
                case DATE:
                    value = DateUtil.getDateWithOutputFormat(value, inputFormat, outputFormat);
                    break;
                default:
                    trimIfRequiredForStringDataType();
            }

            return value;
        } catch (ParseException | NumberFormatException e) {
            throw new NotValidValueForColumnException(String.format("Exception occur during parse data, error: %s", e.getMessage()));
        }
    }

    // If value will not be a String type then this method will trim the value
    private void trimNonStringDataType() {
        if (dataType != DataType.STRING) {
            value = value.trim();
        }
    }

    // If value will be a String type then this method will trim the value based on trimSpaces config.
    private void trimIfRequiredForStringDataType() {
        if (dataType == DataType.STRING) {
            if (!isStaticValue && trimSpaces) {
                value = value.trim();
            }
        }
    }

    // Will throw NumberFormatException id value will not be a double type
    private void validateDouble() {
        if (StringUtils.isEmpty(value)) {
            return;
        }
        Double.parseDouble(value);
    }

    // Will throw NumberFormatException id value will not be a integer type
    private void validateInteger() {
        if (StringUtils.isEmpty(value)) {
            return;
        }
        Integer.parseInt(value);
    }

}
