package com.mdw360.service.util.builders;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class EnhancedDataFieldBuilder<SC, SF> {

    private SC siteXformFileConfiguration;

    private List<SF> sourceFieldList;

    private String requestId;

}
