package com.mdw360.service.util.annotations.validator;

import com.mdw360.service.util.annotations.R5TrackFiled;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ValidParameterValidator implements ConstraintValidator<R5TrackFiled, String> {

   private final static List<String> R5TRACK_FIELDS =
        Collections.unmodifiableList(Arrays.asList(    "transId",
                "tkdTrackDate",
                "tkdTrans",
                "created",
                "sourceSystem",
                "sourceCode",
                "promptData1",
                "promptData2",
                "promptData3",
                "promptData4",
                "promptData5",
                "promptData6",
                "promptData7",
                "promptData8",
                "promptData9",
                "promptData10",
                "promptData11",
                "promptData12",
                "promptData13",
                "promptData14",
                "promptData15",
                "promptData16",
                "promptData17",
                "promptData18",
                "promptData19",
                "promptData20",
                "promptData21",
                "promptData22",
                "promptData23",
                "promptData24",
                "promptData25",
                "promptData26",
                "promptData27",
                "promptData28",
                "promptData29",
                "promptData30",
                "promptData31",
                "promptData32",
                "promptData33",
                "promptData34",
                "promptData35",
                "promptData36",
                "promptData37",
                "promptData38",
                "promptData39",
                "promptData40",
                "promptData41",
                "promptData42",
                "promptData43",
                "promptData44",
                "promptData45",
                "promptData46",
                "promptData47",
                "promptData48",
                "promptData49",
                "promptData50",
                "promptData51",
                "sessionId",
                "rStatus",
                "changed"));

    @Override
    public void initialize(R5TrackFiled constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || R5TRACK_FIELDS.contains(value)) {
            return true;
        }
        return false;
    }

}
