package com.mdw360.service.util.annotations.validator;

import com.mdw360.service.util.annotations.Reserved;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ReservedValidator implements ConstraintValidator<Reserved, String> {

    public static final List<String> RESERVED_TOMS_DB_FIELDS =  Collections.unmodifiableList(Arrays.asList("promptData50", "promptData49"));

    @Override
    public void initialize(Reserved constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (RESERVED_TOMS_DB_FIELDS.contains(value)) {
            return false;
        }
        return true;
    }

}
