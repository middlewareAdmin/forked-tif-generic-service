package com.mdw360.service.util.annotations;


import com.mdw360.service.util.annotations.validator.RequiredValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = RequiredValidator.class)
@Target( { ElementType.FIELD,ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Required {

    String message() default "field required";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
