package com.mdw360.service.util.annotations.validator;

import com.mdw360.service.util.annotations.Required;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.util.Objects.isNull;

public class RequiredValidator implements ConstraintValidator<Required, Object> {

    @Override
    public void initialize(Required required) {

    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        if (isNull(o)) {
            return false;
        }
        return true;
    }

}
