package com.mdw360.service.util.annotations;

import com.mdw360.service.util.annotations.validator.ReservedValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ReservedValidator.class)
@Target( { ElementType.FIELD,ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Reserved {

    String message() default "destinationLocation must not be contains promptData49/promptData50";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
