package com.mdw360.service.util.annotations;

import com.mdw360.service.util.annotations.validator.ValidParameterValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ValidParameterValidator.class)
@Target( { ElementType.FIELD,ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface R5TrackFiled {

    String message() default "field must be r5track filed";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
