package com.mdw360.service.util.enums;

public enum  CustomResponseType {
    SUCCESS, INFO, WARNING, ERROR
}
