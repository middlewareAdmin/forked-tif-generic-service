package com.mdw360.service.util.enums;

public enum  ErrorSourceType {
    PAYLOAD_DATA_APP, DB_SERVICE_APP
}
