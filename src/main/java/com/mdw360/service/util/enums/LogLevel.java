package com.mdw360.service.util.enums;

public enum  LogLevel {
    WARN, INFO, ERROR
}
