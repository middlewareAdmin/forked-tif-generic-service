package com.mdw360.service.util.enums;

import java.util.EnumSet;
import java.util.Set;

public enum ExceptionPolicy {
    HTTP_CLIENT_ERROR, HTTP_CLIENT_ERROR_WITH_NOT_FOUND_STATUS, HTTP_SERVER_ERROR, RESOURCE_ACCESS_ERROR, UNKNOWN, NONE;

    public final static Set<ExceptionPolicy> ALL_EXCEPTION_POLICIES = EnumSet.of(HTTP_CLIENT_ERROR, HTTP_CLIENT_ERROR_WITH_NOT_FOUND_STATUS, HTTP_SERVER_ERROR, RESOURCE_ACCESS_ERROR, UNKNOWN);

    public static final boolean isContainsAnyPolicy(Set<ExceptionPolicy> policies) {
        for (ExceptionPolicy policy : policies) {
            boolean contains = ALL_EXCEPTION_POLICIES.contains(policy);
            if (contains) {
                return true;
            }
        }
        return false;
    }
}