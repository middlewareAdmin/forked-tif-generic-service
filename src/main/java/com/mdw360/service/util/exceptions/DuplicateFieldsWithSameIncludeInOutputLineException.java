package com.mdw360.service.util.exceptions;

public class DuplicateFieldsWithSameIncludeInOutputLineException extends RuntimeException {
    public DuplicateFieldsWithSameIncludeInOutputLineException(String s) {
        super(s);
    }
}
