package com.mdw360.service.util.exceptions;

public class NotValidElementException extends RuntimeException{
    public NotValidElementException() {
        super();
    }

    public NotValidElementException(String message) {
        super(message);
    }

    public NotValidElementException(Throwable cause) {
        super(cause);
    }
}
