package com.mdw360.service.util.exceptions;

public class ExceedIncludeInOutputLineException extends RuntimeException {
    public ExceedIncludeInOutputLineException(String s) {
        super(s);
    }
}
