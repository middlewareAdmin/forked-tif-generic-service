package com.mdw360.service.util.exceptions;

public class ExclusionValueFindException extends Exception {

    public ExclusionValueFindException() {
        super();
    }

    public ExclusionValueFindException(String message) {
        super(message);
    }

    protected ExclusionValueFindException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
