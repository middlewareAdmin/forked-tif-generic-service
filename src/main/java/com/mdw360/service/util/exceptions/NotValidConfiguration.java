package com.mdw360.service.util.exceptions;

public class NotValidConfiguration extends Exception {

    public NotValidConfiguration(String message) {
        super(message);
    }

    public NotValidConfiguration(String message, Throwable cause) {
        super(message, cause);
    }

}
