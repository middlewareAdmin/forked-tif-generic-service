package com.mdw360.service.util.exceptions;

public class NotValidValueForColumnException extends RuntimeException {

    public NotValidValueForColumnException(String message) {
        super(message);
    }

    public NotValidValueForColumnException(String message, Throwable cause) {
        super(message, cause);
    }

}
