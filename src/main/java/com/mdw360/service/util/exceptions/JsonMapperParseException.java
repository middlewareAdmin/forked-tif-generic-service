package com.mdw360.service.util.exceptions;

public class JsonMapperParseException extends Exception {

    public JsonMapperParseException(String message) {
        super(message);
    }

    public JsonMapperParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonMapperParseException(Throwable cause) {
        super(cause);
    }

}
