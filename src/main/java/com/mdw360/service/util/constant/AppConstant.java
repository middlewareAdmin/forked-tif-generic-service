package com.mdw360.service.util.constant;

public class AppConstant {

    //Restrict non-instantiable
    private AppConstant(){
        // Do nothing
    }

    public static final String ERROR = "error";
    public static final String SITE_SYSTEM_POSTFIX = "-FILE";
    public static final String BLANK = "";
    public static final String SPACE = " ";
    public static final String NEW_LINE = "\n";
    public static final String FORWARD_SLASH = "/";
    public static final String ROUTES_INFO = "routesInfo";
    public static final String REALM_NAME = "TIF";
    public static final String FORBIDDEN_ERROR_MESSAGE = "You don't have permission to access this uri";
    public static final String NOT_IMPLEMENTED_ERROR_MESSAGE = "The URL you have reached is not in service at this time (501).";
    public static final String ERROR_PATH = "/error";

    //HTML
    public static final String BREAK = "<br>";
    public static final String BODY_HEADER_TAG_LINE = "TOMS Integration Framework (TIF) - Meter Data Upload Summary";
    public static final String BODY_HEADER_DASH_LINE = "--------------------------------------------------------------------------------------";
    public static final String SITE_ID_IN_BOLD = "<b>SiteID:</b>";
    public static final String FILE_NAME_IN_BOLD = "<b>Filename:</b>";
    public static final String TRANSACTION_ID_IN_BOLD = "<b>TransactionID:</b>";
    public static final String FAILED_RECORD = "<b>Failed Records:</b>";
    public static final String SKIPPED_RECORD = "<b>Skipped Records:</b>";
    public static final String NUMBER_OF_SUCCESSFUL_UPLOADED_RECORD = "<b>Number of Successful Records Uploaded: </b>";
    public static final String NUMBER_OF_SUCCESSFUL_CONVERTED_RECORD = "<b>Number of Successful Converted Rows: </b>";
    public static final String TIF_UPLOAD_SUMMERY = "TIF - TOMS Upload Summary - ";


    //LOGGING
    public static final String LOGGING_DATA_SUCCESS = "Meter Data Summary - Success";
    public static final String LOGGING_DATA_FAILED = "Meter Data Summary - Failure";
    public static final String LOGGING_DASH = " - ";
    public static final String LOGGING_SITE_ID = "SiteID: ";
    public static final String LOGGING_TRANSACTION_ID = "Transaction ID: ";
    public static final String LOGGING_FILENAME = "Filename: ";
    public static final String LOGGING_NUMBER_OF_SUCCESSFUL_UPLOADED_RECORD = "Number of Successful Records Uploaded: ";
    public static final String LOGGING_NUMBER_OF_SUCCESSFUL_CONVERTED_RECORD = "Number of Successful Converted Rows: ";
    public static final String LOGGING_NUMBER_OF_FAILED_RECORD = "Number of Failed Records: ";
    public static final String LOGGING_NUMBER_OF_SKIPPED_RECORD = "Number of Skipped Records: ";
    public static final String LOGGING_FAILED_RECORDS = "Failed Records: ";
    public static final String LOGGING_SKIPPED_RECORDS = "Skipped Records: ";

    // Processor header helper
    public static final String OK = "OK";
    public static final String STATUS = "status";

    public static final String EVENT_TYPE = "eventType";
    public static final String IS_DATA_AVAILABLE = "isDataAvailable";

    // File Temp Name Generator helper
    public static final String FILE_NAME_SPLITTER_IDENTIFIER = "___";

    public static final String ERROR_FILE_EXT = ".err";

    public static final String TEST_PROFILE = "test";

    public static final String FIXED_WIDTH = "FIXED_WIDTH";
    public static final String CSV = "CSV";
    public static final String EXCEL = "EXCEL";

    public static final String FILE_TYPE = "fileType";
    public static final String SITE_NAME = "siteName";
    public static final String SITE_ID = "siteId";
    public static final String SITE_XFORM_FILE_CONFIGURATION = "siteXformFileConfigurations";

}
