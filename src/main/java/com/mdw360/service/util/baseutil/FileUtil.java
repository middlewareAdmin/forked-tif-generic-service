package com.mdw360.service.util.baseutil;

import com.mdw360.service.util.enums.DataType;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class FileUtil {

    //Restrict non-instantiable
    private FileUtil(){
        // Do nothing
    }

    private static final List<String> DAYS = Arrays.asList("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT");
    private static final Pattern VALID_FILE_NAME_REGEX =
            Pattern.compile("\\.[*a-zA-Z]*$");

    /**
    * @fileName :As per current file validation file extension end with . with set of letters from A to Z(CASE INSENSITIVE)
    *            and before . it can be any any set of letters or number or _ or % or * , ex. *.txt, myfile.txt
    * @return   : true if matched file pattern else false.
    * */
    public static boolean validateFileName(String fileName) {
        Matcher matcher = VALID_FILE_NAME_REGEX .matcher(fileName);
        return matcher.find();
    }

    /**
    * @value : value of staticValue
    * @dataType : dataType of staticValue
    * @return true if value is valid for dataType else false.
    * */
    public static boolean validValueForStaticValue(String value, DataType dataType) {
        return isValidDataValue(value, dataType, null);
    }

    /**
    * @param  value
    * @param  dataType
    * @param  inputFormat
    * @return boolean  false : if value will not valid based on Data type else true
    * */
    public static boolean isValidDataValue(String value, DataType dataType, String inputFormat) {
        if (dataType != DataType.STRING) {
            value = value.trim();
        }
        try {
            switch (dataType) {
                case INTEGER:
                    Integer.parseInt(value);
                    return true;
                case DECIMAL:
                    Double.parseDouble(value);
                    return true;
                case DATE:
                    return DateUtil.isValidDateFormatPerDateValue(value, inputFormat);
                case STRING:
                    return true;
                default:
                    return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
    * @param leftTrimSpaces
    * @param rightTrimSpaces
    * @param value
    * @return  String with left/right trim if required
    * */
    public static String trimSpaceIfRequired(Boolean leftTrimSpaces, Boolean rightTrimSpaces, String value) {
        if (value == null) {
            return value;
        }
        if (leftTrimSpaces != null && leftTrimSpaces) {
            value = value.replaceAll("^\\s*", "");
        }
        if (rightTrimSpaces != null && rightTrimSpaces) {
            value = value.replaceAll("\\s+$", "");
        }
        return value;
    }

    public static boolean match(String text, String pattern) {
        return text.matches(pattern.replace("?", ".?").replace("*", ".*?"));
    }


    /**
    * @param siteRawLineMessageFileName
    * @param pattern
    * @return  boolean if filename match with config file name pattern return true else false
    * */
    public static boolean isValidSiteFileConfiguration(String siteRawLineMessageFileName,String pattern) {
        return match(siteRawLineMessageFileName.toLowerCase(), pattern.toLowerCase());
    }

    /**
     * Create a deep copy of any object that is passed.
     * @param object
     * @return Object
     */
    public static Object deepCopy(Object object) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ObjectOutputStream outputStrm = new ObjectOutputStream(outputStream);
            outputStrm.writeObject(object);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            ObjectInputStream objInputStream = new ObjectInputStream(inputStream);
            return objInputStream.readObject();
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
