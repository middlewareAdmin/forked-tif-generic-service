package com.mdw360.service.util.baseutil;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

public class ExceptionUtil {
    //Restrict non-instantiable
    private ExceptionUtil(){
        // Do nothing
    }

    public static void buildExceptionErrorMessage(Exception e, StringBuilder errorMessageBuilder) {
        if(e instanceof HttpClientErrorException) {
            HttpClientErrorException hce = (HttpClientErrorException)e;
            errorMessageBuilder.append(" ,Error Status code:" + (hce.getStatusCode() != null ? hce.getStatusCode() : "Not present"))
                    .append(", error message: ")
                    .append(hce.getResponseBodyAsString());
        } else if (e instanceof ResourceAccessException) {
            ResourceAccessException hse = (ResourceAccessException)e;
            errorMessageBuilder.append(", error message: ")
                    .append(hse.getMessage());
        } else if (e instanceof HttpServerErrorException) {
            HttpServerErrorException hse = (HttpServerErrorException)e;
            errorMessageBuilder.append(" ,Error Status code:" + (hse.getStatusCode() != null ? hse.getStatusCode() : "Not present"))
                    .append(",error message : ")
                    .append(hse.getResponseBodyAsString());
        } else {
            errorMessageBuilder.append(", error message :").append(e.getMessage() != null ? e.getMessage() : "Not available");
        }
    }

}
