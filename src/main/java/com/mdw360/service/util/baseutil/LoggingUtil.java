package com.mdw360.service.util.baseutil;

import com.mdw360.service.util.enums.LogLevel;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;

@Slf4j
public class LoggingUtil {

    public static void printUnexpectedError(LogLevel logLevel, Exchange exchange, String customErrorMessage, Exception causedBy, String requestId, String writeContent) {

       if (logLevel == LogLevel.WARN) {
           log.warn(buildMessage(exchange, customErrorMessage, causedBy, requestId, writeContent), causedBy);
       } else if (logLevel == LogLevel.ERROR) {
           log.error(buildMessage(exchange, customErrorMessage, causedBy, requestId, writeContent), causedBy);
       }  else if (logLevel == LogLevel.INFO) {
           log.info(buildMessage(exchange, customErrorMessage, causedBy, requestId, writeContent), causedBy);
       } else {
           log.debug(buildMessage(exchange, customErrorMessage, causedBy, requestId, writeContent), causedBy);
       }

    }


    private static String buildMessage(Exchange exchange, String customErrorMessage, Exception causedBy, String requestId, String writeContent) {
        return  "\n---------------------------------------------------------------------------------------------------------------------------------------\n" +
                "requestId :"+requestId+"\n" +
                "Custom error message :"+customErrorMessage+"\n" +
                "Exception message :"+causedBy.getMessage()+"\n" +
                "Exchange route id :"+exchange.getFromRouteId() +
                "\ncaused: "+causedBy.getMessage()+
                "\nwrite content: "+writeContent+
                "\n---------------------------------------------------------------------------------------------------------------------------------------\n";
    }
}
