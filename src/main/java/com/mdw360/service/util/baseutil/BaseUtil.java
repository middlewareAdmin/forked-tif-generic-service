package com.mdw360.service.util.baseutil;

import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.apache.tomcat.util.codec.binary.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.io.*;
import java.nio.charset.Charset;


public class BaseUtil {

    //Restrict non-instantiable
    private BaseUtil(){
        // Do nothing
    }

    private static final String AUTH_ENCODING = "iso-8859-1";

    /**
     * @param inputStream
     * @return  String encoded string
     * */
    public static String encodeToString(InputStream inputStream) throws IOException {
       byte[] fileBytes =  IOUtils.toByteArray(inputStream);
        return StringUtils.newStringUtf8(Base64.encodeBase64(fileBytes));
    }

    /**
     * @param bytes
     * @return  String encoded string
     * */
    public static String encodeToString(byte[] bytes) {
        return StringUtils.newStringUtf8(Base64.encodeBase64(bytes));
    }

    /**
    * @param base64String
    * @param fileName
    * */
    public static void decodeToFileFromEncodeString(String base64String, String fileName) throws FileNotFoundException, IOException {
        byte base64DecodedByteArray[] = Base64.decodeBase64(base64String);
        FileOutputStream imageOutFile = new FileOutputStream(fileName);
        imageOutFile.write(base64DecodedByteArray);
        imageOutFile.close();
    }

    /*
    * @param String[]
    * @return concatenations of arg
    * */
    public static String concatArgs(String... args) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String path : args) {
            stringBuilder.append(path);
        }
        return stringBuilder.toString();
    }

    /**
    * @param username
    * @param password
    * @return HttpHeaders with basic auth
    * */
    public static HttpHeaders createHeaderWithBasicAuth(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName(AUTH_ENCODING)) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }

    /**
     * @param username
     * @param password
     * @return HttpHeaders with basic auth and content type json
     * */
    public static HttpHeaders createHeaderWithBasicAuthAndJsonContentType(String username, String password){
        HttpHeaders httpHeaders = createHeaderWithBasicAuth(username, password);
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }

    /**
     * @param encodedString
     * @return InputStream : decoded output in InputStream
     * */
    public static InputStream decodeAndConvertToFileInputStream(String encodedString){
        byte[] decoded = Base64.decodeBase64(encodedString);
        return new ByteArrayInputStream(decoded);
    }

    /**
     * @param encodedString
     * @return String : decoded string output of encoded Base64format
     * */
    public static String decodeToString(String encodedString) {
        byte[] decoded = Base64.decodeBase64(encodedString);
        return new String(decoded);
    }


    /**
     * @param isFileSeparatorRequiredAtTheEnd
     * @param paths
     * @throws IllegalArgumentException
     * */
    public static String formatFolderPath(boolean isFileSeparatorRequiredAtTheEnd, String... paths) {
        Preconditions.checkArgument(paths != null && paths.length > 0 , "Path can't be null or length can't be zero");
        StringBuilder stringBuilder = new StringBuilder("");
        for (String path: paths) {
            if (path != null && !path.equals("")) {
                stringBuilder.append(path);
                if (!path.endsWith(File.separator)) {
                    stringBuilder.append(File.separator);
                }
            }
        }

        if (stringBuilder.length() > 0 && !isFileSeparatorRequiredAtTheEnd) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }

        return stringBuilder.toString();
    }

    public static boolean isNumber(String input) {
        try {
            Integer.parseInt(input);
            return true;
        }
        catch(NumberFormatException ex) {
            return false;
        }
    }
}
