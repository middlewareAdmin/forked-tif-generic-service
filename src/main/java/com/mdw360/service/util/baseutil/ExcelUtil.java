package com.mdw360.service.util.baseutil;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tika.Tika;
import org.apache.tika.mime.MediaType;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
public class ExcelUtil {

    private static final List<String> EXCEL_CONTENT_TYPES = Arrays.asList("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel");

    private static final String XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    private static final String XLS = "application/vnd.ms-excel";

    //Restrict non-instantiable
    private ExcelUtil(){
        // Do nothing
    }

    public static String getCellValue(Row row, String column, boolean isCheckForRowEmpty, boolean isDateFiled, String inputFormat) {
        Cell cell = null;
        if (BaseUtil.isNumber(column)) {
            int cellNumber = isCheckForRowEmpty ? Integer.parseInt(column) : Integer.parseInt(column) - 1;
            cell = row.getCell(cellNumber, Row.CREATE_NULL_AS_BLANK);
        } else {
            CellReference ref = new CellReference(column);
            cell = row.getCell(ref.getCol(), Row.CREATE_NULL_AS_BLANK);
        }

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                return String.valueOf(cell.getStringCellValue());

            case Cell.CELL_TYPE_BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());

            case Cell.CELL_TYPE_NUMERIC:
                if (isDateFiled && DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue() != null ? convertDateWithGiveFormat(cell.getDateCellValue(), inputFormat) : null;
                }
                return String.valueOf(cell.getNumericCellValue());

            case Cell.CELL_TYPE_BLANK:
                return "";
            default:
                log.error("Cell type {} not supported", cell.getCellType());
                return null;
        }
    }

    private static String convertDateWithGiveFormat(Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static Workbook getWorkbook(String fileName, InputStream decodedInputStream) {
        try {
            Tika tika = new Tika();
            MediaType mediaType = MediaType.parse(tika.detect(decodedInputStream, fileName));

            String type = mediaType != null ? mediaType.toString().trim() : null;

            if (EXCEL_CONTENT_TYPES.contains(type)) {
                if (XLSX.equalsIgnoreCase(type)) {
                    return new XSSFWorkbook(decodedInputStream);
                } else {
                    return new HSSFWorkbook(decodedInputStream);
                }
            } else {
                throw new RuntimeException(String.format("Excel content type only support (%s) and given type is %s", EXCEL_CONTENT_TYPES, type));
            }

        } catch (IOException e) {
            log.error("Could not detect content type", e);
            throw new RuntimeException(String.format("Could not detect content type for file %s", fileName));

        }
    }

    public static boolean isRowEmpty(Row row) {
        if (row == null) {
            return true;
        }
        boolean isEmptyRow = true;
        for(int cellNum = row.getFirstCellNum(); cellNum < row.getLastCellNum(); cellNum++){
            String cellValue = getCellValue(row, String.valueOf(cellNum), true, false, null);
            if(!StringUtils.isEmpty(cellValue)){
                isEmptyRow = false;
            }
        }
        return isEmptyRow;
    }
}
