package com.mdw360.service.util.baseutil;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    //Restrict non-instantiable
    private DateUtil(){
        // Do nothing
    }

    /*
    * @dateFormatPattern Date format
    * @return true if valid date format else return false
    * */
    public static boolean isValidDateFormatPattern(String dateFormatPattern) {
        try {
            final DateFormat format = new SimpleDateFormat(dateFormatPattern);
            format.format(new Date());
        } catch (IllegalArgumentException ex) {
            return false;
        }
        return true;
    }


    /*
    * @dateFormatPattern Date value
    * @dateFormat Date format
    * @return true if valid date format and value else return false
    * */
    public static boolean isValidDateFormatPerDateValue(String value, String dateFormat) {
        try {
            DateFormat format = new SimpleDateFormat(dateFormat);
            format.parse(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
    * @param dateInStringFormat
    * @param inputFormat
    * @param outputFormat
    * @return  String data format based on outputFormat
    * */
    public static String getDateWithOutputFormat(String dateInStringFormat, String inputFormat, String outputFormat) throws ParseException {
        if (StringUtils.isEmpty(dateInStringFormat)) {
            return null;
        }
        DateFormat originalFormat = new SimpleDateFormat(inputFormat);
        Date date = originalFormat.parse(dateInStringFormat);
        DateFormat targetFormat = new SimpleDateFormat(outputFormat);
        return targetFormat.format(date);
    }

    /**
    * @param format
    * @throws  Exception id date format is not valid
    * */
    public static boolean isValidDateFormat(String format) {
        try {
            new SimpleDateFormat (format);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
    * @param fileName
    * @return String  yyyy- fileName - EEE MMM dd HH:mm:ss zzz format
    * */
    public static String getEmailSubjectDateFormat(String fileName) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(String.format("yyyy-'%s - 'EEE MMM dd HH:mm:ss zzz", fileName));
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String getTimestampForProcessedFiles(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");
        return sdf.format(new Date());
    }
}
