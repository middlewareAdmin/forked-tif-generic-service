package com.mdw360.service.transform;

import com.google.gson.Gson;
import com.mdw360.service.model.dbservices.R5TrackCO;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("r5TrackProcessorToJson")
@Slf4j
public class R5TrackProcessorToJson implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        List<R5TrackCO> r5TrackCOList = (List<R5TrackCO>) exchange.getIn().getBody();
        Gson gson = new Gson();
        String jsonResult = gson.toJson(r5TrackCOList);
        exchange.getIn().setBody(jsonResult);
        log.info("JSON RESULT:", jsonResult);
    }
}
