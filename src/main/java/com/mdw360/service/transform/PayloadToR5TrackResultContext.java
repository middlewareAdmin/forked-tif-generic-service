package com.mdw360.service.transform;

import com.mdw360.service.model.meterdata.configuration.R5TrackResultContext;

public interface PayloadToR5TrackResultContext {

    /**
     * Implement transforms String Payload to a R5TrackResultContext after transforming the payload into a List of R5TrackCO 's
     * @param payload
     *  - tkdTrans is the stored procedure name in toms to process the data from r5 table
     * @return
     */
    R5TrackResultContext transform(String payload, String tkdTrans) throws Exception;
}
