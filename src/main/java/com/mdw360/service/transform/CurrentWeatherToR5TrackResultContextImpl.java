package com.mdw360.service.transform;

import com.google.gson.Gson;
import com.mdw360.service.model.dbservices.R5TrackCO;
import com.mdw360.service.model.domain.common.Clouds;
import com.mdw360.service.model.domain.common.Coord;
import com.mdw360.service.model.domain.common.Wind;
import com.mdw360.service.model.domain.current.CurrentWeather;
import com.mdw360.service.model.domain.current.Main;
import com.mdw360.service.model.domain.current.Sys;
import com.mdw360.service.model.meterdata.configuration.ErrorContext;
import com.mdw360.service.model.meterdata.configuration.R5TrackResultContext;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class CurrentWeatherToR5TrackResultContextImpl implements PayloadToR5TrackResultContext {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:MM:SS");
    /**
     * Transforms String Payload to a R5TrackResultContext after transforming the payload into a List of R5TrackCO 's
     *
     * @param payload
     * @return
     */
    public R5TrackResultContext transform(String payload, String tkdTrans) throws Exception {

        //Init inner elements
        List<ErrorContext> errorsResult = new ArrayList<>();
        List<ErrorContext> skipsResult = new ArrayList<>();
        List<R5TrackCO> successResult = new ArrayList<>();
        R5TrackCO r5TrackCO = new R5TrackCO();
        Gson gson = new Gson();
        CurrentWeather currentWeather = gson.fromJson(payload, CurrentWeather.class);

        log.debug("Current Weather: {}", currentWeather.toString());
        mapWind(r5TrackCO, currentWeather.getList().get(0).getWind());
        mapClouds(r5TrackCO, currentWeather.getList().get(0).getClouds());
        mapTemp(r5TrackCO, currentWeather.getList().get(0).getMain());
        mapVisibility(r5TrackCO, currentWeather.getList().get(0).getVisibility());
        mapDateTime(r5TrackCO, currentWeather.getList().get(0).getDt());
        mapCoordElements(r5TrackCO, currentWeather.getList().get(0).getCoord());
        mapSyslements(r5TrackCO, currentWeather.getList().get(0).getSys());
        mapWeather(r5TrackCO, currentWeather.getList().get(0));
        mapNameAndId(r5TrackCO, currentWeather.getList().get(0));

        r5TrackCO.setPromptData50("CURRENT_WEATHER");
        r5TrackCO.setTkdTrans(tkdTrans);  //example: KTW (stored procedure to process records)
        successResult.add(r5TrackCO);

        R5TrackResultContext contextInstance = new R5TrackResultContext(errorsResult, skipsResult, successResult);
        return contextInstance;
    }

    private void mapClouds(R5TrackCO r5TrackCO, Clouds cloudsInstance) {
        r5TrackCO.setPromptData19(cloudsInstance.getAll());
        /* mapping document
          |clounds            |promptData19|
        */
    }

    private void mapWind(R5TrackCO r5TrackCO, Wind windInstance) {
        r5TrackCO.setPromptData17(windInstance.getSpeed());
        r5TrackCO.setPromptData18(windInstance.getDeg());

        /* mapping document
          |wind speed         |promptData17|
          |wind deg           |promptData18|
        */
    }

    private void mapTemp(R5TrackCO r5TrackCO, Main main) {
        r5TrackCO.setPromptData10(main.getTemp());
        r5TrackCO.setPromptData11(main.getFeelsLike());
        r5TrackCO.setPromptData12(main.getTempMin());
        r5TrackCO.setPromptData13(main.getTempMax());
        r5TrackCO.setPromptData14(main.getPressure());
        r5TrackCO.setPromptData15(main.getHumidity());

        /* mapping document
         |temp               |promptData10|
         |feels_like         |promptData11|
         |temp_min           |promptData12|
         |temp_max           |promptData13|
         |pressure           |promptData14|
         |humidity           |promptData15|
        */
    }

    private void mapVisibility(R5TrackCO r5TrackCO, String visibility) {
        r5TrackCO.setPromptData16(visibility);
        /* mapping document
        |visibility         |promptData16|
         */
    }

    private void mapDateTime(R5TrackCO r5TrackCO, long dt) {
        r5TrackCO.setPromptData20(unixTimeToStandardUTCString(dt));
        /* mapping document
         - Unix Date Time
        |dt                 |promptData20|
         */
    }

    private void mapNameAndId(R5TrackCO r5TrackCO, com.mdw360.service.model.domain.current.List list) {
        r5TrackCO.setPromptData21(list.getId());
        r5TrackCO.setPromptData22(list.getName());
        /*
        |id                 |promptData21|
        |name               |promptData22| Location Name |
         */
    }

    private void mapWeather(R5TrackCO r5TrackCO, com.mdw360.service.model.domain.current.List list) {
        r5TrackCO.setPromptData7(list.getWeather().get(0).getId());
        r5TrackCO.setPromptData8(list.getWeather().get(0).getMain());
        r5TrackCO.setPromptData9(list.getWeather().get(0).getDescription());

        /* Mapping Document
        |weather id         |promptData7 |
        |weather main       |promptData8 |
        |weather description|promptData9 |
         */
    }

    private void mapCoordElements(R5TrackCO r5TrackCO, Coord coordInstance) {
        r5TrackCO.setPromptData1(coordInstance.getLon());
        r5TrackCO.setPromptData2(coordInstance.getLat());

        /* Mapping Document
        |lon                |promptData1 | Longitude|
        |lat                |promptData2 | Latitude |
         */
    }

    private void mapSyslements(R5TrackCO r5TrackCO, Sys sysInstance) {
        r5TrackCO.setPromptData3(sysInstance.getCountry());
        r5TrackCO.setPromptData4(sysInstance.getTimezone());
        r5TrackCO.setPromptData5(unixTimeToStandardUTCString(sysInstance.getSunrise()));
        r5TrackCO.setPromptData6(unixTimeToStandardUTCString(sysInstance.getSunset()));
        /*
        |country            |promptData3 |
        |timezone           |promptData4 |
        |sunrise            |promptData5 |
        |sunset             |promptData6 |
         */
    }

    /**
     * Utility to convert from the Unix UTC Long value to the output format expected by TOMS
     *
     * @param unixUtcRaw
     * @return
     */
    private String unixTimeToStandardUTCString(Long unixUtcRaw) {
        Instant nowUtc = Instant.ofEpochSecond(unixUtcRaw);
        ZoneId vancouver = ZoneId.of("America/Vancouver");
        ZonedDateTime nowVancouver = ZonedDateTime.ofInstant(nowUtc, vancouver);
        return nowVancouver.format(formatter);
    }
}
