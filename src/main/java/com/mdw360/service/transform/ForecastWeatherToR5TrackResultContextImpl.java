package com.mdw360.service.transform;

import com.google.gson.Gson;
import com.mdw360.service.model.dbservices.R5TrackCO;
import com.mdw360.service.model.domain.common.Clouds;
import com.mdw360.service.model.domain.common.Coord;
import com.mdw360.service.model.domain.common.Weather;
import com.mdw360.service.model.domain.common.Wind;
import com.mdw360.service.model.domain.forecast.City;
import com.mdw360.service.model.domain.forecast.ForecastWeather;
import com.mdw360.service.model.domain.forecast.Main;
import com.mdw360.service.model.domain.forecast.Snow;
import com.mdw360.service.model.meterdata.configuration.ErrorContext;
import com.mdw360.service.model.meterdata.configuration.R5TrackResultContext;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
public class ForecastWeatherToR5TrackResultContextImpl implements PayloadToR5TrackResultContext {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:MM:SS");

    /**
     * Transforms String Payload to a R5TrackResultContext after transforming the payload into a List of R5TrackCO 's
     *
     * @param payload
     * @return
     */
    public R5TrackResultContext transform(String payload, String tkdTrans) throws Exception {

        //Init inner elements
        List<ErrorContext> errorsResult = new ArrayList<>();
        List<ErrorContext> skipsResult = new ArrayList<>();
        List<R5TrackCO> successResult = new ArrayList<>();

        Gson gson = new Gson();
        ForecastWeather forecastWeather = gson.fromJson(payload, ForecastWeather.class);
        log.info("Forecast Weather: {}", forecastWeather.toString());

        for (com.mdw360.service.model.domain.forecast.List list : forecastWeather.getList()) {
            R5TrackCO r5TrackCO = new R5TrackCO();
            //Map Root Elements
            mapCoordElements(r5TrackCO, forecastWeather.getCity().getCoord());
            mapCity(r5TrackCO, forecastWeather.getCity());
            mapWeather(r5TrackCO, list.getWeather().get(0));
            mapWind(r5TrackCO, list.getWind());
            mapClouds(r5TrackCO, list.getClouds());
            mapDateTime(r5TrackCO, list.getDtTxt());
            mapTemp(r5TrackCO, list.getMain());
            mapSnow(r5TrackCO, list.getSnow());
            r5TrackCO.setPromptData50("FORECAST_WEATHER");
            r5TrackCO.setTkdTrans(tkdTrans);  //example: KTW (stored procedure to process records)
            successResult.add(r5TrackCO);
        }

        R5TrackResultContext contextInstance = new R5TrackResultContext(errorsResult, skipsResult, successResult);
        return contextInstance;
    }

    private void mapSnow(R5TrackCO r5TrackCO, Snow snow) {
        if(snow!=null) {
            r5TrackCO.setPromptData26(snow.get_3h());
        }

        /* mapping document
        |snow 3h            |promptData26|
         */
    }

    private void mapCity(R5TrackCO r5TrackCO, City city) {
        r5TrackCO.setPromptData21(city.getId());
        r5TrackCO.setPromptData22(city.getName());
        r5TrackCO.setPromptData3(city.getCountry());
        r5TrackCO.setPromptData4(city.getTimezone());
        r5TrackCO.setPromptData5(unixTimeToStandardUTCString(city.getSunrise()));
        r5TrackCO.setPromptData6(unixTimeToStandardUTCString(city.getSunset()));

        /* mapping document
        |id                 |promptData21|
        |name               |promptData22| Location Name |

        |country            |promptData3 |
        |timezone           |promptData4 |
        |sunrise            |promptData5 |
        |sunset             |promptData6 |
         */
    }


    private void mapWind(R5TrackCO r5TrackCO, Wind wind) {
        r5TrackCO.setPromptData17(wind.getSpeed());
        r5TrackCO.setPromptData18(wind.getDeg());

        /* mapping document
          |wind speed         |promptData17|
          |wind deg           |promptData18|
        */
    }

    private void mapClouds(R5TrackCO r5TrackCO, Clouds cloudsInstance) {
        r5TrackCO.setPromptData19(cloudsInstance.getAll());
        /* mapping document
          |clounds            |promptData19|
        */
    }

    private void mapTemp(R5TrackCO r5TrackCO, Main main) {
        r5TrackCO.setPromptData10(main.getTemp());
        r5TrackCO.setPromptData11(main.getFeelsLike());
        r5TrackCO.setPromptData12(main.getTempMin());
        r5TrackCO.setPromptData13(main.getTempMax());
        r5TrackCO.setPromptData14(main.getPressure());
        r5TrackCO.setPromptData15(main.getHumidity());
        r5TrackCO.setPromptData23(main.getGrndLevel());
        r5TrackCO.setPromptData24(main.getSeaLevel());
        r5TrackCO.setPromptData25(main.getTempKf());

        /* mapping document
         |temp               |promptData10|
         |feels_like         |promptData11|
         |temp_min           |promptData12|
         |temp_max           |promptData13|
         |pressure           |promptData14|
         |humidity           |promptData15|

         |sea_level          |promptData23|
         |grnd_level         |promptData24|
         |temp_kf            |promptData25|
        */
    }

    private void mapDateTime(R5TrackCO r5TrackCO, String dt_txt) {
        r5TrackCO.setPromptData20(dt_txt);
        /* mapping document
        |dt                 |promptData20|
         */
    }

    private void mapWeather(R5TrackCO r5TrackCO, Weather weatherInstance) {
        r5TrackCO.setPromptData7(weatherInstance.getId());
        r5TrackCO.setPromptData8(weatherInstance.getMain());
        r5TrackCO.setPromptData9(weatherInstance.getDescription());

        /* Mapping Document
        |weather id         |promptData7 |
        |weather main       |promptData8 |
        |weather description|promptData9 |
         */
    }

    private void mapCoordElements(R5TrackCO r5TrackCO, Coord coordInstance) {
        r5TrackCO.setPromptData1(coordInstance.getLon());
        r5TrackCO.setPromptData2(coordInstance.getLat());

        /* Mapping Document
        |lon                |promptData1 | Longitude|
        |lat                |promptData2 | Latitude |
         */
    }

    /**
     * Utility to convert from the Unix UTC Long value to the output format expected by TOMS
     *
     * @param unixUtcRaw
     * @return
     */
    private String unixTimeToStandardUTCString(Long unixUtcRaw) {
        Instant nowUtc = Instant.ofEpochSecond(unixUtcRaw);
        ZoneId vancouver = ZoneId.of("America/Vancouver");
        ZonedDateTime nowVancouver = ZonedDateTime.ofInstant(nowUtc, vancouver);
        return nowVancouver.format(formatter);
    }

}
