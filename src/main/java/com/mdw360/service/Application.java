package com.mdw360.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

	private static final Logger LOG = LoggerFactory.getLogger(Application.class);

	@Value("${rest.host}") String restHost;
	@Value("${server.port}") String serverPort;
	@Value("${context-path}") String contextPath;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

	}

	@PostConstruct
	void postConstruct(){
		LOG.info(
"\n*************************************************************************************************\n"+
"*************************************************************************************************\n"+
		"\n"+
"HOST / PORT CONFIGURATIONS:\n"+
	"( {}:{} ) Application, Swagger, Health URLs\n"+
		"\n"+
		"\n"+
"- Spring Framework Metrics:\n"+
    "  - http://{}:{}/env\n"+
    "  - http://{}:{}/mappings\n"+
    "  - http://{}:{}/configprops\n"+
    "\n"+
"HEALTH / ACTUATOR / CAMEL ENDPOINTS\n"+
"The following health URL's are available \n"+
		"\n"+
"- Application Health\n"+
	"http://{}:{}/health\n"+
		"\n"+
"- Beans Loaded\n"+
	"http://{}:{}/beans\n"+
		"\n"+
"- Camel Routes:\n"+
	"http://{}:{}/camel/routes\n"+
	"http://{}:{}/camel/routes/{route_id}/info\n"+
	"http://{}:{}/camel/routes/{route_id}/detail\n"+
		"\n"+
"*************************************************************************************************\n"+
"*************************************************************************************************",
    restHost, serverPort,
    restHost, serverPort, restHost, serverPort, restHost, serverPort,
    restHost, serverPort, restHost, serverPort,
    restHost, serverPort, restHost, serverPort, restHost, serverPort );

	}

}
