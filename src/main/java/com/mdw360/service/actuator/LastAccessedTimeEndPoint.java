package com.mdw360.service.actuator;


import com.mdw360.service.component.DateHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class LastAccessedTimeEndPoint implements Endpoint<Map<String, String>> {

    private final static String LAST_ACCESSED_TIME = "lastAccessedTime";
    private final static String NOT_CONNECTED_YET = "Not connected yet";

    @Autowired
    private DateHolder dateHolder;

    @Override
    public String getId() {
        return LAST_ACCESSED_TIME;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isSensitive() {
        return false;
    }

    @Override
    public Map<String, String> invoke() {

        Date lastAccessedOfDb= dateHolder.getLastAccessTimeOfDBServiceConnection();
        Date lastAccessedOfEmail = dateHolder.getLastAccessTimeOfEmailServiceConnection();
        Date lastAccessedOfSftp = dateHolder.getLastAccessTimeOfSftpServiceConnection();

        Map<String, String> map = new HashMap<String, String>();

        map.put("Date of last connection to Database Service", lastAccessedOfDb == null ? NOT_CONNECTED_YET : lastAccessedOfDb.toString());
        map.put("Date of last connection to SFTP Service", lastAccessedOfSftp == null ? NOT_CONNECTED_YET : lastAccessedOfSftp.toString());
        map.put("Date of last connection to Email Service", lastAccessedOfEmail == null ? NOT_CONNECTED_YET : lastAccessedOfEmail.toString());
        return map;
    }

}
