package com.mdw360.service.actuator;

import com.mdw360.service.util.baseutil.BaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@Component
@Slf4j
public class HealthService extends AbstractHealthIndicator {

    @SuppressWarnings("unused")
    @Value("${security.user.name}")
    private String userName;

    @SuppressWarnings("unused")
    @Value("${security.user.password}")
    private String password;

    @SuppressWarnings("unused")
    @Value("${db.service.service.ping.url}")
    private String dbServicePingUrl;

    @SuppressWarnings("unused")
    @Autowired
    private RestTemplate restTemplate;

    private boolean initialCall = true;
    private Map<ServiceType, Date> lastCheck;
    private SimpleDateFormat dateFormat;

    @PostConstruct
    protected void init() {
        lastCheck = new HashMap<>();
        for (ServiceType serviceType : ServiceType.ALL) {
            lastCheck.put(serviceType, new Date());
        }
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm z");
    }

    @Override
    protected void doHealthCheck(Health.Builder builder) {
        Set<ServiceType> serviceTypes = ServiceType.ALL;
        createHealthInfo(serviceTypes, builder);
    }

    private void createHealthInfo(Set<ServiceType> serviceTypes, Health.Builder builder) {
        final AtomicLong successCallCounter = new AtomicLong(0);
        serviceTypes.forEach(serviceType -> {
            HealthDataInfo healthDataInfo = new HealthDataInfo();
            healthDataInfo.serviceType = serviceType;
            try {
                String url = getUrl(serviceType);
                HttpHeaders httpHeaders = BaseUtil.createHeaderWithBasicAuth(userName, password);
                HttpEntity entity = new HttpEntity(httpHeaders);
                ResponseEntity<String> output = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
                assert output.getBody().equals("OK");
                lastCheck.put(serviceType, new Date());
                healthDataInfo.isUP = true;
                healthDataInfo.message = String.format("Successfully connected with %s (%s)",
                        serviceType.name(),
                        dateFormat.format(lastCheck.get(serviceType)));

                successCallCounter.incrementAndGet();
            } catch (Exception e) {
                log.error("Exception occurred during ping {} - service", serviceType.name(), e);
                healthDataInfo.isUP = false;
                healthDataInfo.message = String.format("Not connected with %s (%s)",
                        serviceType.name(),
                        dateFormat.format(lastCheck.get(serviceType)));
            }

            buildHealthBuilder(healthDataInfo, builder);
        });

        if (successCallCounter.get() >= serviceTypes.size()) {
            builder.up();
        } else {
            builder.down();
        }
        initialCall = false;
    }

    private void buildHealthBuilder(HealthDataInfo healthDataInfo, Health.Builder builder) {
        String serviceNameInLowerCase = healthDataInfo.serviceType.name().toLowerCase();
        String connectionUpKey = String.format("%s.service.connection.up", serviceNameInLowerCase);
        String connectionMessageKey = String.format("%s.service.connection.message", serviceNameInLowerCase);
        builder.withDetail(connectionUpKey, initialCall || healthDataInfo.isUP);
        builder.withDetail(connectionMessageKey, healthDataInfo.message);
    }

    private String getUrl(ServiceType serviceType) {
        switch (serviceType) {
            case DB:
                return dbServicePingUrl;
            default:
                throw new IllegalStateException("Unexpected value: " + serviceType);
        }
    }

    private static class HealthDataInfo {
        private ServiceType serviceType;
        private boolean isUP;
        private String message;
    }

    private enum  ServiceType {
        DB;

        public static Set<ServiceType> ALL = EnumSet.allOf(ServiceType.class);
    }
}


