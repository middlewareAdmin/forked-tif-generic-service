package com.mdw360.service.actuator;


import com.mdw360.service.model.actuator.ResponseInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.mdw360.service.util.constant.AppConstant.ROUTES_INFO;

/**
 * {@link Endpoint} to expose all {@link Route} information.
 */

@Component
@Slf4j
public class RoutesInfoEndpoint implements Endpoint<List<ResponseInfo>> {

    @Autowired
    private CamelContext camelContext;

    @Override
    public String getId() {
        return ROUTES_INFO;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isSensitive() {
        return false;
    }

    @Override
    public List<ResponseInfo> invoke() {

        log.info( "Starting invoke" );
        List<ResponseInfo> routeDetailsInfos = new ArrayList<>();

        List<Route> routes = camelContext.getRoutes();

        if (routes != null && routes.size() > 0) {
            log.debug( "Number of routes: {}", routes.size() );
            for (Route route : routes) {
                ResponseInfo routeDetailsInfo = new ResponseInfo(camelContext, route);
                routeDetailsInfos.add(routeDetailsInfo);
            }
        }

        log.info( "End invoke" );
        return routeDetailsInfos;
    }

}
