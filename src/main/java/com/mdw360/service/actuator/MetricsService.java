package com.mdw360.service.actuator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class MetricsService {

    @Autowired
    private CounterService counterService;

//    @Value("${total.file.processed}")
//    private String totalFileProcessed;
//    @Value("${total.emails.file.processed}")
//    private String totalEmailProcessed;



    @PostConstruct
    void init() {
//        counterService.reset(totalFileProcessed);
//        counterService.reset(totalEmailProcessed);
        log.debug( "Resetting counters" );
    }

}
