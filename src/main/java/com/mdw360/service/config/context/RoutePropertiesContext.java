package com.mdw360.service.config.context;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("routePropertiesContext")
@Slf4j
@Getter
public class RoutePropertiesContext {
    @Value("${camel.route.exception.backOffMultiplier}")
    private int backOffMultiplier;

    @Value("${camel.route.exception.maximumRedeliveryDelay}")
    private int maximumRedeliveryDelay;

    @Value("${weather.app.locations.forecast}")
    private String forecastWeatherLocationList;

    @Value("${weather.app.locations.current}")
    private String currentWeatherLocationList;

    @Value("${weather.app.quartz.forecast.expression}")
    private String currentWeatherQuartzExpression;

    @Value("${weather.app.quartz.current.expression}")
    private String forecastWeatherQuartzExpression;

    @Value("${weather.app.api.key}")
    private String weatherApiKey;

    /* Stored Procedure to use to process the r5 table in TOMS */
    @Value("${weather.app.tkdTrans.code}")
    private String tkdTransCode;

    @PostConstruct
    private void postConstruct(){
        log.info("Route Properties Context: ******");
        log.info("\tBackOffMultiplier: {}", getBackOffMultiplier());
        log.info("\tMaximumRedeliveryDelay: {}", getMaximumRedeliveryDelay());
        log.info("\tForecastWeatherLocationList: {}", getForecastWeatherLocationList());
        log.info("\tCurrentWeatherLocationList: {}", getCurrentWeatherLocationList());
        log.info("\tCurrentWeatherQuartzExpression: {}", getCurrentWeatherQuartzExpression());
        log.info("\tForecastWeatherQuartzExpression: {}", getForecastWeatherQuartzExpression());
        log.info("\tWeatherApiKey: {}", getWeatherApiKey());
        log.info("\tTKD Trans Code: {}", getTkdTransCode());
    }

}
