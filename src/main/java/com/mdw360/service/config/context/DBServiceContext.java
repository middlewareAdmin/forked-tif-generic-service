package com.mdw360.service.config.context;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Purpose is to hold the properties and values to call the Repository Service
 */
@Component("dBServiceContext")
@Slf4j
@Getter
public class DBServiceContext {

    @Value("${security.user.name}")
    private String userName;

    @Value("${security.user.password}")
    private String password;

    @Value("${db.service.create.r5track.url}")
    private String createR5trackURI;

    @PostConstruct
    private void postConstruct(){
        log.info("DB Service Configuration: URL: {}, USER: {}, PASS: {}", getCreateR5trackURI(), getUserName(), getPassword().length());
    }
}
