package com.mdw360.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import static com.mdw360.service.util.constant.AppConstant.REALM_NAME;

@Configuration
public class SecurityConfig {
    @Bean
    BasicAuthenticationFilter basicAuthFilter(AuthenticationManager authenticationManager) {
        return new BasicAuthenticationFilter(authenticationManager, basicAuthEntryPoint());
    }

    @Bean
    BasicAuthenticationEntryPoint basicAuthEntryPoint() {
        BasicAuthenticationEntryPoint basicAuth = new BasicAuthenticationEntryPoint();
        basicAuth.setRealmName(REALM_NAME);
        return basicAuth;
    }
}
