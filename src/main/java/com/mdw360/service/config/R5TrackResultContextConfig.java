package com.mdw360.service.config;

import com.mdw360.service.transform.CurrentWeatherToR5TrackResultContextImpl;
import com.mdw360.service.transform.ForecastWeatherToR5TrackResultContextImpl;
import com.mdw360.service.transform.PayloadToR5TrackResultContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class R5TrackResultContextConfig {
    //TODO: move to better config object
    @Bean(name = "currentWeatherToR5TrackResultContext")
    PayloadToR5TrackResultContext currentWeatherToR5TrackResultContext(){
        return new CurrentWeatherToR5TrackResultContextImpl();
    }

    @Bean(name = "forecastWeatherToR5TrackResultContext")
    PayloadToR5TrackResultContext forecastWeatherToR5TrackResultContext(){
        return new ForecastWeatherToR5TrackResultContextImpl();
    }
}
