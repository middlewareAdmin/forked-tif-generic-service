package com.mdw360.service.model.domain.current;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

// Used handy online tool :  http://www.jsonschema2pojo.org/
// Generates all the Pojo's for GSON

@Getter
@Setter
@ToString
public class CurrentWeather {
    @SerializedName("cnt")
    @Expose
    private int cnt;
    @SerializedName("list")
    @Expose
    private java.util.List<List> list = null;
}
