package com.mdw360.service.model.domain.forecast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mdw360.service.model.domain.common.Clouds;
import com.mdw360.service.model.domain.common.Weather;
import com.mdw360.service.model.domain.common.Wind;
import com.mdw360.service.model.domain.current.Main;
import com.mdw360.service.model.domain.current.Sys;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ForecastList {
    @SerializedName("dt")
    @Expose
    private String dt;
    @SerializedName("main")
    @Expose
    private Main main;
    @SerializedName("weather")
    @Expose
    private java.util.List<Weather> weather = null;
    @SerializedName("clouds")
    @Expose
    private Clouds clouds;
    @SerializedName("wind")
    @Expose
    private Wind wind;
    @SerializedName("sys")
    @Expose
    private Sys sys;
    @SerializedName("dt_txt")
    @Expose
    private String dtTxt;
    @SerializedName("snow")
    @Expose
    private Snow snow;
}
