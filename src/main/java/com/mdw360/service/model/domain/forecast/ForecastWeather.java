package com.mdw360.service.model.domain.forecast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

// Used handy online tool :  http://www.jsonschema2pojo.org/
// Generates all the Pojo's for GSON

@Getter
@Setter
@ToString
public class ForecastWeather {
    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("cnt")
    @Expose
    private int cnt;

    @SerializedName("list")
    @Expose
    private java.util.List<com.mdw360.service.model.domain.forecast.List> list = null;

    @SerializedName("city")
    @Expose
    private City city;
}
