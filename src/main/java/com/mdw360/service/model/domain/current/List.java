package com.mdw360.service.model.domain.current;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mdw360.service.model.domain.common.Clouds;
import com.mdw360.service.model.domain.common.Coord;
import com.mdw360.service.model.domain.common.Weather;
import com.mdw360.service.model.domain.common.Wind;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class List {

    @SerializedName("coord")
    @Expose
    private Coord coord;
    @SerializedName("sys")
    @Expose
    private Sys sys;
    @SerializedName("weather")
    @Expose
    private java.util.List<Weather> weather = null;
    @SerializedName("main")
    @Expose
    private Main main;
    @SerializedName("visibility")
    @Expose
    private String visibility;
    @SerializedName("wind")
    @Expose
    private Wind wind;
    @SerializedName("clouds")
    @Expose
    private Clouds clouds;
    @SerializedName("dt")
    @Expose
    private long dt;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;


}
