package com.mdw360.service.model.domain.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Clouds {
    @SerializedName("all")
    @Expose
    private String all;
}
