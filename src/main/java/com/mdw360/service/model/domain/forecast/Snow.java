package com.mdw360.service.model.domain.forecast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Snow {
    @SerializedName("3h")
    @Expose
    private String _3h;
}
