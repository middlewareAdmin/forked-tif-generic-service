package com.mdw360.service.model.actuator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import org.apache.camel.Route;
import org.apache.camel.StatefulService;

@JsonPropertyOrder({"id", "description", "uptime", "uptimeMillis"})
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter @Setter
public class RouteInfo {

    private final String name;

    private final String description;

    private final String uptime;

    private final long uptimeMillis;

    private final String status;

    public RouteInfo(Route route) {
        this.name = route.getId();
        this.description = route.getDescription();
        this.uptime = route.getUptime();
        this.uptimeMillis = route.getUptimeMillis();

        if (route instanceof StatefulService) {
            this.status = ((StatefulService) route).getStatus().name();
        } else {
            this.status = null;
        }
    }
}
