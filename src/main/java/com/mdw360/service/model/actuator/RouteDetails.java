package com.mdw360.service.model.actuator;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.apache.camel.api.management.mbean.ManagedRouteMBean;
import org.apache.camel.spi.RouteError;

import java.util.Date;

@Getter @Setter
@JsonInclude
public class RouteDetails {

    private long deltaProcessingTime;

    private long exchangesInflight;

    private long exchangesTotal;

    private long externalRedeliveries;

    private long failuresHandled;

    private String firstExchangeCompletedExchangeId;

    private Date firstExchangeCompletedTimestamp;

    private String firstExchangeFailureExchangeId;

    private Date firstExchangeFailureTimestamp;

    private String lastExchangeCompletedExchangeId;

    private Date lastExchangeCompletedTimestamp;

    private String lastExchangeFailureExchangeId;

    private Date lastExchangeFailureTimestamp;

    private long lastProcessingTime;

    private long maxProcessingTime;

    private long meanProcessingTime;

    private long minProcessingTime;

    private Long oldestInflightDuration;

    private String oldestInflightExchangeId;

    private long redeliveries;

    private long totalProcessingTime;

    private RouteError lastError;

    private boolean hasRouteController;

    public RouteDetails(ManagedRouteMBean managedRoute) {
        try {
            this.deltaProcessingTime = managedRoute.getDeltaProcessingTime();
            this.exchangesInflight = managedRoute.getExchangesInflight();
            this.exchangesTotal = managedRoute.getExchangesTotal();
            this.externalRedeliveries = managedRoute.getExternalRedeliveries();
            this.failuresHandled = managedRoute.getFailuresHandled();
            this.firstExchangeCompletedExchangeId = managedRoute.getFirstExchangeCompletedExchangeId();
            this.firstExchangeCompletedTimestamp = managedRoute.getFirstExchangeCompletedTimestamp();
            this.firstExchangeFailureExchangeId = managedRoute.getFirstExchangeFailureExchangeId();
            this.firstExchangeFailureTimestamp = managedRoute.getFirstExchangeFailureTimestamp();
            this.lastExchangeCompletedExchangeId = managedRoute.getLastExchangeCompletedExchangeId();
            this.lastExchangeCompletedTimestamp = managedRoute.getLastExchangeCompletedTimestamp();
            this.lastExchangeFailureExchangeId = managedRoute.getLastExchangeFailureExchangeId();
            this.lastExchangeFailureTimestamp = managedRoute.getLastExchangeFailureTimestamp();
            this.lastProcessingTime = managedRoute.getLastProcessingTime();
            this.maxProcessingTime = managedRoute.getMaxProcessingTime();
            this.meanProcessingTime = managedRoute.getMeanProcessingTime();
            this.minProcessingTime = managedRoute.getMinProcessingTime();
            this.oldestInflightDuration = managedRoute.getOldestInflightDuration();
            this.oldestInflightExchangeId = managedRoute.getOldestInflightExchangeId();
            this.redeliveries = managedRoute.getRedeliveries();
            this.totalProcessingTime = managedRoute.getTotalProcessingTime();
            this.lastError = managedRoute.getLastError();
            this.hasRouteController = managedRoute.getHasRouteController();
        } catch (Exception e) {
            // Ignore
        }
    }

}