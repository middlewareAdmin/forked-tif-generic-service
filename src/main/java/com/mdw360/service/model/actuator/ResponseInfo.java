package com.mdw360.service.model.actuator;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.camel.CamelContext;
import org.apache.camel.Route;
import org.apache.camel.api.management.mbean.ManagedRouteMBean;
import org.apache.camel.spring.boot.model.RouteDetails;

public class ResponseInfo extends RouteInfo {

    @JsonProperty("details")
    private RouteDetails routeDetails;

    public ResponseInfo(Route route) {
        super(route);
    }

    public ResponseInfo(final CamelContext camelContext, final Route route) {
        super(route);

        if (camelContext.getManagementStrategy().getManagementAgent() != null) {
            this.routeDetails = new RouteDetails(camelContext.getManagedRoute(route.getId(), ManagedRouteMBean.class));
        }
    }
}
