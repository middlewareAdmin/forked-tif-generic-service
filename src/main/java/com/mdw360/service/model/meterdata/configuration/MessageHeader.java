package com.mdw360.service.model.meterdata.configuration;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "siteName",
        "siteId",
        "initiationTime",
        "transactionId"
})
@ToString @Getter
public class MessageHeader implements Serializable {


    @JsonProperty("initiationTime")
    @JsonPropertyDescription("The timestamp of when the process was initiated")
    private final String initiationTime;

    /**
     * The transactionId Attribute
     * <p>
     * A UUID generated at the start of the process to track
     * (Required)
     *
     */
    @JsonProperty("transactionId")
    @JsonPropertyDescription("A UUID generated at the start of the process to track")
    private final String transactionId;

    public MessageHeader(String initiationTime, String transactionId) {
        this.initiationTime = initiationTime;
        this.transactionId = transactionId;
    }

    @JsonIgnore
    public MessageHeader getDeepCopyWithNewTransactionIdAndInitTime() {
        return new MessageHeader(new Date().toString(), UUID.randomUUID().toString());
    }
}