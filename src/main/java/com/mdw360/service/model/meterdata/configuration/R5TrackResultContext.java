package com.mdw360.service.model.meterdata.configuration;

import com.mdw360.service.model.dbservices.R5TrackCO;
import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
public class R5TrackResultContext {

    private final List<ErrorContext> errorsResult;

    private final  List<ErrorContext> skipsResult;

    private final List<R5TrackCO> successResult;

    public R5TrackResultContext(List<ErrorContext> errorsResult, List<ErrorContext> skipsResult, List<R5TrackCO> successResult) {
        this.errorsResult = errorsResult != null ? errorsResult : new ArrayList<>();
        this.skipsResult = skipsResult != null ? skipsResult : new ArrayList<>();
        this.successResult = successResult != null ? successResult : new ArrayList<>();
    }
}
