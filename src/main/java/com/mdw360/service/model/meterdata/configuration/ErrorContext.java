package com.mdw360.service.model.meterdata.configuration;

import com.mdw360.service.util.enums.ErrorSourceType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString @NoArgsConstructor
public class ErrorContext {

    private String record;

    private String errorMessage;

    private ErrorSourceType errorSourceType = ErrorSourceType.PAYLOAD_DATA_APP;

    public ErrorContext(String record, String errorMessage) {
        this.record = record;
        this.errorMessage = errorMessage;
    }
}
