package com.mdw360.service.model.dbservices;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "transId",
        "tkdTrackDate",
        "tkdTrans",
        "created",
        "sourceSystem",
        "sourceCode",
        "promptData1",
        "promptData2",
        "promptData3",
        "promptData4",
        "promptData5",
        "promptData6",
        "promptData7",
        "promptData8",
        "promptData9",
        "promptData10",
        "promptData11",
        "promptData12",
        "promptData13",
        "promptData14",
        "promptData15",
        "promptData16",
        "promptData17",
        "promptData18",
        "promptData19",
        "promptData20",
        "promptData21",
        "promptData22",
        "promptData23",
        "promptData24",
        "promptData25",
        "promptData26",
        "promptData27",
        "promptData28",
        "promptData29",
        "promptData30",
        "promptData31",
        "promptData32",
        "promptData33",
        "promptData34",
        "promptData35",
        "promptData36",
        "promptData37",
        "promptData38",
        "promptData39",
        "promptData40",
        "promptData41",
        "promptData42",
        "promptData43",
        "promptData44",
        "promptData45",
        "promptData46",
        "promptData47",
        "promptData48",
        "promptData49",
        "promptData50",
        "promptData51",
        "sessionId",
        "rStatus",
        "changed"
})
@ToString
public class R5TrackCO {

    @NotNull
    @JsonProperty("transId")
    private String transId;
    /**
     *
     * (Required)
     *
     */
    @NotNull
    @JsonProperty("tkdTrackDate")
    private Date tkdTrackDate;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("tkdTrans")
    private String tkdTrans;
    @JsonProperty("created")
    private String created;
    @JsonProperty("sourceSystem")
    private String sourceSystem;
    @JsonProperty("sourceCode")
    private String sourceCode;
    @JsonProperty("promptData1")
    private String promptData1;
    @JsonProperty("promptData2")
    private String promptData2;
    @JsonProperty("promptData3")
    private String promptData3;
    @JsonProperty("promptData4")
    private String promptData4;
    @JsonProperty("promptData5")
    private String promptData5;
    @JsonProperty("promptData6")
    private String promptData6;
    @JsonProperty("promptData7")
    private String promptData7;
    @JsonProperty("promptData8")
    private String promptData8;
    @JsonProperty("promptData9")
    private String promptData9;
    @JsonProperty("promptData10")
    private String promptData10;
    @JsonProperty("promptData11")
    private String promptData11;
    @JsonProperty("promptData12")
    private String promptData12;
    @JsonProperty("promptData13")
    private String promptData13;
    @JsonProperty("promptData14")
    private String promptData14;
    @JsonProperty("promptData15")
    private String promptData15;
    @JsonProperty("promptData16")
    private String promptData16;
    @JsonProperty("promptData17")
    private String promptData17;
    @JsonProperty("promptData18")
    private String promptData18;
    @JsonProperty("promptData19")
    private String promptData19;
    @JsonProperty("promptData20")
    private String promptData20;
    @JsonProperty("promptData21")
    private String promptData21;
    @JsonProperty("promptData22")
    private String promptData22;
    @JsonProperty("promptData23")
    private String promptData23;
    @JsonProperty("promptData24")
    private String promptData24;
    @JsonProperty("promptData25")
    private String promptData25;
    @JsonProperty("promptData26")
    private String promptData26;
    @JsonProperty("promptData27")
    private String promptData27;
    @JsonProperty("promptData28")
    private String promptData28;
    @JsonProperty("promptData29")
    private String promptData29;
    @JsonProperty("promptData30")
    private String promptData30;
    @JsonProperty("promptData31")
    private String promptData31;
    @JsonProperty("promptData32")
    private String promptData32;
    @JsonProperty("promptData33")
    private String promptData33;
    @JsonProperty("promptData34")
    private String promptData34;
    @JsonProperty("promptData35")
    private String promptData35;
    @JsonProperty("promptData36")
    private String promptData36;
    @JsonProperty("promptData37")
    private String promptData37;
    @JsonProperty("promptData38")
    private String promptData38;
    @JsonProperty("promptData39")
    private String promptData39;
    @JsonProperty("promptData40")
    private String promptData40;
    @JsonProperty("promptData41")
    private String promptData41;
    @JsonProperty("promptData42")
    private String promptData42;
    @JsonProperty("promptData43")
    private String promptData43;
    @JsonProperty("promptData44")
    private String promptData44;
    @JsonProperty("promptData45")
    private String promptData45;
    @JsonProperty("promptData46")
    private String promptData46;
    @JsonProperty("promptData47")
    private String promptData47;
    @JsonProperty("promptData48")
    private String promptData48;
    @JsonProperty("promptData49")
    private String promptData49;
    @JsonProperty("promptData50")
    private String promptData50;
    @JsonProperty("promptData51")
    private String promptData51;
    @JsonProperty("sessionId")
    private Double sessionId;
    @JsonProperty("rStatus")
    private String rStatus;
    @JsonProperty("changed")
    private String changed;


    @JsonProperty("transId")
    public String getTransId() {
        return transId;
    }

    @JsonProperty("transId")
    public void setTransId(String transId) {
        this.transId = transId;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("tkdTrackDate")
    public Date getTkdTrackDate() {
        return tkdTrackDate;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("tkdTrackDate")
    public void setTkdTrackDate(Date tkdTrackDate) {
        this.tkdTrackDate = tkdTrackDate;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("tkdTrans")
    public String getTkdTrans() {
        return tkdTrans;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("tkdTrans")
    public void setTkdTrans(String tkdTrans) {
        this.tkdTrans = tkdTrans;
    }

    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    @JsonProperty("sourceSystem")
    public String getSourceSystem() {
        return sourceSystem;
    }

    @JsonProperty("sourceSystem")
    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    @JsonProperty("sourceCode")
    public String getSourceCode() {
        return sourceCode;
    }

    @JsonProperty("sourceCode")
    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    @JsonProperty("promptData1")
    public String getPromptData1() {
        return promptData1;
    }

    @JsonProperty("promptData1")
    public void setPromptData1(String promptData1) {
        this.promptData1 = promptData1;
    }

    @JsonProperty("promptData2")
    public String getPromptData2() {
        return promptData2;
    }

    @JsonProperty("promptData2")
    public void setPromptData2(String promptData2) {
        this.promptData2 = promptData2;
    }

    @JsonProperty("promptData3")
    public String getPromptData3() {
        return promptData3;
    }

    @JsonProperty("promptData3")
    public void setPromptData3(String promptData3) {
        this.promptData3 = promptData3;
    }

    @JsonProperty("promptData4")
    public String getPromptData4() {
        return promptData4;
    }

    @JsonProperty("promptData4")
    public void setPromptData4(String promptData4) {
        this.promptData4 = promptData4;
    }

    @JsonProperty("promptData5")
    public String getPromptData5() {
        return promptData5;
    }

    @JsonProperty("promptData5")
    public void setPromptData5(String promptData5) {
        this.promptData5 = promptData5;
    }

    @JsonProperty("promptData6")
    public String getPromptData6() {
        return promptData6;
    }

    @JsonProperty("promptData6")
    public void setPromptData6(String promptData6) {
        this.promptData6 = promptData6;
    }

    @JsonProperty("promptData7")
    public String getPromptData7() {
        return promptData7;
    }

    @JsonProperty("promptData7")
    public void setPromptData7(String promptData7) {
        this.promptData7 = promptData7;
    }

    @JsonProperty("promptData8")
    public String getPromptData8() {
        return promptData8;
    }

    @JsonProperty("promptData8")
    public void setPromptData8(String promptData8) {
        this.promptData8 = promptData8;
    }

    @JsonProperty("promptData9")
    public String getPromptData9() {
        return promptData9;
    }

    @JsonProperty("promptData9")
    public void setPromptData9(String promptData9) {
        this.promptData9 = promptData9;
    }

    @JsonProperty("promptData10")
    public String getPromptData10() {
        return promptData10;
    }

    @JsonProperty("promptData10")
    public void setPromptData10(String promptData10) {
        this.promptData10 = promptData10;
    }

    @JsonProperty("promptData11")
    public String getPromptData11() {
        return promptData11;
    }

    @JsonProperty("promptData11")
    public void setPromptData11(String promptData11) {
        this.promptData11 = promptData11;
    }

    @JsonProperty("promptData12")
    public String getPromptData12() {
        return promptData12;
    }

    @JsonProperty("promptData12")
    public void setPromptData12(String promptData12) {
        this.promptData12 = promptData12;
    }

    @JsonProperty("promptData13")
    public String getPromptData13() {
        return promptData13;
    }

    @JsonProperty("promptData13")
    public void setPromptData13(String promptData13) {
        this.promptData13 = promptData13;
    }

    @JsonProperty("promptData14")
    public String getPromptData14() {
        return promptData14;
    }

    @JsonProperty("promptData14")
    public void setPromptData14(String promptData14) {
        this.promptData14 = promptData14;
    }

    @JsonProperty("promptData15")
    public String getPromptData15() {
        return promptData15;
    }

    @JsonProperty("promptData15")
    public void setPromptData15(String promptData15) {
        this.promptData15 = promptData15;
    }

    @JsonProperty("promptData16")
    public String getPromptData16() {
        return promptData16;
    }

    @JsonProperty("promptData16")
    public void setPromptData16(String promptData16) {
        this.promptData16 = promptData16;
    }

    @JsonProperty("promptData17")
    public String getPromptData17() {
        return promptData17;
    }

    @JsonProperty("promptData17")
    public void setPromptData17(String promptData17) {
        this.promptData17 = promptData17;
    }

    @JsonProperty("promptData18")
    public String getPromptData18() {
        return promptData18;
    }

    @JsonProperty("promptData18")
    public void setPromptData18(String promptData18) {
        this.promptData18 = promptData18;
    }

    @JsonProperty("promptData19")
    public String getPromptData19() {
        return promptData19;
    }

    @JsonProperty("promptData19")
    public void setPromptData19(String promptData19) {
        this.promptData19 = promptData19;
    }

    @JsonProperty("promptData20")
    public String getPromptData20() {
        return promptData20;
    }

    @JsonProperty("promptData20")
    public void setPromptData20(String promptData20) {
        this.promptData20 = promptData20;
    }

    @JsonProperty("promptData21")
    public String getPromptData21() {
        return promptData21;
    }

    @JsonProperty("promptData21")
    public void setPromptData21(String promptData21) {
        this.promptData21 = promptData21;
    }

    @JsonProperty("promptData22")
    public String getPromptData22() {
        return promptData22;
    }

    @JsonProperty("promptData22")
    public void setPromptData22(String promptData22) {
        this.promptData22 = promptData22;
    }

    @JsonProperty("promptData23")
    public String getPromptData23() {
        return promptData23;
    }

    @JsonProperty("promptData23")
    public void setPromptData23(String promptData23) {
        this.promptData23 = promptData23;
    }

    @JsonProperty("promptData24")
    public String getPromptData24() {
        return promptData24;
    }

    @JsonProperty("promptData24")
    public void setPromptData24(String promptData24) {
        this.promptData24 = promptData24;
    }

    @JsonProperty("promptData25")
    public String getPromptData25() {
        return promptData25;
    }

    @JsonProperty("promptData25")
    public void setPromptData25(String promptData25) {
        this.promptData25 = promptData25;
    }

    @JsonProperty("promptData26")
    public String getPromptData26() {
        return promptData26;
    }

    @JsonProperty("promptData26")
    public void setPromptData26(String promptData26) {
        this.promptData26 = promptData26;
    }

    @JsonProperty("promptData27")
    public String getPromptData27() {
        return promptData27;
    }

    @JsonProperty("promptData27")
    public void setPromptData27(String promptData27) {
        this.promptData27 = promptData27;
    }

    @JsonProperty("promptData28")
    public String getPromptData28() {
        return promptData28;
    }

    @JsonProperty("promptData28")
    public void setPromptData28(String promptData28) {
        this.promptData28 = promptData28;
    }

    @JsonProperty("promptData29")
    public String getPromptData29() {
        return promptData29;
    }

    @JsonProperty("promptData29")
    public void setPromptData29(String promptData29) {
        this.promptData29 = promptData29;
    }

    @JsonProperty("promptData30")
    public String getPromptData30() {
        return promptData30;
    }

    @JsonProperty("promptData30")
    public void setPromptData30(String promptData30) {
        this.promptData30 = promptData30;
    }

    @JsonProperty("promptData31")
    public String getPromptData31() {
        return promptData31;
    }

    @JsonProperty("promptData31")
    public void setPromptData31(String promptData31) {
        this.promptData31 = promptData31;
    }

    @JsonProperty("promptData32")
    public String getPromptData32() {
        return promptData32;
    }

    @JsonProperty("promptData32")
    public void setPromptData32(String promptData32) {
        this.promptData32 = promptData32;
    }

    @JsonProperty("promptData33")
    public String getPromptData33() {
        return promptData33;
    }

    @JsonProperty("promptData33")
    public void setPromptData33(String promptData33) {
        this.promptData33 = promptData33;
    }

    @JsonProperty("promptData34")
    public String getPromptData34() {
        return promptData34;
    }

    @JsonProperty("promptData34")
    public void setPromptData34(String promptData34) {
        this.promptData34 = promptData34;
    }

    @JsonProperty("promptData35")
    public String getPromptData35() {
        return promptData35;
    }

    @JsonProperty("promptData35")
    public void setPromptData35(String promptData35) {
        this.promptData35 = promptData35;
    }

    @JsonProperty("promptData36")
    public String getPromptData36() {
        return promptData36;
    }

    @JsonProperty("promptData36")
    public void setPromptData36(String promptData36) {
        this.promptData36 = promptData36;
    }

    @JsonProperty("promptData37")
    public String getPromptData37() {
        return promptData37;
    }

    @JsonProperty("promptData37")
    public void setPromptData37(String promptData37) {
        this.promptData37 = promptData37;
    }

    @JsonProperty("promptData38")
    public String getPromptData38() {
        return promptData38;
    }

    @JsonProperty("promptData38")
    public void setPromptData38(String promptData38) {
        this.promptData38 = promptData38;
    }

    @JsonProperty("promptData39")
    public String getPromptData39() {
        return promptData39;
    }

    @JsonProperty("promptData39")
    public void setPromptData39(String promptData39) {
        this.promptData39 = promptData39;
    }

    @JsonProperty("promptData40")
    public String getPromptData40() {
        return promptData40;
    }

    @JsonProperty("promptData40")
    public void setPromptData40(String promptData40) {
        this.promptData40 = promptData40;
    }

    @JsonProperty("promptData41")
    public String getPromptData41() {
        return promptData41;
    }

    @JsonProperty("promptData41")
    public void setPromptData41(String promptData41) {
        this.promptData41 = promptData41;
    }

    @JsonProperty("promptData42")
    public String getPromptData42() {
        return promptData42;
    }

    @JsonProperty("promptData42")
    public void setPromptData42(String promptData42) {
        this.promptData42 = promptData42;
    }

    @JsonProperty("promptData43")
    public String getPromptData43() {
        return promptData43;
    }

    @JsonProperty("promptData43")
    public void setPromptData43(String promptData43) {
        this.promptData43 = promptData43;
    }

    @JsonProperty("promptData44")
    public String getPromptData44() {
        return promptData44;
    }

    @JsonProperty("promptData44")
    public void setPromptData44(String promptData44) {
        this.promptData44 = promptData44;
    }

    @JsonProperty("promptData45")
    public String getPromptData45() {
        return promptData45;
    }

    @JsonProperty("promptData45")
    public void setPromptData45(String promptData45) {
        this.promptData45 = promptData45;
    }

    @JsonProperty("promptData46")
    public String getPromptData46() {
        return promptData46;
    }

    @JsonProperty("promptData46")
    public void setPromptData46(String promptData46) {
        this.promptData46 = promptData46;
    }

    @JsonProperty("promptData47")
    public String getPromptData47() {
        return promptData47;
    }

    @JsonProperty("promptData47")
    public void setPromptData47(String promptData47) {
        this.promptData47 = promptData47;
    }

    @JsonProperty("promptData48")
    public String getPromptData48() {
        return promptData48;
    }

    @JsonProperty("promptData48")
    public void setPromptData48(String promptData48) {
        this.promptData48 = promptData48;
    }

    @JsonProperty("promptData49")
    public String getPromptData49() {
        return promptData49;
    }

    @JsonProperty("promptData49")
    public void setPromptData49(String promptData49) {
        this.promptData49 = promptData49;
    }

    @JsonProperty("promptData50")
    public String getPromptData50() {
        return promptData50;
    }

    @JsonProperty("promptData50")
    public void setPromptData50(String promptData50) {
        this.promptData50 = promptData50;
    }

    @JsonProperty("promptData51")
    public String getPromptData51() {
        return promptData51;
    }

    @JsonProperty("promptData51")
    public void setPromptData51(String promptData51) {
        this.promptData51 = promptData51;
    }

    @JsonProperty("sessionId")
    public Double getSessionId() {
        return sessionId;
    }

    @JsonProperty("sessionId")
    public void setSessionId(Double sessionId) {
        this.sessionId = sessionId;
    }

    @JsonProperty("rStatus")
    public String getRStatus() {
        return rStatus;
    }

    @JsonProperty("rStatus")
    public void setRStatus(String rStatus) {
        this.rStatus = rStatus;
    }

    @JsonProperty("changed")
    public String getChanged() {
        return changed;
    }

    @JsonProperty("changed")
    public void setChanged(String changed) {
        this.changed = changed;
    }

}
