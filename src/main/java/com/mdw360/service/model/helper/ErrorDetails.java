package com.mdw360.service.model.helper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter @Setter @NoArgsConstructor
public class ErrorDetails {

    private Date date;

    private String message;

    private String path;

    public ErrorDetails(Date date, String message, String path) {
        this.date = date;
        this.message = message;
        this.path = path;
    }
}
