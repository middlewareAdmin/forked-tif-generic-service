package com.mdw360.service.model.helper;

import com.mdw360.service.util.enums.ExceptionPolicy;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.camel.Exchange;

import java.util.Set;

@Setter @Getter @NoArgsConstructor
public class ExceptionHandlerVO {

    private Exception exception;

    private Exchange exchange;

    private String requestId;

    private boolean needToRetry;

    private Set<ExceptionPolicy> retryExceptionPolicies;

    private Set<ExceptionPolicy> skipExceptionPolicies;

    public ExceptionHandlerVO(Exception exception, Exchange exchange, String requestId, Set<ExceptionPolicy> exceptionPolicies) {
        this.exception = exception;
        this.exchange = exchange;
        this.requestId = requestId;
        this.retryExceptionPolicies = exceptionPolicies;
    }

}
