package com.mdw360.service.model.helper;

import com.mdw360.service.model.meterdata.configuration.R5TrackResultContext;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@NoArgsConstructor
public class R5TrackResultContextHelper {

    private R5TrackResultContext r5TrackResultContext;

    private String requestId;

    public R5TrackResultContextHelper(R5TrackResultContext r5TrackResultContext, String requestId) {
        this.r5TrackResultContext = r5TrackResultContext;
        this.requestId = requestId;
    }

    private int successCountOfSendR5TrackToDbService;
}
