package com.mdw360.service.route;

import com.mdw360.service.aggregate.strategy.CurrentWeatherAggregateStrategy;
import com.mdw360.service.aggregate.strategy.ForecastWeatherAggregateStrategy;
import com.mdw360.service.config.context.RoutePropertiesContext;
import com.mdw360.service.util.exceptions.RetryException;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.ws.rs.HttpMethod;

import static com.mdw360.service.util.constant.AppConstant.IS_DATA_AVAILABLE;

@Component("appRoutes")
@Slf4j
class AppRoutes extends RouteBuilder {

    @Autowired
    @Qualifier("routePropertiesContext")
    RoutePropertiesContext routePropertiesContext;

    @Override
    public void configure() throws Exception {

        log.info("Starting RestRoutes");
        CamelContext camelContext = getContext();
        camelContext.setUseMDCLogging(true);
        camelContext.setUseBreadcrumb(true);

        onException(RetryException.class)
            .routeId("onExceptionRetryRoute")
            .maximumRedeliveries(-1)
            .log("log:com.mdw350.route?level=ERROR")
            .retryAttemptedLogLevel(LoggingLevel.WARN)
            .backOffMultiplier(routePropertiesContext.getBackOffMultiplier())
            .maximumRedeliveryDelay(routePropertiesContext.getMaximumRedeliveryDelay())
            .useExponentialBackOff();

        rest("/weather")
            .get("/current").consumes("application/json").to("direct:current").produces("application/json").bindingMode(RestBindingMode.off)
            .get("/forecast").consumes("application/json").to("direct:forecast").produces("application/json").bindingMode(RestBindingMode.off);

        /* FORECAST ROUTES */
        from("quartz://weatherGroup/forecastSchedule?cron=" + routePropertiesContext.getForecastWeatherQuartzExpression() + "&trigger.timeZone=America/Vancouver")
            .routeId("routeForecastWeatherTrigger")
            .log(LoggingLevel.INFO, "... Forecast Weather Trigger has executed")
            .to("direct:forecast");

        from("direct:forecast")
            .routeId("routeDirectForecastWeather")
            .setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)
            .setBody(constant(routePropertiesContext.getForecastWeatherLocationList()))
            .split(bodyAs(String.class).tokenize(","), new ForecastWeatherAggregateStrategy())
            .toD("https://api.openweathermap.org/data/2.5/forecast?id=${body}&bridgeEndpoint=true&units=metric&appid=" + routePropertiesContext.getWeatherApiKey())
            .setHeader("REQUEST_TYPE", constant("FORECAST_WEATHER"))
            .to("direct:toms-repository-queue")
               // .process("emptyProcessor")
            .end()
            .process("r5TrackProcessorToJson");

        /* CURRENT WEATHER ROUTES */
        from("quartz://weatherGroup/currentSchedule?cron=" + routePropertiesContext.getCurrentWeatherQuartzExpression() + "&trigger.timeZone=America/Vancouver")
            .routeId("routeCurrentWeatherTrigger")
            .log(LoggingLevel.INFO, "... routeCurrent Weather Trigger has executed")
            .to("direct:current");

        from("direct:current")
            .routeId("routeDirectCurrentWeather")
            .setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)
            .setBody(constant(routePropertiesContext.getCurrentWeatherLocationList()))
            .split(bodyAs(String.class).tokenize(","), new CurrentWeatherAggregateStrategy())
            .toD("https://api.openweathermap.org/data/2.5/group?id=${body}&bridgeEndpoint=true&units=metric&appid=" + routePropertiesContext.getWeatherApiKey())
            .setHeader("REQUEST_TYPE", constant("CURRENT_WEATHER"))
            .to("direct:toms-repository-queue")
            .end()
            .process("r5TrackProcessorToJson");

        from("direct:toms-repository-queue")
            .routeId("routePostTomRepository")
            .process("r5TrackResultContextProcessor")
            .choice()
                .when(header(IS_DATA_AVAILABLE).isEqualTo(true))
                    .process("r5TrackProcessor")
                    .log("Successfully execute query process")
                .endChoice();
    }
}
