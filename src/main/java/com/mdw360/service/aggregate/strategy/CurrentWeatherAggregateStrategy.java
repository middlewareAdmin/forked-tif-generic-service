package com.mdw360.service.aggregate.strategy;

import com.mdw360.service.model.dbservices.R5TrackCO;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class CurrentWeatherAggregateStrategy implements AggregationStrategy {
    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        List<R5TrackCO> list;
        R5TrackCO newBody = newExchange.getIn().getBody(R5TrackCO.class);

        if(oldExchange == null){
            list = new ArrayList<>();
            list.add(newBody);
            newExchange.getIn().setBody(list);
            log.debug("New Exchange {}", newExchange);
            return newExchange;
        }
        else{
            list = oldExchange.getIn().getBody(ArrayList.class);
            list.add(newBody);
            log.debug("Old Exchange {}", oldExchange);
            return oldExchange;
        }
    }

}
