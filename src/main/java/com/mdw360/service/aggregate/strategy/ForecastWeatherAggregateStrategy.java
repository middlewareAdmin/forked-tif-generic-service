package com.mdw360.service.aggregate.strategy;

import com.mdw360.service.model.dbservices.R5TrackCO;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import java.util.List;

@Slf4j
public class ForecastWeatherAggregateStrategy implements AggregationStrategy {
    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {

        List<R5TrackCO> newBodyList = (List<R5TrackCO>) newExchange.getIn().getBody();

        if(oldExchange == null){
            newExchange.getIn().setBody(newBodyList);
            log.debug("New Exchange {}", newExchange);
            return newExchange;
        }
        else{
            List oldBodyList = (List<R5TrackCO>) oldExchange.getIn().getBody();
            oldBodyList.addAll(newBodyList);
            log.debug("Old Exchange {}", oldExchange);
            return oldExchange;
        }
    }

}
