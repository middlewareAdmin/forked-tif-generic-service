package com.mdw360.service.api;

import com.mdw360.service.model.helper.ErrorDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

import static com.mdw360.service.util.constant.AppConstant.*;

@RestController
@RequestMapping(ERROR_PATH)
@Slf4j
/**
 * Override Spring-boot ErrorController, provided custom implementation.
 * */
public class ServiceExceptionHandlerApi implements ErrorController{

    private final ErrorAttributes errorAttributes;

    @Autowired
    public ServiceExceptionHandlerApi(ErrorAttributes errorAttributes) {
        Assert.notNull(errorAttributes, "ErrorAttributes must not be  null");
        this.errorAttributes = errorAttributes;
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }

    @RequestMapping
    public ResponseEntity<?> error(HttpServletRequest request) {
        Map<String, Object> map = logAndGetError(request, getTraceParameter(request));
        String path = null;
        try {
            path =  (String)request.getAttribute("javax.servlet.error.request_uri");
        } catch (Exception e) {
            log.error("Exception during find path cause: {}", e.getMessage());
        }

        ErrorDetails errorDetails = null;
        Integer status = map.get(STATUS) == null ? 500 : (Integer)map.get(STATUS);

        if (status == 401 || status == 403) {
            errorDetails = new ErrorDetails(new Date(), FORBIDDEN_ERROR_MESSAGE, path);
            return new ResponseEntity<>(errorDetails, HttpStatus.FORBIDDEN);
        } else if (status == 404){
            errorDetails = new ErrorDetails(new Date(), NOT_IMPLEMENTED_ERROR_MESSAGE, path);
            return new ResponseEntity<>(errorDetails, HttpStatus.NOT_IMPLEMENTED);
        } else {
            return new ResponseEntity<>(map, HttpStatus.valueOf(status));
        }

    }

    /**
     * getTraceParameter
     * @param request
     */
    private boolean getTraceParameter(HttpServletRequest request) {
        String parameter = request.getParameter("trace");
        if (parameter == null) {
            return false;
        }
        return !"false".equals(parameter.toLowerCase());
    }

    /**
     * logAndGetError
     * @param request
     * @param trace
     * @return
     */
    private Map<String, Object> logAndGetError(HttpServletRequest  request, boolean trace) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        Map<String, Object> map = errorAttributes.getErrorAttributes(requestAttributes,  trace);
        log.warn("Error occur: {}", map);
        return map;
    }

}
