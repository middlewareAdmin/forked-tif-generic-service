package com.mdw360.service;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.mdw360.service.util.constant.AppConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import testhelper.utility.RestUtil;

import static testhelper.utility.RestUtil.createUri;
import static testhelper.utility.RestUtil.getBasicAuth;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
@ActiveProfiles(AppConstant.TEST_PROFILE)
public class HttpRestResponseTest {

    @Value("${rest.host}")
    private String host;

    @Value("${local.server.port}")
    protected String port;

    @Value("${context-path}")
    private String apiVersion;

    @Autowired
    private CamelContext camelContext;

    @Before
    public void init() throws Exception {
        camelContext.stop();
    }

    @Test
    public void test200Security() throws Exception {
        final String uri = RestUtil.createUri(host, port, null, "metrics");
        log.info("test200Security URI: {}", uri);
        RequestSpecification basicAuth = getBasicAuth("TIF", "password");
        Response response = basicAuth.accept(ContentType.JSON).get(uri);
        log.info("Response Code: {}", response.getStatusCode());
        Assert.assertThat(response.getStatusCode(), Matchers.equalTo(200));
    }

    @Test
    public void test501NotImplementedWithSecurity() throws Exception {
        final String uri = createUri(host, port, "999999", "routesInfo");
        RequestSpecification basicAuth = getBasicAuth("TIF", "password");
        Response response = basicAuth.accept(ContentType.JSON).get(uri);
        log.info("Response Code: {}", response.getStatusCode());
        Assert.assertThat(response.getStatusCode(), Matchers.equalTo(501));
    }

    @Test
    public void test403Forbidden() throws Exception {
        final String uri = createUri(host, port, null, "routesInfo");
        RequestSpecification basicAuth = getBasicAuth("TIF", "INVALID-PASSWORD");
        Response response = basicAuth.accept(ContentType.JSON).get(uri);
        log.info("Response Code: {}", response.getStatusCode());
        Assert.assertThat(response.getStatusCode(), Matchers.equalTo(403));
    }

}
