package component;

import com.mdw360.service.component.DateHolder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class DateHolderTest {


    @Test
    public void testDateHolder() throws Exception {
        Date date = new Date();
        DateHolder dateHolder = new DateHolder();

        assertThat(dateHolder.getLastAccessTimeOfDBServiceConnection()).isNull();
        assertThat(dateHolder.getLastAccessTimeOfEmailServiceConnection()).isNull();
        assertThat(dateHolder.getLastAccessTimeOfSftpServiceConnection()).isNull();

        dateHolder.setLastAccessTimeOfSftpServiceConnection(date);
        dateHolder.setLastAccessTimeOfEmailServiceConnection(date);
        dateHolder.setLastAccessTimeOfDBServiceConnection(date);

        assertThat(dateHolder.getLastAccessTimeOfDBServiceConnection()).isEqualTo(date);
        assertThat(dateHolder.getLastAccessTimeOfEmailServiceConnection()).isEqualTo(date);
        assertThat(dateHolder.getLastAccessTimeOfSftpServiceConnection()).isEqualTo(date);
    }

    // When try to set null value on any filed of dataholder class, pre condition will fail, and throw Nullpointer exception
    @Test(expected = NullPointerException.class)
    public void testWithNullValue() {
        DateHolder dateHolder = new DateHolder();
        dateHolder.setLastAccessTimeOfDBServiceConnection(null);
    }
}
