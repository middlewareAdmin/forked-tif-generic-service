package component.handler;

import com.mdw360.service.component.handler.ExceptionHandler;
import com.mdw360.service.component.handler.ExceptionHandlerImpl;
import com.mdw360.service.model.helper.ExceptionHandlerVO;
import com.mdw360.service.util.constant.AppConstant;
import com.mdw360.service.util.enums.ExceptionPolicy;
import com.mdw360.service.util.exceptions.RetryException;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import java.util.EnumSet;
import java.util.Set;
import java.util.UUID;

import static com.mdw360.service.util.enums.ExceptionPolicy.HTTP_CLIENT_ERROR_WITH_NOT_FOUND_STATUS;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class ExceptionHandlerTest {

    //In case of load Main Application class spring will configured all required beans, here we just need ExceptionHandler bean so for save startup time without unused beans configuration, i am using TestConfiguration
    private ExceptionHandler exceptionHandler;

    private Exchange exchange;
    private String requestId;

    @Before
    public void init() {
        exceptionHandler = new ExceptionHandlerImpl();

        //http://svn.apache.org/viewvc/camel/trunk/camel-core/src/test/java/org/apache/camel/ExchangeTestSupport.java?view=markup
        CamelContext ctx = new DefaultCamelContext();
        exchange = new DefaultExchange(ctx);
        requestId = UUID.randomUUID().toString();
        assertNullBodyAndHeaderOfExchange();
    }


    @Test
    public void testHandleTifExceptionWithHttpClientErrorExceptionWithBadRequest() {
        HttpClientErrorException errorException = new HttpClientErrorException(HttpStatus.BAD_REQUEST);

        ExceptionHandlerVO exceptionHandlerVO = new ExceptionHandlerVO(errorException,exchange, requestId, EnumSet.of(ExceptionPolicy.NONE));
        exceptionHandler.handleTifException(exceptionHandlerVO);

        assertNotNull(exchange.getIn().getHeader(AppConstant.ERROR));
        assertNotNull(exchange.getIn().getBody());

        Exception exception = (Exception)exchange.getIn().getBody();

        assertTrue(exception instanceof  HttpClientErrorException);
        assertTrue((Boolean) exchange.getIn().getHeader(AppConstant.ERROR));
    }

    @Test
    public void testHandleTifExceptionWithHttpClientErrorExceptionWithNotFound() {
        HttpClientErrorException errorException = new HttpClientErrorException(HttpStatus.NOT_FOUND);

        ExceptionHandlerVO exceptionHandlerVO = new ExceptionHandlerVO(errorException,exchange, requestId, EnumSet.of(ExceptionPolicy.HTTP_CLIENT_ERROR_WITH_NOT_FOUND_STATUS));
        Set<ExceptionPolicy> skipExceptionPolicies = EnumSet.of(ExceptionPolicy.RESOURCE_ACCESS_ERROR,HTTP_CLIENT_ERROR_WITH_NOT_FOUND_STATUS);;

        exceptionHandlerVO.setSkipExceptionPolicies(skipExceptionPolicies);
        exceptionHandler.handleTifException(exceptionHandlerVO);

        assertNullBodyAndHeaderOfExchange();
    }

    @Test(expected = RetryException.class)
    public void testHandleTifExceptionWithNeedToRetryInCaseOFResourceAccessException() {
        ResourceAccessException errorException = new ResourceAccessException("Service Down");

        ExceptionHandlerVO exceptionHandlerVO = new ExceptionHandlerVO(errorException,exchange, requestId, EnumSet.of(ExceptionPolicy.RESOURCE_ACCESS_ERROR));
        exceptionHandlerVO.setNeedToRetry(true);

        exceptionHandler.handleTifException(exceptionHandlerVO);
    }

    @Test
    public void testUnknownRestException() {
        Exception errorException = new NullPointerException("Testing purpose only");

        ExceptionHandlerVO exceptionHandlerVO = new ExceptionHandlerVO(errorException,exchange, requestId, EnumSet.of(ExceptionPolicy.NONE));
        exceptionHandler.handleTifException(exceptionHandlerVO);

        Exception exception = (Exception)exchange.getIn().getBody();

        assertTrue(exception instanceof  NullPointerException);
        assertTrue((Boolean) exchange.getIn().getHeader(AppConstant.ERROR));
    }


    private void assertNullBodyAndHeaderOfExchange() {
        assertNull(exchange.getIn().getHeader(AppConstant.ERROR));
        assertNull(exchange.getIn().getBody());
    }

}
