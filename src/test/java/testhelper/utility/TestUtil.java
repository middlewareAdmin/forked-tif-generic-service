package testhelper.utility;


public class TestUtil {
//
//    public static CsvSourceField getSourceField() {
//        CsvSourceField sourceField = new CsvSourceField();
//        sourceField.setFieldName("name");
//        sourceField.setColumn(1);
//        sourceField.setDataType(DataType.STRING);
//        sourceField.setDestinationLocation("promptData51");
//        sourceField.setTrimSpaces(true);
//
//        ExcludeRowBasedOnValue excludeRowBasedOnValue = new ExcludeRowBasedOnValue();
//        excludeRowBasedOnValue.setKey("abc");
//        List<ExcludeRowBasedOnValue> excludeRowBasedOnValues = new ArrayList<>();
//        excludeRowBasedOnValues.add(excludeRowBasedOnValue);
//
//        sourceField.setExcludeRowBasedOnValues(excludeRowBasedOnValues);
//
//        return sourceField;
//    }
//
//    public static SiteXformFileConfiguration getSiteXformFileConfiguration() {
//        SiteXformFileConfiguration siteXformFileConfiguration = new SiteXformFileConfiguration();
//        siteXformFileConfiguration.setStopOnError(false);
//        siteXformFileConfiguration.setFileName("Kearl");
//        siteXformFileConfiguration.setHeadersToSkip(0);
//        siteXformFileConfiguration.setFileType(FileType.FIXED_WIDTH);
//
//        return siteXformFileConfiguration;
//    }
//
//    public static SiteFileMessage getSiteFileMessage(String siteName, String siteId, String fileName) {
//        SftpMessageHeader messageHeader = new SftpMessageHeader();
//        messageHeader.setSiteName(siteName);
//        messageHeader.setSiteID(siteId);
//        messageHeader.setInitiationTime(new Date().toString());
//        messageHeader.setTransactionID(UUID.randomUUID().toString());
//        messageHeader.setTotalFileCount(1);
//
//        SiteDataFile siteDataFile = new SiteDataFile();
//        siteDataFile.setCurrentFileCount(1);
//        siteDataFile.setInputFileName(fileName);
//        siteDataFile.setFileDataBlock("MzMwMSAgICAgICAwMS1NQVItMTggIDIuMjQwMDAwMAozMzAyICAgICAgIDAxLU1BUi0xOCAgMTIuOTAwMDAwMAozMzAzICAgICAgIDAxLU1BUi0xOCAgMjQuMDAwMDAwMAozMzA0ICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKNjUwMSAgICAgICAwMS1NQVItMTggIDcuMTQwMDAwMAo2NTAyICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKMTAxOSAgICAgICAwMS1NQVItMTggIDExLjczMDAwMDAKNDEwMSAgICAgICAwMS1NQVItMTggIDE5LjQzMDAwMDAKNDEwMiAgICAgICAwMS1NQVItMTggIDE4Ljc3MDAwMDAKNDEwMyAgICAgICAwMS1NQVItMTggIDE1Ljk0MDAwMDAKNDEwNCAgICAgICAwMS1NQVItMTggIDYuMjAwMDAwMAo0MTA1ICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKNDEwNiAgICAgICAwMS1NQVItMTggIDkuODgwMDAwMAo0MTA3ICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKNDEwOCAgICAgICAwMS1NQVItMTggIDIxLjUxMDAwMDAKNDEwOSAgICAgICAwMS1NQVItMTggIDE5LjQxMDAwMDAKNDExMCAgICAgICAwMS1NQVItMTggIDI0LjAwMDAwMDAKNDExMSAgICAgICAwMS1NQVItMTggIDE4LjI0MDAwMDAKNDExMiAgICAgICAwMS1NQVItMTggIDE0Ljk3MDAwMDAKNDExMyAgICAgICAwMS1NQVItMTggIDIyLjk0MDAwMDAKNDUwMSAgICAgICAwMS1NQVItMTggIDEyLjE0MDAwMDAKNTMwMSAgICAgICAwMS1NQVItMTggIC4wMDAwMDAwCjUzMDIgICAgICAgMDEtTUFSLTE4ICAuMDAwMDAwMAo2NzAxICAgICAgIDAxLU1BUi0xOCAgMTIuMTMwMDAwMAo2ODAxICAgICAgIDAxLU1BUi0xOCAgMjIuMDIwMDAwMAo2ODAyICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKMjYwMSAgICAgICAwMS1NQVItMTggIDkuMzMwMDAwMAoyNjAyICAgICAgIDAxLU1BUi0xOCAgMTguMjUwMDAwMAoyNjAzICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKMjcwMSAgICAgICAwMS1NQVItMTggIC4wMDAwMDAwCjI3MDIgICAgICAgMDEtTUFSLTE4ICAuMDAwMDAwMAoyNzAzICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKMjcwNCAgICAgICAwMS1NQVItMTggIC4wMDAwMDAwCjI3MDUgICAgICAgMDEtTUFSLTE4ICAuMDAwMDAwMAoyMTAxICAgICAgIDAxLU1BUi0xOCAgMTcuOTAwMDAwMAoyMTAyICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKMjEwMyAgICAgICAwMS1NQVItMTggIDIxLjcyMDAwMDAKMjEwNCAgICAgICAwMS1NQVItMTggIDE5LjkwMDAwMDAKMjEwNSAgICAgICAwMS1NQVItMTggIDEyLjA1MDAwMDAKMjEwNiAgICAgICAwMS1NQVItMTggIDE2LjM4MDAwMDAKMjEwNyAgICAgICAwMS1NQVItMTggIDE2LjI4MDAwMDAKMjEwOCAgICAgICAwMS1NQVItMTggIC4wMDAwMDAwCjIxMDkgICAgICAgMDEtTUFSLTE4ICAxOC41NzAwMDAwCjIxMTAgICAgICAgMDEtTUFSLTE4ICAxNC45NjAwMDAwCjIxMTEgICAgICAgMDEtTUFSLTE4ICAyMC44MzAwMDAwCjIxMTIgICAgICAgMDEtTUFSLTE4ICAuMTIwMDAwMAoyMTEzICAgICAgIDAxLU1BUi0xOCAgNC4yMjAwMDAwCjIxMTQgICAgICAgMDEtTUFSLTE4ICAuMDAwMDAwMAoyMTE1ICAgICAgIDAxLU1BUi0xOCAgMTQuMzAwMDAwMAoyMTE2ICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKMjExNyAgICAgICAwMS1NQVItMTggIDE5Ljc3MDAwMDAKMjExOCAgICAgICAwMS1NQVItMTggIC4wMDAwMDAwCjIxMTkgICAgICAgMDEtTUFSLTE4ICAxNS4zNjAwMDAwCjIxMjAgICAgICAgMDEtTUFSLTE4ICAxNi40NjAwMDAwCjIxMjEgICAgICAgMDEtTUFSLTE4ICAuMDAwMDAwMAoyMTIyICAgICAgIDAxLU1BUi0xOCAgMTEuODgwMDAwMAoyMTIzICAgICAgIDAxLU1BUi0xOCAgMTYuMzIwMDAwMAoyMTI0ICAgICAgIDAxLU1BUi0xOCAgMTguMDMwMDAwMAoyMTI1ICAgICAgIDAxLU1BUi0xOCAgMTcuMDcwMDAwMAoyMTI2ICAgICAgIDAxLU1BUi0xOCAgMTguMDIwMDAwMAoyMTI3ICAgICAgIDAxLU1BUi0xOCAgMTQuNTQwMDAwMAoyMTI4ICAgICAgIDAxLU1BUi0xOCAgMTcuNzIwMDAwMAoyMTI5ICAgICAgIDAxLU1BUi0xOCAgMTkuNDYwMDAwMAoyMTMwICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKMjEzMSAgICAgICAwMS1NQVItMTggIDE2Ljc5MDAwMDAKMjEzMiAgICAgICAwMS1NQVItMTggIDE3LjIxMDAwMDAKMjEzMyAgICAgICAwMS1NQVItMTggIDE2LjUzMDAwMDAKMjEzNCAgICAgICAwMS1NQVItMTggIDE0Ljg2MDAwMDAKMjEzNSAgICAgICAwMS1NQVItMTggIDExLjU3MDAwMDAKMjEzNiAgICAgICAwMS1NQVItMTggIDE5LjE4MDAwMDAKMjEzNyAgICAgICAwMS1NQVItMTggIDkuNzEwMDAwMAoyMTM4ICAgICAgIDAxLU1BUi0xOCAgMTkuMjMwMDAwMAoyMTM5ICAgICAgIDAxLU1BUi0xOCAgMTkuMTQwMDAwMAoyMTQwICAgICAgIDAxLU1BUi0xOCAgMTYuNTUwMDAwMAoyMTQxICAgICAgIDAxLU1BUi0xOCAgMTUuNDQwMDAwMAoyMTQyICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKMjE0MyAgICAgICAwMS1NQVItMTggIDEyLjQ1MDAwMDAKMjE0NCAgICAgICAwMS1NQVItMTggIDE1LjQ5MDAwMDAKMjE0NSAgICAgICAwMS1NQVItMTggIDEyLjg1MDAwMDAKMjE0NiAgICAgICAwMS1NQVItMTggIDE4LjMyMDAwMDAKMjE0NyAgICAgICAwMS1NQVItMTggIDE5LjIwMDAwMDAKMjE0OCAgICAgICAwMS1NQVItMTggIDIwLjEyMDAwMDAKMjE0OSAgICAgICAwMS1NQVItMTggIDE3Ljk5MDAwMDAKMjE1MCAgICAgICAwMS1NQVItMTggIDE5LjYwMDAwMDAKMjE1MSAgICAgICAwMS1NQVItMTggIDE4LjA4MDAwMDAKMjE1MiAgICAgICAwMS1NQVItMTggIDE2LjMxMDAwMDAKMjE1MyAgICAgICAwMS1NQVItMTggIC4wMDAwMDAwCjIxNTQgICAgICAgMDEtTUFSLTE4ICAxOC4xOTAwMDAwCjIxNTUgICAgICAgMDEtTUFSLTE4ICAxOC4yMTAwMDAwCjIxNTYgICAgICAgMDEtTUFSLTE4ICAuMDAwMDAwMAoyMTU3ICAgICAgIDAxLU1BUi0xOCAgMTMuMzIwMDAwMAoyMTU4ICAgICAgIDAxLU1BUi0xOCAgMjAuOTYwMDAwMAoyMTU5ICAgICAgIDAxLU1BUi0xOCAgMTYuOTUwMDAwMAoyMTYwICAgICAgIDAxLU1BUi0xOCAgMTkuNjgwMDAwMAoyMTYxICAgICAgIDAxLU1BUi0xOCAgMTcuMzgwMDAwMAoyMTYyICAgICAgIDAxLU1BUi0xOCAgMjAuODIwMDAwMAoyMTYzICAgICAgIDAxLU1BUi0xOCAgMTMuMDkwMDAwMAoyMTY0ICAgICAgIDAxLU1BUi0xOCAgMTguODgwMDAwMAoyMTY1ICAgICAgIDAxLU1BUi0xOCAgMTMuMjcwMDAwMAoyNTAxICAgICAgIDAxLU1BUi0xOCAgMTkuOTQwMDAwMAoyNTAyICAgICAgIDAxLU1BUi0xOCAgMTguMzIwMDAwMAoyNTAzICAgICAgIDAxLU1BUi0xOCAgLjAwMDAwMDAKMjUwNCAgICAgICAwMS1NQVItMTggIDE5Ljk0MDAwMDAKMjUwNSAgICAgICAwMS1NQVItMTggIDEyLjc5MDAwMDAKMjUwNiAgICAgICAwMS1NQVItMTggIDE5LjMzMDAwMDAKMjUwNyAgICAgICAwMS1NQVItMTggIC4wMDAwMDAwCjI1MDggICAgICAgMDEtTUFSLTE4ICA0LjMzMDAwMDAKMjUwOSAgICAgICAwMS1NQVItMTggIDE3Ljg3MDAwMDAKMjUxMCAgICAgICAwMS1NQVItMTggIDE2LjM0MDAwMDAK");
//        siteDataFile.updateFileNameWithUniquePrefix(messageHeader.getTransactionID());
//
//        SiteFileMessage siteFilesMessage = new SiteFileMessage();
//        siteFilesMessage.setMessageSource(messageHeader);
//        siteFilesMessage.setSiteDataFile(siteDataFile);
//        return siteFilesMessage;
//    }
//
//    public static EnhancedDataField getEnhancedDataField() {
//        EnhancedDataField enhancedDataField = new EnhancedDataField();
//        enhancedDataField.setSiteDataField(getSiteDataField());
//        return enhancedDataField;
//    }
//
//
//    private static SiteDataField getSiteDataField() {
//        SiteDataField siteDataField = new SiteDataField();
//        siteDataField.setTomsDbLocation("promptData44");
//        siteDataField.setFieldValue("promptData44");
//        siteDataField.setFieldType("String");
//        siteDataField.setFieldName("name");
//        siteDataField.setFieldExcludedFromSource(false);
//        return siteDataField;
//    }
//
//    public static SiteXformConfiguration getSiteXformConfigurationForSite(String siteId, List<SiteXformConfiguration> siteXformConfigurations) {
//        return siteXformConfigurations.stream().filter(s -> s.getSiteId().equals(siteId)).findFirst().get();
//    }
//
//    public static List<SiteControllerEventMessage> siteControllerEventMessages() {
//
//        MessageHeader messageHeader = new MessageHeader("kearl", "kearl", new Date().toString(), UUID.randomUUID().toString());
//
//        List<String> fromEmailAddresses = new ArrayList<>();
//        fromEmailAddresses.add("bhushan@middleware360.com");
//
//        EmailSourceEvent emailSourceEvent = new EmailSourceEvent(fromEmailAddresses, "/error", "/archive", "/destination");
//
//        SftpSourceEvent sftpSourceEvent = new SftpSourceEvent("/source", "/destination", "/archive");
//
//        SiteControllerEventMessage siteControllerEventMessage = new SiteControllerEventMessage(messageHeader, emailSourceEvent, sftpSourceEvent);
//
//        List<SiteControllerEventMessage> siteControllerEventMessages = new ArrayList<>();
//        siteControllerEventMessages.add(siteControllerEventMessage);
//
//        return siteControllerEventMessages;
//    }
//
//    public static R5TrackResultContextHelper r5TrackResultContextHelper() {
//        List<ErrorContext> errorsResult = new ArrayList<>();
//        List<ErrorContext> skipResult = new ArrayList<>();
//        List<R5TrackCO> successResult = new ArrayList<>();
//
//        errorsResult.add(new ErrorContext("Instantiating BootstrapContext using constructor", "error"));
//        R5TrackCO r5TrackCO = new R5TrackCO();
//        r5TrackCO.setTkdTrans("1234");
//        successResult.add(r5TrackCO);
//
//        R5TrackResultContext r5TrackResultContext = new R5TrackResultContext(errorsResult, skipResult, successResult);
//
//
//
//        R5TrackResultContextHelper r5TrackResultContextHelper = new R5TrackResultContextHelper();
//        r5TrackResultContextHelper.setR5TrackResultContext(r5TrackResultContext);
//        r5TrackResultContextHelper.setRequestId(UUID.randomUUID().toString());
//        r5TrackResultContextHelper.setSiteFileMessage(siteFileMessage);
//
//        return r5TrackResultContextHelper;
//
//    }



}
