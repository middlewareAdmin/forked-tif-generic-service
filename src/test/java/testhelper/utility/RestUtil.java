package testhelper.utility;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.RequestSpecification;

public class RestUtil {


    /**
     * createUri
     * @param host
     * @param port
     * @param apiVersion
     * @param action
     * @return
     */
    public static String createUri(String host, String port,String apiVersion, String action) {
        if(apiVersion == null){
            return String.format("http://%s:%s/%s", host, port, action);
        }
        else {
            return String.format("http://%s:%s/%s/%s", host, port, apiVersion, action);
        }
    }

    /**
     * getBasicAuth
     * @param userName
     * @param password
     * @return
     */
    public static RequestSpecification getBasicAuth(String userName, String password) {
        return RestAssured.given().auth().preemptive().basic(userName, password);
    }

    /**
     * createUri
     * @param host
     * @param port
     * @param contextPath
     * @param action
     * @param siteId
     * @return
     */
    public static String createUri(String host, String port,String contextPath, String action, String siteId) {
        return String.format("http://%s:%s/%s/%s/%s", host, port, contextPath, action, siteId);
    }

}
