package testhelper.context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mdw360.service.actuator.MetricsService;
import com.mdw360.service.component.DateHolder;
import com.mdw360.service.component.handler.ExceptionHandler;
import com.mdw360.service.component.handler.ExceptionHandlerImpl;
import org.springframework.boot.actuate.metrics.repository.InMemoryMetricRepository;
import org.springframework.boot.actuate.metrics.writer.DefaultCounterService;
import org.springframework.boot.actuate.metrics.writer.MetricWriter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@TestConfiguration
public class ProcessContext {

    @Bean("restTemplate")
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ExceptionHandler exceptionHandler() {
        return new ExceptionHandlerImpl();
    }

    @Bean
    public MetricsService metricsService() {
        return new MetricsService();
    }

    @Bean
    public DefaultCounterService counterService() {
        MetricWriter metricWriter = new InMemoryMetricRepository();
        return new DefaultCounterService(metricWriter);
    }

    @Bean
    public DateHolder dateHolder() {
        return new DateHolder();
    }




    @Bean
    @ConditionalOnMissingBean
    public ObjectMapper jsonObjectMapper() {
        return new ObjectMapper();
    }




}

