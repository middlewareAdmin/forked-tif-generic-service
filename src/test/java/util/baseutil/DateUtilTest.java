package util.baseutil;

import org.hamcrest.core.Is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;

import static com.mdw360.service.util.baseutil.DateUtil.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class DateUtilTest {


    @Test
    public void testIsValidDateFormatPattern() {
       boolean isValid =  isValidDateFormatPattern("dd/yy/mm");
       assertTrue(isValid);
    }

    @Test
    public void testInvalidValidDateFormatPattern() {
        boolean isValid =  isValidDateFormatPattern("ll/yy/mm");
        assertFalse(isValid);
    }

    @Test
    public void testIsValidDateFormatPerDateValue() {
        boolean isValid = isValidDateFormatPerDateValue("12/05/10", "dd/MM/YY");
        assertTrue(isValid);
    }

    @Test
    public void testInvalidValidDateFormatPerDateValue() {
        boolean isValid = isValidDateFormatPerDateValue("12/aa/10", "dd/MM/YY");
        assertFalse(isValid);
    }

    @Test
    public void testGetDateWithOutputFormat() throws ParseException {
        String output = getDateWithOutputFormat("12/05/10", "dd/MM/yy", "yy/dd/MM");
        assertThat(output, Is.is("10/12/05"));
    }

    @Test(expected = ParseException.class)
    public void testGetDateWithOutputFormatForInvalidValue() throws ParseException {
        String output = getDateWithOutputFormat("12/0q/10", "dd/MM/yy", "yy/dd/MM");
    }

    @Test
    public void testIsValidDateFormat() {
        boolean isValid = isValidDateFormat("12/25/10");
        assertTrue(isValid);
    }

    @Test
    public void testIsInValidDateFormat() {
        boolean isValid = isValidDateFormat("12/AA/10");
        assertFalse(isValid);
    }

}
