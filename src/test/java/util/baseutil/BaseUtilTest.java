package util.baseutil;

import org.hamcrest.core.Is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.mdw360.service.util.baseutil.BaseUtil.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class BaseUtilTest {

    @Test
    public void testEncodeToStringWithByteCode() {
        String result = "Hello World!";
        String encodeInput = encodeToString(result.getBytes());
        assertNotNull(encodeInput);
        assertTrue(!encodeInput.equals(result));
    }

    @Test
    public void testDecodeToString() {
        String input = "Hello";
        String encodeInput = encodeToString(input.getBytes());
        assertNotNull(encodeInput);
        assertTrue(!encodeInput.equals(input));

        String decode = decodeToString(encodeInput);
        assertTrue(decode.equals(input));

    }

    @Test
    public void testconcatArgs() {
        String url = concatArgs("www.google.com","/search");
        assertThat(url, Is.is("www.google.com/search"));
    }

    @Test
    public void testCreateHeaderWithBasicAuth() {
        HttpHeaders httpHeaders = createHeaderWithBasicAuth("TIF", "PASSWORD");
        List<String> data = httpHeaders.get("Authorization");
        assertNotNull(data);
        assertThat(data.get(0), Is.is("Basic VElGOlBBU1NXT1JE"));
    }

    @Test
    public void testCreateHeaderWithBasicAuthAndJsonContentType() {
        HttpHeaders httpHeaders = createHeaderWithBasicAuthAndJsonContentType("TIF", "PASSWORD");
        assertThat(httpHeaders.getContentType(), Is.is(MediaType.APPLICATION_JSON));
        assertNotNull(httpHeaders.get("Authorization"));
    }
}
