package service.transformation;

import com.mdw360.service.model.meterdata.configuration.R5TrackResultContext;
import com.mdw360.service.transform.CurrentWeatherToR5TrackResultContextImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CurrentWeatherToR5TrackResultContextImplTest {

    CurrentWeatherToR5TrackResultContextImpl payloadToR5TrackResultContext;
    String payload = "{\"cnt\":1,\"list\":[{\"coord\":{\"lon\":-119.27,\"lat\":50.26},\"sys\":{\"country\":\"CA\",\"timezone\":-28800,\"sunrise\":1580830049,\"sunset\":1580864066},\"weather\":[{\"id\":600,\"main\":\"Snow\",\"description\":\"light snow\",\"icon\":\"13d\"}],\"main\":{\"temp\":-5.29,\"feels_like\":-9.95,\"temp_min\":-7.22,\"temp_max\":-3.33,\"pressure\":1023,\"humidity\":85},\"visibility\":8047,\"wind\":{\"speed\":2.6,\"deg\":110},\"clouds\":{\"all\":90},\"dt\":1580850951,\"id\":6173864,\"name\":\"Vernon\"}]}";

    @Before
    public void init() throws Exception {
        payloadToR5TrackResultContext = new CurrentWeatherToR5TrackResultContextImpl();
    }

    @Test
    public void testLongAndLat() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
        //Long
        assertEquals("-119.27",r5TrackResultContext.getSuccessResult().get(0).getPromptData1());
        //Lat
        assertEquals("50.26",r5TrackResultContext.getSuccessResult().get(0).getPromptData2());
    }

    @Test
    public void testCountryAndTimeData() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
        //Country
        assertEquals("CA",r5TrackResultContext.getSuccessResult().get(0).getPromptData3());
        //Timezone
        assertEquals("-28800",r5TrackResultContext.getSuccessResult().get(0).getPromptData4());
        //Sunrise
        assertEquals("2020-02-04 07:02:00",r5TrackResultContext.getSuccessResult().get(0).getPromptData5());
        //Sunset
        assertEquals("2020-02-04 16:02:00",r5TrackResultContext.getSuccessResult().get(0).getPromptData6());
        assertEquals("KTW", r5TrackResultContext.getSuccessResult().get(0).getTkdTrans());
    }

    @Test
    public void testWeatherAndTemps() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
        //Weather Id
        assertEquals("600",r5TrackResultContext.getSuccessResult().get(0).getPromptData7());
        //Weather Main
        assertEquals("Snow",r5TrackResultContext.getSuccessResult().get(0).getPromptData8());
        //Weather Descr
        assertEquals("light snow",r5TrackResultContext.getSuccessResult().get(0).getPromptData9());
        //Temp
        assertEquals("-5.29",r5TrackResultContext.getSuccessResult().get(0).getPromptData10());
        //Feels Like
        assertEquals("-9.95",r5TrackResultContext.getSuccessResult().get(0).getPromptData11());
        //temp_min
        assertEquals("-7.22",r5TrackResultContext.getSuccessResult().get(0).getPromptData12());
        //temp_max
        assertEquals("-3.33",r5TrackResultContext.getSuccessResult().get(0).getPromptData13());
        //pressure
        assertEquals("1023",r5TrackResultContext.getSuccessResult().get(0).getPromptData14());
        //humidity
        assertEquals("85",r5TrackResultContext.getSuccessResult().get(0).getPromptData15());
    }

    @Test
    public void testVisibilityAndWindData() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
        //Visibility
        assertEquals("8047",r5TrackResultContext.getSuccessResult().get(0).getPromptData16());
        //Wind Speed
        assertEquals("2.6",r5TrackResultContext.getSuccessResult().get(0).getPromptData17());
        //Wind Deg
        assertEquals("110",r5TrackResultContext.getSuccessResult().get(0).getPromptData18());
        //Clouds
        assertEquals("90",r5TrackResultContext.getSuccessResult().get(0).getPromptData19());
    }

    @Test
    public void testRequestData() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
        //dt
        assertEquals("2020-02-04 13:02:00",r5TrackResultContext.getSuccessResult().get(0).getPromptData20());
        //id
        assertEquals("6173864",r5TrackResultContext.getSuccessResult().get(0).getPromptData21());
        //name
        assertEquals("Vernon",r5TrackResultContext.getSuccessResult().get(0).getPromptData22());
    }

}
