package service.transformation;

import com.mdw360.service.model.meterdata.configuration.R5TrackResultContext;
import com.mdw360.service.transform.ForecastWeatherToR5TrackResultContextImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ForecastWeatherToR5TrackResultContextImplTest {

    ForecastWeatherToR5TrackResultContextImpl payloadToR5TrackResultContext;

    @Before
    public void init() throws Exception {
        payloadToR5TrackResultContext = new ForecastWeatherToR5TrackResultContextImpl();
    }

    @Test
    public void testTransId() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
    }

    @Test
    public void testLongAndLatAndSnow() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
        //Long
        assertEquals("-119.2691",r5TrackResultContext.getSuccessResult().get(0).getPromptData1());
        assertEquals("-119.2691",r5TrackResultContext.getSuccessResult().get(8).getPromptData1());
        assertEquals("-119.2691",r5TrackResultContext.getSuccessResult().get(9).getPromptData1());

        //Lat
        assertEquals("50.2581",r5TrackResultContext.getSuccessResult().get(0).getPromptData2());
        assertEquals("50.2581",r5TrackResultContext.getSuccessResult().get(8).getPromptData2());
        assertEquals("50.2581",r5TrackResultContext.getSuccessResult().get(9).getPromptData2());
        //Snow
        assertEquals("0.56",r5TrackResultContext.getSuccessResult().get(0).getPromptData26());
        assertEquals("0.13",r5TrackResultContext.getSuccessResult().get(8).getPromptData26());
        assertEquals(null,r5TrackResultContext.getSuccessResult().get(9).getPromptData26());
    }

    @Test
    public void testCountryAndTimeData() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
        //Country
        assertEquals("CA",r5TrackResultContext.getSuccessResult().get(0).getPromptData3());
        assertEquals("CA",r5TrackResultContext.getSuccessResult().get(8).getPromptData3());
        assertEquals("CA",r5TrackResultContext.getSuccessResult().get(9).getPromptData3());

        //Timezone
        assertEquals("-28800",r5TrackResultContext.getSuccessResult().get(0).getPromptData4());
        assertEquals("-28800",r5TrackResultContext.getSuccessResult().get(8).getPromptData4());
        assertEquals("-28800",r5TrackResultContext.getSuccessResult().get(9).getPromptData4());

        //Sunrise
        assertEquals("2020-02-07 07:02:00",r5TrackResultContext.getSuccessResult().get(0).getPromptData5());
        assertEquals("2020-02-07 07:02:00",r5TrackResultContext.getSuccessResult().get(8).getPromptData5());
        assertEquals("2020-02-07 07:02:00",r5TrackResultContext.getSuccessResult().get(9).getPromptData5());

        //Sunset
        assertEquals("2020-02-07 16:02:00",r5TrackResultContext.getSuccessResult().get(0).getPromptData6());
        assertEquals("2020-02-07 16:02:00",r5TrackResultContext.getSuccessResult().get(8).getPromptData6());
        assertEquals("2020-02-07 16:02:00",r5TrackResultContext.getSuccessResult().get(9).getPromptData6());
    }

    @Test
    public void testWeatherAndTemps() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
        //Weather Id
        assertEquals("600",r5TrackResultContext.getSuccessResult().get(0).getPromptData7());
        assertEquals("600",r5TrackResultContext.getSuccessResult().get(8).getPromptData7());
        assertEquals("804",r5TrackResultContext.getSuccessResult().get(9).getPromptData7());
        assertEquals("800",r5TrackResultContext.getSuccessResult().get(12).getPromptData7());

        //Weather Main
        assertEquals("Snow",r5TrackResultContext.getSuccessResult().get(0).getPromptData8());
        assertEquals("Snow",r5TrackResultContext.getSuccessResult().get(8).getPromptData8());
        assertEquals("Clouds",r5TrackResultContext.getSuccessResult().get(9).getPromptData8());

        //Weather Descr
        assertEquals("light snow",r5TrackResultContext.getSuccessResult().get(0).getPromptData9());
        assertEquals("light snow",r5TrackResultContext.getSuccessResult().get(8).getPromptData9());
        assertEquals("overcast clouds",r5TrackResultContext.getSuccessResult().get(9).getPromptData9());

        //Temp
        assertEquals("-0.38",r5TrackResultContext.getSuccessResult().get(0).getPromptData10());
        assertEquals("-3.11",r5TrackResultContext.getSuccessResult().get(8).getPromptData10());
        assertEquals("-4.24",r5TrackResultContext.getSuccessResult().get(9).getPromptData10());

        //Feels Like
        assertEquals("-3.72",r5TrackResultContext.getSuccessResult().get(0).getPromptData11());
        assertEquals("-7.67",r5TrackResultContext.getSuccessResult().get(8).getPromptData11());
        assertEquals("-8.48",r5TrackResultContext.getSuccessResult().get(9).getPromptData11());

        //temp_min
        assertEquals("-3",r5TrackResultContext.getSuccessResult().get(0).getPromptData12());
        assertEquals("-3.11",r5TrackResultContext.getSuccessResult().get(8).getPromptData12());
        assertEquals("-4.24",r5TrackResultContext.getSuccessResult().get(9).getPromptData12());

        //temp_max
        assertEquals("-0.38",r5TrackResultContext.getSuccessResult().get(0).getPromptData13());
        assertEquals("-3.11",r5TrackResultContext.getSuccessResult().get(8).getPromptData13());
        assertEquals("-4.24",r5TrackResultContext.getSuccessResult().get(9).getPromptData13());

        //pressure
        assertEquals("1015",r5TrackResultContext.getSuccessResult().get(0).getPromptData14());
        assertEquals("1019",r5TrackResultContext.getSuccessResult().get(8).getPromptData14());
        assertEquals("1023",r5TrackResultContext.getSuccessResult().get(9).getPromptData14());

        //humidity
        assertEquals("95",r5TrackResultContext.getSuccessResult().get(0).getPromptData15());
        assertEquals("84",r5TrackResultContext.getSuccessResult().get(8).getPromptData15());
        assertEquals("89",r5TrackResultContext.getSuccessResult().get(9).getPromptData15());

        //sea_level          |promptData23|
        assertEquals("888",r5TrackResultContext.getSuccessResult().get(0).getPromptData23());
        assertEquals("892",r5TrackResultContext.getSuccessResult().get(8).getPromptData23());
        assertEquals("895",r5TrackResultContext.getSuccessResult().get(9).getPromptData23());

        //grnd_level         |promptData24|
        assertEquals("1015",r5TrackResultContext.getSuccessResult().get(0).getPromptData24());
        assertEquals("1019",r5TrackResultContext.getSuccessResult().get(8).getPromptData24());
        assertEquals("1023",r5TrackResultContext.getSuccessResult().get(9).getPromptData24());

        //temp_kf            |promptData25|
        assertEquals("2.62",r5TrackResultContext.getSuccessResult().get(0).getPromptData25());
        assertEquals("0",r5TrackResultContext.getSuccessResult().get(8).getPromptData25());
        assertEquals("0",r5TrackResultContext.getSuccessResult().get(9).getPromptData25());
    }

    @Test
    public void testVisibilityAndWindData() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
        //Visibility not in Forecast only in Current
        assertEquals(null ,r5TrackResultContext.getSuccessResult().get(0).getPromptData16());
        assertEquals(null ,r5TrackResultContext.getSuccessResult().get(8).getPromptData16());
        assertEquals(null ,r5TrackResultContext.getSuccessResult().get(9).getPromptData16());

        //Wind Speed
        assertEquals("1.71",r5TrackResultContext.getSuccessResult().get(0).getPromptData17());
        assertEquals("2.72",r5TrackResultContext.getSuccessResult().get(8).getPromptData17());
        assertEquals("2.22",r5TrackResultContext.getSuccessResult().get(9).getPromptData17());

        //Wind Deg
        assertEquals("118",r5TrackResultContext.getSuccessResult().get(0).getPromptData18());
        assertEquals("295",r5TrackResultContext.getSuccessResult().get(8).getPromptData18());
        assertEquals("299",r5TrackResultContext.getSuccessResult().get(9).getPromptData18());

        //Clouds
        assertEquals("100",r5TrackResultContext.getSuccessResult().get(0).getPromptData19());
        assertEquals("97",r5TrackResultContext.getSuccessResult().get(8).getPromptData19());
        assertEquals("99",r5TrackResultContext.getSuccessResult().get(9).getPromptData19());
    }

    @Test
    public void testRequestData() throws Exception {
        R5TrackResultContext r5TrackResultContext = payloadToR5TrackResultContext.transform(payload, "KTW");
        //dt
        assertEquals("2020-02-07 21:00:00",r5TrackResultContext.getSuccessResult().get(0).getPromptData20());
        assertEquals("2020-02-08 21:00:00",r5TrackResultContext.getSuccessResult().get(8).getPromptData20());
        assertEquals("2020-02-09 00:00:00",r5TrackResultContext.getSuccessResult().get(9).getPromptData20());

        //id
        assertEquals("6173864",r5TrackResultContext.getSuccessResult().get(0).getPromptData21());
        assertEquals("6173864",r5TrackResultContext.getSuccessResult().get(8).getPromptData21());
        assertEquals("6173864",r5TrackResultContext.getSuccessResult().get(9).getPromptData21());

        //name
        assertEquals("Vernon",r5TrackResultContext.getSuccessResult().get(0).getPromptData22());
        assertEquals("Vernon",r5TrackResultContext.getSuccessResult().get(8).getPromptData22());
        assertEquals("Vernon",r5TrackResultContext.getSuccessResult().get(9).getPromptData22());

    }


    String payload = "{\n" +
            "\"cod\": \"200\",\n" +
            "\"message\": 0,\n" +
            "\"cnt\": 40,\n" +
            "\"list\": [\n" +
            "{\n" +
            "\"dt\": 1581109200,\n" +
            "\"main\": {\n" +
            "\"temp\": -0.38,\n" +
            "\"feels_like\": -3.72,\n" +
            "\"temp_min\": -3,\n" +
            "\"temp_max\": -0.38,\n" +
            "\"pressure\": 1015,\n" +
            "\"sea_level\": 1015,\n" +
            "\"grnd_level\": 888,\n" +
            "\"humidity\": 95,\n" +
            "\"temp_kf\": 2.62\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 600,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"light snow\",\n" +
            "\"icon\": \"13d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.71,\n" +
            "\"deg\": 118\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 0.56\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-07 21:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581120000,\n" +
            "\"main\": {\n" +
            "\"temp\": -1.01,\n" +
            "\"feels_like\": -4.5,\n" +
            "\"temp_min\": -2.98,\n" +
            "\"temp_max\": -1.01,\n" +
            "\"pressure\": 1010,\n" +
            "\"sea_level\": 1010,\n" +
            "\"grnd_level\": 884,\n" +
            "\"humidity\": 96,\n" +
            "\"temp_kf\": 1.97\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 600,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"light snow\",\n" +
            "\"icon\": \"13d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.84,\n" +
            "\"deg\": 98\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 1.44\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-08 00:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581130800,\n" +
            "\"main\": {\n" +
            "\"temp\": -2.63,\n" +
            "\"feels_like\": -6.38,\n" +
            "\"temp_min\": -3.94,\n" +
            "\"temp_max\": -2.63,\n" +
            "\"pressure\": 1006,\n" +
            "\"sea_level\": 1006,\n" +
            "\"grnd_level\": 880,\n" +
            "\"humidity\": 97,\n" +
            "\"temp_kf\": 1.31\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 600,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"light snow\",\n" +
            "\"icon\": \"13n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.94,\n" +
            "\"deg\": 94\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 1.5\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-08 03:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581141600,\n" +
            "\"main\": {\n" +
            "\"temp\": -2.85,\n" +
            "\"feels_like\": -6.77,\n" +
            "\"temp_min\": -3.51,\n" +
            "\"temp_max\": -2.85,\n" +
            "\"pressure\": 1005,\n" +
            "\"sea_level\": 1005,\n" +
            "\"grnd_level\": 878,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0.66\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 601,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"snow\",\n" +
            "\"icon\": \"13n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 2.17,\n" +
            "\"deg\": 133\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 2.75\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-08 06:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581152400,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.67,\n" +
            "\"feels_like\": -6.68,\n" +
            "\"temp_min\": -3.67,\n" +
            "\"temp_max\": -3.67,\n" +
            "\"pressure\": 1005,\n" +
            "\"sea_level\": 1005,\n" +
            "\"grnd_level\": 879,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 600,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"light snow\",\n" +
            "\"icon\": \"13n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.73,\n" +
            "\"deg\": 109\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 0.19\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-08 09:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581163200,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.97,\n" +
            "\"feels_like\": -7.92,\n" +
            "\"temp_min\": -3.97,\n" +
            "\"temp_max\": -3.97,\n" +
            "\"pressure\": 1008,\n" +
            "\"sea_level\": 1008,\n" +
            "\"grnd_level\": 881,\n" +
            "\"humidity\": 96,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 601,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"snow\",\n" +
            "\"icon\": \"13n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.99,\n" +
            "\"deg\": 286\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 1.56\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-08 12:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581174000,\n" +
            "\"main\": {\n" +
            "\"temp\": -5.67,\n" +
            "\"feels_like\": -10.02,\n" +
            "\"temp_min\": -5.67,\n" +
            "\"temp_max\": -5.67,\n" +
            "\"pressure\": 1012,\n" +
            "\"sea_level\": 1012,\n" +
            "\"grnd_level\": 884,\n" +
            "\"humidity\": 95,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 600,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"light snow\",\n" +
            "\"icon\": \"13n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 2.29,\n" +
            "\"deg\": 290\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 0.5\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-08 15:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581184800,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.75,\n" +
            "\"feels_like\": -8.2,\n" +
            "\"temp_min\": -3.75,\n" +
            "\"temp_max\": -3.75,\n" +
            "\"pressure\": 1015,\n" +
            "\"sea_level\": 1015,\n" +
            "\"grnd_level\": 888,\n" +
            "\"humidity\": 89,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 97\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 2.58,\n" +
            "\"deg\": 294\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-08 18:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581195600,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.11,\n" +
            "\"feels_like\": -7.67,\n" +
            "\"temp_min\": -3.11,\n" +
            "\"temp_max\": -3.11,\n" +
            "\"pressure\": 1019,\n" +
            "\"sea_level\": 1019,\n" +
            "\"grnd_level\": 892,\n" +
            "\"humidity\": 84,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 600,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"light snow\",\n" +
            "\"icon\": \"13d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 97\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 2.72,\n" +
            "\"deg\": 295\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 0.13\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-08 21:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581206400,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.24,\n" +
            "\"feels_like\": -8.48,\n" +
            "\"temp_min\": -4.24,\n" +
            "\"temp_max\": -4.24,\n" +
            "\"pressure\": 1023,\n" +
            "\"sea_level\": 1023,\n" +
            "\"grnd_level\": 895,\n" +
            "\"humidity\": 89,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 99\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 2.22,\n" +
            "\"deg\": 299\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 00:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581217200,\n" +
            "\"main\": {\n" +
            "\"temp\": -8.62,\n" +
            "\"feels_like\": -13.25,\n" +
            "\"temp_min\": -8.62,\n" +
            "\"temp_max\": -8.62,\n" +
            "\"pressure\": 1030,\n" +
            "\"sea_level\": 1030,\n" +
            "\"grnd_level\": 899,\n" +
            "\"humidity\": 92,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 802,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"scattered clouds\",\n" +
            "\"icon\": \"03n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 30\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 2.28,\n" +
            "\"deg\": 295\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 03:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581228000,\n" +
            "\"main\": {\n" +
            "\"temp\": -10.44,\n" +
            "\"feels_like\": -14.97,\n" +
            "\"temp_min\": -10.44,\n" +
            "\"temp_max\": -10.44,\n" +
            "\"pressure\": 1034,\n" +
            "\"sea_level\": 1034,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 93,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 801,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"few clouds\",\n" +
            "\"icon\": \"02n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 15\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.97,\n" +
            "\"deg\": 292\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 06:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581238800,\n" +
            "\"main\": {\n" +
            "\"temp\": -11.62,\n" +
            "\"feels_like\": -15.91,\n" +
            "\"temp_min\": -11.62,\n" +
            "\"temp_max\": -11.62,\n" +
            "\"pressure\": 1037,\n" +
            "\"sea_level\": 1037,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 93,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 800,\n" +
            "\"main\": \"Clear\",\n" +
            "\"description\": \"clear sky\",\n" +
            "\"icon\": \"01n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 0\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.51,\n" +
            "\"deg\": 286\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 09:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581249600,\n" +
            "\"main\": {\n" +
            "\"temp\": -12.57,\n" +
            "\"feels_like\": -16.67,\n" +
            "\"temp_min\": -12.57,\n" +
            "\"temp_max\": -12.57,\n" +
            "\"pressure\": 1037,\n" +
            "\"sea_level\": 1037,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 93,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 800,\n" +
            "\"main\": \"Clear\",\n" +
            "\"description\": \"clear sky\",\n" +
            "\"icon\": \"01n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 0\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.17,\n" +
            "\"deg\": 286\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 12:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581260400,\n" +
            "\"main\": {\n" +
            "\"temp\": -13.39,\n" +
            "\"feels_like\": -17.37,\n" +
            "\"temp_min\": -13.39,\n" +
            "\"temp_max\": -13.39,\n" +
            "\"pressure\": 1038,\n" +
            "\"sea_level\": 1038,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 94,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 800,\n" +
            "\"main\": \"Clear\",\n" +
            "\"description\": \"clear sky\",\n" +
            "\"icon\": \"01n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 0\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.93,\n" +
            "\"deg\": 293\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 15:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581271200,\n" +
            "\"main\": {\n" +
            "\"temp\": -7.77,\n" +
            "\"feels_like\": -11.04,\n" +
            "\"temp_min\": -7.77,\n" +
            "\"temp_max\": -7.77,\n" +
            "\"pressure\": 1036,\n" +
            "\"sea_level\": 1036,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 89,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 802,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"scattered clouds\",\n" +
            "\"icon\": \"03d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 29\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.38,\n" +
            "\"deg\": 17\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 18:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581282000,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.47,\n" +
            "\"feels_like\": -7.71,\n" +
            "\"temp_min\": -4.47,\n" +
            "\"temp_max\": -4.47,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 87,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.71,\n" +
            "\"deg\": 52\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 21:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581292800,\n" +
            "\"main\": {\n" +
            "\"temp\": -5.18,\n" +
            "\"feels_like\": -8.28,\n" +
            "\"temp_min\": -5.18,\n" +
            "\"temp_max\": -5.18,\n" +
            "\"pressure\": 1031,\n" +
            "\"sea_level\": 1031,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 94,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.56,\n" +
            "\"deg\": 33\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 00:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581303600,\n" +
            "\"main\": {\n" +
            "\"temp\": -9.43,\n" +
            "\"feels_like\": -13.1,\n" +
            "\"temp_min\": -9.43,\n" +
            "\"temp_max\": -9.43,\n" +
            "\"pressure\": 1032,\n" +
            "\"sea_level\": 1032,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 94,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 99\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.85,\n" +
            "\"deg\": 286\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 03:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581314400,\n" +
            "\"main\": {\n" +
            "\"temp\": -10.09,\n" +
            "\"feels_like\": -13.93,\n" +
            "\"temp_min\": -10.09,\n" +
            "\"temp_max\": -10.09,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 91,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 89\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.99,\n" +
            "\"deg\": 284\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 06:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581325200,\n" +
            "\"main\": {\n" +
            "\"temp\": -9.38,\n" +
            "\"feels_like\": -13.05,\n" +
            "\"temp_min\": -9.38,\n" +
            "\"temp_max\": -9.38,\n" +
            "\"pressure\": 1032,\n" +
            "\"sea_level\": 1032,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 91,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 99\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.81,\n" +
            "\"deg\": 299\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 09:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581336000,\n" +
            "\"main\": {\n" +
            "\"temp\": -10.54,\n" +
            "\"feels_like\": -14.36,\n" +
            "\"temp_min\": -10.54,\n" +
            "\"temp_max\": -10.54,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 92,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 96\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.93,\n" +
            "\"deg\": 305\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 12:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581346800,\n" +
            "\"main\": {\n" +
            "\"temp\": -11.84,\n" +
            "\"feels_like\": -15.77,\n" +
            "\"temp_min\": -11.84,\n" +
            "\"temp_max\": -11.84,\n" +
            "\"pressure\": 1034,\n" +
            "\"sea_level\": 1034,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 93,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 801,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"few clouds\",\n" +
            "\"icon\": \"02n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 14\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.98,\n" +
            "\"deg\": 325\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 15:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581357600,\n" +
            "\"main\": {\n" +
            "\"temp\": -6.32,\n" +
            "\"feels_like\": -9.68,\n" +
            "\"temp_min\": -6.32,\n" +
            "\"temp_max\": -6.32,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 90,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 801,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"few clouds\",\n" +
            "\"icon\": \"02d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 16\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.7,\n" +
            "\"deg\": 59\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 18:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581368400,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.44,\n" +
            "\"feels_like\": -6.58,\n" +
            "\"temp_min\": -3.44,\n" +
            "\"temp_max\": -3.44,\n" +
            "\"pressure\": 1031,\n" +
            "\"sea_level\": 1031,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 90,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 97\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.78,\n" +
            "\"deg\": 86\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 21:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581379200,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.16,\n" +
            "\"feels_like\": -7.47,\n" +
            "\"temp_min\": -4.16,\n" +
            "\"temp_max\": -4.16,\n" +
            "\"pressure\": 1031,\n" +
            "\"sea_level\": 1031,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 95,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 98\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.03,\n" +
            "\"deg\": 89\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 00:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581390000,\n" +
            "\"main\": {\n" +
            "\"temp\": -6.63,\n" +
            "\"feels_like\": -9.6,\n" +
            "\"temp_min\": -6.63,\n" +
            "\"temp_max\": -6.63,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 97,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 99\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.23,\n" +
            "\"deg\": 88\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 03:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581400800,\n" +
            "\"main\": {\n" +
            "\"temp\": -8,\n" +
            "\"feels_like\": -11.01,\n" +
            "\"temp_min\": -8,\n" +
            "\"temp_max\": -8,\n" +
            "\"pressure\": 1034,\n" +
            "\"sea_level\": 1034,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 97,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 90\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.11,\n" +
            "\"deg\": 353\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 06:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581411600,\n" +
            "\"main\": {\n" +
            "\"temp\": -9,\n" +
            "\"feels_like\": -12.11,\n" +
            "\"temp_min\": -9,\n" +
            "\"temp_max\": -9,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 97,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.14,\n" +
            "\"deg\": 72\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 09:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581422400,\n" +
            "\"main\": {\n" +
            "\"temp\": -9.74,\n" +
            "\"feels_like\": -13.11,\n" +
            "\"temp_min\": -9.74,\n" +
            "\"temp_max\": -9.74,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 97,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 80\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.44,\n" +
            "\"deg\": 56\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 12:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581433200,\n" +
            "\"main\": {\n" +
            "\"temp\": -10.39,\n" +
            "\"feels_like\": -13.86,\n" +
            "\"temp_min\": -10.39,\n" +
            "\"temp_max\": -10.39,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 52\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.52,\n" +
            "\"deg\": 61\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 15:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581444000,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.44,\n" +
            "\"feels_like\": -7.69,\n" +
            "\"temp_min\": -4.44,\n" +
            "\"temp_max\": -4.44,\n" +
            "\"pressure\": 1032,\n" +
            "\"sea_level\": 1032,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 92,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 52\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.84,\n" +
            "\"deg\": 89\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 18:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581454800,\n" +
            "\"main\": {\n" +
            "\"temp\": -1.71,\n" +
            "\"feels_like\": -4.64,\n" +
            "\"temp_min\": -1.71,\n" +
            "\"temp_max\": -1.71,\n" +
            "\"pressure\": 1029,\n" +
            "\"sea_level\": 1029,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 95,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 87\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.88,\n" +
            "\"deg\": 106\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 21:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581465600,\n" +
            "\"main\": {\n" +
            "\"temp\": -2.2,\n" +
            "\"feels_like\": -5.12,\n" +
            "\"temp_min\": -2.2,\n" +
            "\"temp_max\": -2.2,\n" +
            "\"pressure\": 1029,\n" +
            "\"sea_level\": 1029,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 96,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 93\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.81,\n" +
            "\"deg\": 127\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 00:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581476400,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.61,\n" +
            "\"feels_like\": -6.69,\n" +
            "\"temp_min\": -3.61,\n" +
            "\"temp_max\": -3.61,\n" +
            "\"pressure\": 1030,\n" +
            "\"sea_level\": 1030,\n" +
            "\"grnd_level\": 900,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.84,\n" +
            "\"deg\": 126\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 03:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581487200,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.32,\n" +
            "\"feels_like\": -7.52,\n" +
            "\"temp_min\": -4.32,\n" +
            "\"temp_max\": -4.32,\n" +
            "\"pressure\": 1030,\n" +
            "\"sea_level\": 1030,\n" +
            "\"grnd_level\": 900,\n" +
            "\"humidity\": 99,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.93,\n" +
            "\"deg\": 133\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 06:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581498000,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.45,\n" +
            "\"feels_like\": -7.67,\n" +
            "\"temp_min\": -4.45,\n" +
            "\"temp_max\": -4.45,\n" +
            "\"pressure\": 1030,\n" +
            "\"sea_level\": 1030,\n" +
            "\"grnd_level\": 899,\n" +
            "\"humidity\": 99,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.94,\n" +
            "\"deg\": 135\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 09:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581508800,\n" +
            "\"main\": {\n" +
            "\"temp\": -5.91,\n" +
            "\"feels_like\": -9.04,\n" +
            "\"temp_min\": -5.91,\n" +
            "\"temp_max\": -5.91,\n" +
            "\"pressure\": 1029,\n" +
            "\"sea_level\": 1029,\n" +
            "\"grnd_level\": 899,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.57,\n" +
            "\"deg\": 149\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 12:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581519600,\n" +
            "\"main\": {\n" +
            "\"temp\": -7.14,\n" +
            "\"feels_like\": -10.37,\n" +
            "\"temp_min\": -7.14,\n" +
            "\"temp_max\": -7.14,\n" +
            "\"pressure\": 1029,\n" +
            "\"sea_level\": 1029,\n" +
            "\"grnd_level\": 899,\n" +
            "\"humidity\": 97,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 95\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.54,\n" +
            "\"deg\": 303\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 15:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581530400,\n" +
            "\"main\": {\n" +
            "\"temp\": -1.54,\n" +
            "\"feels_like\": -4.11,\n" +
            "\"temp_min\": -1.54,\n" +
            "\"temp_max\": -1.54,\n" +
            "\"pressure\": 1029,\n" +
            "\"sea_level\": 1029,\n" +
            "\"grnd_level\": 900,\n" +
            "\"humidity\": 92,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 89\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.32,\n" +
            "\"deg\": 193\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 18:00:00\"\n" +
            "}\n" +
            "],\n" +
            "\"city\": {\n" +
            "\"id\": 6173864,\n" +
            "\"name\": \"Vernon\",\n" +
            "\"coord\": {\n" +
            "\"lat\": 50.2581,\n" +
            "\"lon\": -119.2691\n" +
            "},\n" +
            "\"country\": \"CA\",\n" +
            "\"timezone\": -28800,\n" +
            "\"sunrise\": 1581088965,\n" +
            "\"sunset\": 1581123581\n" +
            "}\n" +
            "}";

}
