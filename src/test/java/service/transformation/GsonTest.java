package service.transformation;

import com.google.gson.Gson;
import com.mdw360.service.model.domain.current.CurrentWeather;
import com.mdw360.service.model.domain.forecast.ForecastWeather;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class GsonTest {

    @Test
    public void testMarshall() throws Exception {
        Gson gson = new Gson();

        CurrentWeather currentWeather = gson.fromJson(currentWeatherPayload, CurrentWeather.class);
        log.info("Current Weather: {}", currentWeather.toString());

        ForecastWeather forecastWeather = gson.fromJson(forecastWeatherPayload, ForecastWeather.class);
        log.info("Forecast Weather: {}", forecastWeather.toString());

    }

    String currentWeatherPayload = "{\n" +
            "\"cnt\": 1,\n" +
            "\"list\": [\n" +
            "{\n" +
            "\"coord\": {\n" +
            "\"lon\": -111.62,\n" +
            "\"lat\": 57.18\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"country\": \"CA\",\n" +
            "\"timezone\": -25200,\n" +
            "\"sunrise\": 1581174744,\n" +
            "\"sunset\": 1581206939\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 701,\n" +
            "\"main\": \"Mist\",\n" +
            "\"description\": \"mist\",\n" +
            "\"icon\": \"50n\"\n" +
            "}\n" +
            "],\n" +
            "\"main\": {\n" +
            "\"temp\": -12,\n" +
            "\"feels_like\": -15.96,\n" +
            "\"temp_min\": -12,\n" +
            "\"temp_max\": -12,\n" +
            "\"pressure\": 1012,\n" +
            "\"humidity\": 92\n" +
            "},\n" +
            "\"visibility\": 2816,\n" +
            "\"wind\": {\n" +
            "\"speed\": 1,\n" +
            "\"deg\": 310\n" +
            "},\n" +
            "\"clouds\": {\n" +
            "\"all\": 90\n" +
            "},\n" +
            "\"dt\": 1581171369,\n" +
            "\"id\": 5955889,\n" +
            "\"name\": \"Fort MacKay\"\n" +
            "}\n" +
            "]\n" +
            "}";

    String forecastWeatherPayload = "{\n" +
            "\"cod\": \"200\",\n" +
            "\"message\": 0,\n" +
            "\"cnt\": 40,\n" +
            "\"list\": [\n" +
            "{\n" +
            "\"dt\": 1581184800,\n" +
            "\"main\": {\n" +
            "\"temp\": 1.42,\n" +
            "\"feels_like\": -2.41,\n" +
            "\"temp_min\": -3.91,\n" +
            "\"temp_max\": 1.42,\n" +
            "\"pressure\": 1016,\n" +
            "\"sea_level\": 1016,\n" +
            "\"grnd_level\": 889,\n" +
            "\"humidity\": 89,\n" +
            "\"temp_kf\": 5.33\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 82\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 2.59,\n" +
            "\"deg\": 291\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-08 18:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581195600,\n" +
            "\"main\": {\n" +
            "\"temp\": 0.79,\n" +
            "\"feels_like\": -3.28,\n" +
            "\"temp_min\": -3.21,\n" +
            "\"temp_max\": 0.79,\n" +
            "\"pressure\": 1020,\n" +
            "\"sea_level\": 1020,\n" +
            "\"grnd_level\": 893,\n" +
            "\"humidity\": 83,\n" +
            "\"temp_kf\": 4\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 97\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 2.63,\n" +
            "\"deg\": 295\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-08 21:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581206400,\n" +
            "\"main\": {\n" +
            "\"temp\": -1.64,\n" +
            "\"feels_like\": -5.59,\n" +
            "\"temp_min\": -4.3,\n" +
            "\"temp_max\": -1.64,\n" +
            "\"pressure\": 1024,\n" +
            "\"sea_level\": 1024,\n" +
            "\"grnd_level\": 896,\n" +
            "\"humidity\": 89,\n" +
            "\"temp_kf\": 2.66\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 98\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 2.2,\n" +
            "\"deg\": 297\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 00:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581217200,\n" +
            "\"main\": {\n" +
            "\"temp\": -7.41,\n" +
            "\"feels_like\": -11.76,\n" +
            "\"temp_min\": -8.74,\n" +
            "\"temp_max\": -7.41,\n" +
            "\"pressure\": 1031,\n" +
            "\"sea_level\": 1031,\n" +
            "\"grnd_level\": 900,\n" +
            "\"humidity\": 92,\n" +
            "\"temp_kf\": 1.33\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 802,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"scattered clouds\",\n" +
            "\"icon\": \"03n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 34\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 2.02,\n" +
            "\"deg\": 294\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 03:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581228000,\n" +
            "\"main\": {\n" +
            "\"temp\": -10.5,\n" +
            "\"feels_like\": -15.01,\n" +
            "\"temp_min\": -10.5,\n" +
            "\"temp_max\": -10.5,\n" +
            "\"pressure\": 1034,\n" +
            "\"sea_level\": 1034,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 93,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 801,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"few clouds\",\n" +
            "\"icon\": \"02n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 17\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.94,\n" +
            "\"deg\": 287\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 06:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581238800,\n" +
            "\"main\": {\n" +
            "\"temp\": -11.71,\n" +
            "\"feels_like\": -15.79,\n" +
            "\"temp_min\": -11.71,\n" +
            "\"temp_max\": -11.71,\n" +
            "\"pressure\": 1036,\n" +
            "\"sea_level\": 1036,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 92,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 800,\n" +
            "\"main\": \"Clear\",\n" +
            "\"description\": \"clear sky\",\n" +
            "\"icon\": \"01n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 0\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.2,\n" +
            "\"deg\": 284\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 09:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581249600,\n" +
            "\"main\": {\n" +
            "\"temp\": -12.65,\n" +
            "\"feels_like\": -16.7,\n" +
            "\"temp_min\": -12.65,\n" +
            "\"temp_max\": -12.65,\n" +
            "\"pressure\": 1037,\n" +
            "\"sea_level\": 1037,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 93,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 800,\n" +
            "\"main\": \"Clear\",\n" +
            "\"description\": \"clear sky\",\n" +
            "\"icon\": \"01n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 0\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.09,\n" +
            "\"deg\": 268\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 12:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581260400,\n" +
            "\"main\": {\n" +
            "\"temp\": -12.69,\n" +
            "\"feels_like\": -16.54,\n" +
            "\"temp_min\": -12.69,\n" +
            "\"temp_max\": -12.69,\n" +
            "\"pressure\": 1038,\n" +
            "\"sea_level\": 1038,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 93,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 67\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.8,\n" +
            "\"deg\": 317\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 15:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581271200,\n" +
            "\"main\": {\n" +
            "\"temp\": -7.52,\n" +
            "\"feels_like\": -10.8,\n" +
            "\"temp_min\": -7.52,\n" +
            "\"temp_max\": -7.52,\n" +
            "\"pressure\": 1035,\n" +
            "\"sea_level\": 1035,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 88,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 84\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.41,\n" +
            "\"deg\": 251\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 18:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581282000,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.01,\n" +
            "\"feels_like\": -7.14,\n" +
            "\"temp_min\": -4.01,\n" +
            "\"temp_max\": -4.01,\n" +
            "\"pressure\": 1031,\n" +
            "\"sea_level\": 1031,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 86,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.6,\n" +
            "\"deg\": 94\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-09 21:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581292800,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.73,\n" +
            "\"feels_like\": -7.76,\n" +
            "\"temp_min\": -4.73,\n" +
            "\"temp_max\": -4.73,\n" +
            "\"pressure\": 1029,\n" +
            "\"sea_level\": 1029,\n" +
            "\"grnd_level\": 900,\n" +
            "\"humidity\": 94,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.52,\n" +
            "\"deg\": 117\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 00:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581303600,\n" +
            "\"main\": {\n" +
            "\"temp\": -8.13,\n" +
            "\"feels_like\": -11.69,\n" +
            "\"temp_min\": -8.13,\n" +
            "\"temp_max\": -8.13,\n" +
            "\"pressure\": 1029,\n" +
            "\"sea_level\": 1029,\n" +
            "\"grnd_level\": 899,\n" +
            "\"humidity\": 92,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.81,\n" +
            "\"deg\": 288\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 03:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581314400,\n" +
            "\"main\": {\n" +
            "\"temp\": -8.01,\n" +
            "\"feels_like\": -11.9,\n" +
            "\"temp_min\": -8.01,\n" +
            "\"temp_max\": -8.01,\n" +
            "\"pressure\": 1029,\n" +
            "\"sea_level\": 1029,\n" +
            "\"grnd_level\": 899,\n" +
            "\"humidity\": 91,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.28,\n" +
            "\"deg\": 287\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 06:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581325200,\n" +
            "\"main\": {\n" +
            "\"temp\": -9.32,\n" +
            "\"feels_like\": -13.4,\n" +
            "\"temp_min\": -9.32,\n" +
            "\"temp_max\": -9.32,\n" +
            "\"pressure\": 1031,\n" +
            "\"sea_level\": 1031,\n" +
            "\"grnd_level\": 900,\n" +
            "\"humidity\": 93,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 55\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.44,\n" +
            "\"deg\": 310\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 09:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581336000,\n" +
            "\"main\": {\n" +
            "\"temp\": -10.97,\n" +
            "\"feels_like\": -15,\n" +
            "\"temp_min\": -10.97,\n" +
            "\"temp_max\": -10.97,\n" +
            "\"pressure\": 1032,\n" +
            "\"sea_level\": 1032,\n" +
            "\"grnd_level\": 901,\n" +
            "\"humidity\": 94,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 802,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"scattered clouds\",\n" +
            "\"icon\": \"03n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 43\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.22,\n" +
            "\"deg\": 315\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 12:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581346800,\n" +
            "\"main\": {\n" +
            "\"temp\": -11.72,\n" +
            "\"feels_like\": -15.5,\n" +
            "\"temp_min\": -11.72,\n" +
            "\"temp_max\": -11.72,\n" +
            "\"pressure\": 1035,\n" +
            "\"sea_level\": 1035,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 95,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 801,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"few clouds\",\n" +
            "\"icon\": \"02n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 21\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.8,\n" +
            "\"deg\": 296\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 15:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581357600,\n" +
            "\"main\": {\n" +
            "\"temp\": -6.17,\n" +
            "\"feels_like\": -9.52,\n" +
            "\"temp_min\": -6.17,\n" +
            "\"temp_max\": -6.17,\n" +
            "\"pressure\": 1034,\n" +
            "\"sea_level\": 1034,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 91,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 54\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.72,\n" +
            "\"deg\": 101\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 18:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581368400,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.05,\n" +
            "\"feels_like\": -7.42,\n" +
            "\"temp_min\": -4.05,\n" +
            "\"temp_max\": -4.05,\n" +
            "\"pressure\": 1032,\n" +
            "\"sea_level\": 1032,\n" +
            "\"grnd_level\": 903,\n" +
            "\"humidity\": 90,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.02,\n" +
            "\"deg\": 112\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-10 21:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581379200,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.52,\n" +
            "\"feels_like\": -7.9,\n" +
            "\"temp_min\": -4.52,\n" +
            "\"temp_max\": -4.52,\n" +
            "\"pressure\": 1031,\n" +
            "\"sea_level\": 1031,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 93,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.03,\n" +
            "\"deg\": 108\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 00:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581390000,\n" +
            "\"main\": {\n" +
            "\"temp\": -7.1,\n" +
            "\"feels_like\": -10.3,\n" +
            "\"temp_min\": -7.1,\n" +
            "\"temp_max\": -7.1,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 98\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.52,\n" +
            "\"deg\": 103\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 03:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581400800,\n" +
            "\"main\": {\n" +
            "\"temp\": -8.38,\n" +
            "\"feels_like\": -11.72,\n" +
            "\"temp_min\": -8.38,\n" +
            "\"temp_max\": -8.38,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 902,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 97\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.56,\n" +
            "\"deg\": 115\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 06:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581411600,\n" +
            "\"main\": {\n" +
            "\"temp\": -9.86,\n" +
            "\"feels_like\": -13.36,\n" +
            "\"temp_min\": -9.86,\n" +
            "\"temp_max\": -9.86,\n" +
            "\"pressure\": 1033,\n" +
            "\"sea_level\": 1033,\n" +
            "\"grnd_level\": 900,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 82\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.62,\n" +
            "\"deg\": 135\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 09:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581422400,\n" +
            "\"main\": {\n" +
            "\"temp\": -9.69,\n" +
            "\"feels_like\": -13.17,\n" +
            "\"temp_min\": -9.69,\n" +
            "\"temp_max\": -9.69,\n" +
            "\"pressure\": 1032,\n" +
            "\"sea_level\": 1032,\n" +
            "\"grnd_level\": 900,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 80\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.61,\n" +
            "\"deg\": 152\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 12:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581433200,\n" +
            "\"main\": {\n" +
            "\"temp\": -10.07,\n" +
            "\"feels_like\": -13.79,\n" +
            "\"temp_min\": -10.07,\n" +
            "\"temp_max\": -10.07,\n" +
            "\"pressure\": 1031,\n" +
            "\"sea_level\": 1031,\n" +
            "\"grnd_level\": 898,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.92,\n" +
            "\"deg\": 143\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 15:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581444000,\n" +
            "\"main\": {\n" +
            "\"temp\": -5.82,\n" +
            "\"feels_like\": -9.67,\n" +
            "\"temp_min\": -5.82,\n" +
            "\"temp_max\": -5.82,\n" +
            "\"pressure\": 1028,\n" +
            "\"sea_level\": 1028,\n" +
            "\"grnd_level\": 898,\n" +
            "\"humidity\": 94,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 99\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.54,\n" +
            "\"deg\": 129\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 18:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581454800,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.39,\n" +
            "\"feels_like\": -7.25,\n" +
            "\"temp_min\": -3.39,\n" +
            "\"temp_max\": -3.39,\n" +
            "\"pressure\": 1024,\n" +
            "\"sea_level\": 1024,\n" +
            "\"grnd_level\": 896,\n" +
            "\"humidity\": 93,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.88,\n" +
            "\"deg\": 140\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-11 21:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581465600,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.63,\n" +
            "\"feels_like\": -6.86,\n" +
            "\"temp_min\": -3.63,\n" +
            "\"temp_max\": -3.63,\n" +
            "\"pressure\": 1022,\n" +
            "\"sea_level\": 1022,\n" +
            "\"grnd_level\": 894,\n" +
            "\"humidity\": 96,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.01,\n" +
            "\"deg\": 112\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 00:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581476400,\n" +
            "\"main\": {\n" +
            "\"temp\": -5.43,\n" +
            "\"feels_like\": -8.71,\n" +
            "\"temp_min\": -5.43,\n" +
            "\"temp_max\": -5.43,\n" +
            "\"pressure\": 1022,\n" +
            "\"sea_level\": 1022,\n" +
            "\"grnd_level\": 893,\n" +
            "\"humidity\": 97,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.84,\n" +
            "\"deg\": 139\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 03:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581487200,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.11,\n" +
            "\"feels_like\": -6.98,\n" +
            "\"temp_min\": -4.11,\n" +
            "\"temp_max\": -4.11,\n" +
            "\"pressure\": 1023,\n" +
            "\"sea_level\": 1023,\n" +
            "\"grnd_level\": 894,\n" +
            "\"humidity\": 97,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 600,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"light snow\",\n" +
            "\"icon\": \"13n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.44,\n" +
            "\"deg\": 308\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 0.38\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 06:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581498000,\n" +
            "\"main\": {\n" +
            "\"temp\": -5.78,\n" +
            "\"feels_like\": -9.32,\n" +
            "\"temp_min\": -5.78,\n" +
            "\"temp_max\": -5.78,\n" +
            "\"pressure\": 1023,\n" +
            "\"sea_level\": 1023,\n" +
            "\"grnd_level\": 894,\n" +
            "\"humidity\": 97,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 601,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"snow\",\n" +
            "\"icon\": \"13n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 91\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.16,\n" +
            "\"deg\": 289\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 2.13\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 09:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581508800,\n" +
            "\"main\": {\n" +
            "\"temp\": -5.22,\n" +
            "\"feels_like\": -8.66,\n" +
            "\"temp_min\": -5.22,\n" +
            "\"temp_max\": -5.22,\n" +
            "\"pressure\": 1025,\n" +
            "\"sea_level\": 1025,\n" +
            "\"grnd_level\": 895,\n" +
            "\"humidity\": 96,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 600,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"light snow\",\n" +
            "\"icon\": \"13n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 89\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.07,\n" +
            "\"deg\": 259\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 0.13\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 12:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581519600,\n" +
            "\"main\": {\n" +
            "\"temp\": -5.53,\n" +
            "\"feels_like\": -8.72,\n" +
            "\"temp_min\": -5.53,\n" +
            "\"temp_max\": -5.53,\n" +
            "\"pressure\": 1026,\n" +
            "\"sea_level\": 1026,\n" +
            "\"grnd_level\": 895,\n" +
            "\"humidity\": 96,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 600,\n" +
            "\"main\": \"Snow\",\n" +
            "\"description\": \"light snow\",\n" +
            "\"icon\": \"13n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 94\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.67,\n" +
            "\"deg\": 255\n" +
            "},\n" +
            "\"snow\": {\n" +
            "\"3h\": 0.13\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 15:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581530400,\n" +
            "\"main\": {\n" +
            "\"temp\": -1.82,\n" +
            "\"feels_like\": -4.5,\n" +
            "\"temp_min\": -1.82,\n" +
            "\"temp_max\": -1.82,\n" +
            "\"pressure\": 1025,\n" +
            "\"sea_level\": 1025,\n" +
            "\"grnd_level\": 897,\n" +
            "\"humidity\": 91,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 80\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.41,\n" +
            "\"deg\": 280\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 18:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581541200,\n" +
            "\"main\": {\n" +
            "\"temp\": -0.58,\n" +
            "\"feels_like\": -3.26,\n" +
            "\"temp_min\": -0.58,\n" +
            "\"temp_max\": -0.58,\n" +
            "\"pressure\": 1024,\n" +
            "\"sea_level\": 1024,\n" +
            "\"grnd_level\": 897,\n" +
            "\"humidity\": 92,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 803,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"broken clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 81\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.65,\n" +
            "\"deg\": 223\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-12 21:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581552000,\n" +
            "\"main\": {\n" +
            "\"temp\": -0.97,\n" +
            "\"feels_like\": -3.35,\n" +
            "\"temp_min\": -0.97,\n" +
            "\"temp_max\": -0.97,\n" +
            "\"pressure\": 1024,\n" +
            "\"sea_level\": 1024,\n" +
            "\"grnd_level\": 897,\n" +
            "\"humidity\": 94,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04d\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 88\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.2,\n" +
            "\"deg\": 155\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"d\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-13 00:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581562800,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.31,\n" +
            "\"feels_like\": -5.9,\n" +
            "\"temp_min\": -3.31,\n" +
            "\"temp_max\": -3.31,\n" +
            "\"pressure\": 1025,\n" +
            "\"sea_level\": 1025,\n" +
            "\"grnd_level\": 897,\n" +
            "\"humidity\": 97,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 95\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.18,\n" +
            "\"deg\": 185\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-13 03:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581573600,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.98,\n" +
            "\"feels_like\": -7.08,\n" +
            "\"temp_min\": -3.98,\n" +
            "\"temp_max\": -3.98,\n" +
            "\"pressure\": 1026,\n" +
            "\"sea_level\": 1026,\n" +
            "\"grnd_level\": 897,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 98\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.82,\n" +
            "\"deg\": 104\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-13 06:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581584400,\n" +
            "\"main\": {\n" +
            "\"temp\": -3.98,\n" +
            "\"feels_like\": -7.05,\n" +
            "\"temp_min\": -3.98,\n" +
            "\"temp_max\": -3.98,\n" +
            "\"pressure\": 1025,\n" +
            "\"sea_level\": 1025,\n" +
            "\"grnd_level\": 895,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 0.77,\n" +
            "\"deg\": 125\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-13 09:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581595200,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.48,\n" +
            "\"feels_like\": -7.78,\n" +
            "\"temp_min\": -4.48,\n" +
            "\"temp_max\": -4.48,\n" +
            "\"pressure\": 1024,\n" +
            "\"sea_level\": 1024,\n" +
            "\"grnd_level\": 894,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.03,\n" +
            "\"deg\": 145\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-13 12:00:00\"\n" +
            "},\n" +
            "{\n" +
            "\"dt\": 1581606000,\n" +
            "\"main\": {\n" +
            "\"temp\": -4.93,\n" +
            "\"feels_like\": -8.52,\n" +
            "\"temp_min\": -4.93,\n" +
            "\"temp_max\": -4.93,\n" +
            "\"pressure\": 1022,\n" +
            "\"sea_level\": 1022,\n" +
            "\"grnd_level\": 893,\n" +
            "\"humidity\": 98,\n" +
            "\"temp_kf\": 0\n" +
            "},\n" +
            "\"weather\": [\n" +
            "{\n" +
            "\"id\": 804,\n" +
            "\"main\": \"Clouds\",\n" +
            "\"description\": \"overcast clouds\",\n" +
            "\"icon\": \"04n\"\n" +
            "}\n" +
            "],\n" +
            "\"clouds\": {\n" +
            "\"all\": 100\n" +
            "},\n" +
            "\"wind\": {\n" +
            "\"speed\": 1.37,\n" +
            "\"deg\": 136\n" +
            "},\n" +
            "\"sys\": {\n" +
            "\"pod\": \"n\"\n" +
            "},\n" +
            "\"dt_txt\": \"2020-02-13 15:00:00\"\n" +
            "}\n" +
            "],\n" +
            "\"city\": {\n" +
            "\"id\": 6173864,\n" +
            "\"name\": \"Vernon\",\n" +
            "\"coord\": {\n" +
            "\"lat\": 50.2581,\n" +
            "\"lon\": -119.2691\n" +
            "},\n" +
            "\"country\": \"CA\",\n" +
            "\"timezone\": -28800,\n" +
            "\"sunrise\": 1581175268,\n" +
            "\"sunset\": 1581210087\n" +
            "}\n" +
            "}";
}
